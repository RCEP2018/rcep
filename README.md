# RCEP

A Test Implementation for the publication:

#Further documentations will be uploaded soon.

Java -jar rcep.jar <parameters>

###To test the sliding window thechniques, parameters must be: -Mode windowTest -dataDescription windowdata -dataFile <file> -windowSize <int> -algorithm <int>


	---Mode & dataDescription are fix
	---dataFile: The data file location
	---WindowSize: The size of the window
	---Algorithme: the algorithme to test

     	--Description of algorithms:
 		--1  CPU Friendly Methode
  		--2  Memory Friendly Methode
 
####example
 
```
#####To test the CPUFriendlyMethode:

java -jar rcep.jar -mode windowtest -dataDescription windowdata -dataFile ./src/main/resources/iofiles/data_test.txt -windowSize 100 -algorithme 1

```


###To test the different implemented queries, parameters must be: -Mode queryTest -dataFile <file> -queryToRun <Q1 or Q2> 

	---Mode & dataDescription are fix
	---dataFile: The data file location:
		--1M Generated Stock events: .src/main/resources/streamDataFiles/GeneratedStock1M.stream
		--Real Stock events: .src/main/resources/streamDataFiles/RealStockEvent.stream
		--Real Credit Card events: .src/main/resources/streamDataFiles/RealCreditCard.stream


Description of queryToRun:
```
---Q1:  PATTERN SEQ (a, b+, c)
WHERE a.id = b.id 
AND b.id = c.id 
AND a.price < b.price 
AND b.price < NEXT(b.price) 
AND c.price < FIRST(b.price)
WITHIN 30 minutes SLIDE 2 minutes
```
```
---Q2:  PATTERN SEQ (a, b+)
WHERE a.id = b.id 
AND a.amount ≥ b.amount * 5 
AND b.amount > NEXT(b.amount) 
WITHIN 30 minutes SLIDE 5 minutes
```
####example
 
#####To test the first query:
```
java -jar rcep.jar -mode querytest -dataFile ./src/main/resources/streamDataFiles/GeneratedStock1M.streap -queryToRun Q1
```
#####To test the second query:
```
java -jar rcep.jar -mode querytest -dataFile ./src/main/resources/streamDataFiles/RealCreditCard.streap -queryToRun Q2

```

###To test the join algorithme, parameters must be: -Mode joinTest -dataFile <file> -dimension <int>  -size <int> -algorithm <int>
	--Mode & dataDescription are fix
	--dataFile: The data file location: src/main/resources/streamDataFiles/JoinTest.stream

	--dimension Number of dimensions: between 2 and 4 for this tests
	--size: How many elements to parse: 10000 or 100000
	--Algoriyhm:
	---Description of algorithms:
		---1  Hybrid Algorithme
		---2  Nested Join Algorithme
		---3  IEjoin Algorithme
####example
#####To Test the Hybrid Join Algorithme with anitcorrelated dataset:
```
java -jar rcep.jar -mode joinTest -dataDescription joindata -dataFile ./src/main/resources/streamDataFiles/jointest/anticorrelated_3d.stream -dimension 3 -windowSize 0 -size 10000 -algorithme 1
```

###examples:
```
To test the CPUFriendlyMethode:
java -jar rcep.jar -mode windowtest -dataDescription windowdata -dataFile ./src/main/resources/iofiles/data_test.txt -windowSize 100 -algorithme 1
```
```
To test the MemoryFriendlyMethode:
java -jar rcep.jar -mode windowtest -dataDescription windowdata -dataFile ./src/main/resources/iofiles/data_test.txt -windowSize 100 -algorithme 2
```
```
To Test the Hybrid Join Algorithme with anitcorrelated dataset:
java -jar rcep.jar -mode joinTest -dataDescription joindata -dataFile ./src/main/resources/streamDataFiles/jointest/anticorrelated_3d.stream -dimension 3 -windowSize 0 -size 10000 -algorithme 1
```

#Further documentations will be uploaded soon.
