package org.event;

import java.util.HashMap;

public class SimpleEvent {

	public int[] values;

	private static HashMap<String, Integer> comapny = new HashMap<>();
	private static int ids = 0;

	public void parseJoin(String line) {
		String[] input = line.split(" ");

		// take each float and convert it into an Integer...
		values = new int[input.length];

		for (int i = 0; i < values.length; i++) {
			String[] dim = input[i].split("\\.");
			values[i] = Integer.parseInt(dim[1]);
		}

	}

	public void parseJoinDyn(String line) {
		String[] input = line.split(",");
		// if (input.length < 6)
		// return;
		// return;
		// take each float and convert it into an Integer...
		values = new int[3];

		for (int i = 0; i < values.length; i++) {
			try {
				String[] dim = input[i + 6].split("\\.");

				values[i] = Integer.parseInt(dim[1]);
			} catch (Exception ex) {
				values[0] = -1;
			}
		}

	}

	private void putval(int i, int j, String[] input) {

		String[] dim1 = input[j].split("\\.");

		dim1[0] = dim1[0].substring(1);
		dim1[1] = dim1[1].replaceAll("\\p{P}", "");
		char[] d = dim1[1].toCharArray();
		String newd = "0";
		if (d.length - 1 >= 3) {
			newd = d[0] + "" + d[1] + d[2] + d[3];
		} else if (d.length - 1 == 2) {
			newd = d[0] + "" + d[1] + d[2];
		} else if (d.length - 1 == 1) {
			newd = d[0] + "" + d[1];
		}
		values[i] = Integer.parseInt(dim1[0]) * 1000 + Integer.parseInt(newd);
	}

	public void parse(String line) {

		String[] input = line.split(";");
		values = new int[4];

		values[0] = Integer.parseInt(input[0]);
		values[3] = Integer.parseInt(input[2]);
		values[1] = Integer.parseInt(input[3]);
		values[2] = Integer.parseInt(input[4]);

		// for (int i = 0; i < input.length; i++) {
		// values[i] = Integer.parseInt(input[i]);
		// }

	}

	public void parseForTree(String line) {

		String[] input = line.split(";");
		values = new int[input.length-1];
		for (int i = 0; i < values.length; i++) {
			values[i] = Integer.parseInt(input[i + 1]);
		}

	}

	public void parseCard(String line) {

		String[] input = line.split(";");
		values = new int[3];

		values[0] = Integer.parseInt(input[0]);

		values[1] = Integer.parseInt(input[1]);
		values[2] = Integer.parseInt(input[2]);

	}

	public void parseCard(String line, int t) {

		String[] input = line.split(",");
		values = new int[3];

		values[0] = Integer.parseInt(input[0]);

		// int floats = 0;
		// try {
		// floats = Integer.parseInt(amount[1]);
		// } catch (Exception ex) {
		// floats = 0;
		// }
		values[1] = (int) Double.parseDouble(input[1]);
		// values[2] = Integer.parseInt(input[2]);
		if (comapny.containsKey(input[2])) {
			values[2] = comapny.get(input[2]);
		} else {
			comapny.put(input[2], ++ids);
			values[2] = ids;
		}

	}

	public void parse2(String line, int t) {

		String[] input = line.split(" ");
		values = new int[3];
		String[] input2 = input[3].split("\\.");
		values[0] = t;
		values[1] = Integer.parseInt(input2[1]) + (Integer.parseInt(input2[0]) * 1000);
		values[2] = Integer.parseInt(input[2]);

	}

	// teststockevent.txt
	public void parseRealStock(String line) {

		String[] input = line.split("\t");
		values = new int[4];
		String[] input2 = input[3].split("\\.");
		int timeStamp = Integer.parseInt(input[1]);
		// long unixTime = System.currentTimeMillis() / 1000L;
		// long timeDiffernce = unixTime - timeStamp;
		// timeDiffernce /= 60;
		values[0] = timeStamp;

		if (input2.length >= 2) {
			values[1] = // Integer.parseInt(input2[1]) +
						// (Integer.parseInt(input2[0]) * 1000);
					(int) (Double.parseDouble(input[3]) * 100);
		} else
			values[1] = Integer.parseInt(input[3]) * 100;

		values[2] = (int) Double.parseDouble(input[3]);

		if (comapny.containsKey(input[0])) {
			values[3] = comapny.get(input[0]);
		} else {

			comapny.put(input[0], ++ids);
			values[3] = ids;
			System.out.println("Company " + input[0] + ids);
		}

	}

	// real_stock.txt
	public void parse4(String line, int t) {

		String[] input = line.split(" ");
		values = new int[3];
		String[] input2 = input[3].split("\\.");
		values[0] = t;
		values[1] = Integer.parseInt(input2[1]) + (Integer.parseInt(input2[0]) * 1000);

		values[2] = Integer.parseInt(input[2]);

	}

	public int[] getValues() {
		return values;
	}

	public void setValues(int[] values) {
		this.values = values;
	}

}
