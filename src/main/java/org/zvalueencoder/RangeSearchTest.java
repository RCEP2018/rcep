package org.zvalueencoder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.TreeSet;

import uk.co.omegaprime.btreemap.BTreeMap;

public class RangeSearchTest {

	static ZIndex lastreported;
	static int globalerror = 0, globacorrect = 0;

	public static void main(String[] args) throws IOException, DimensionException, CloneNotSupportedException {

		ZIndex minZRange = new ZIndex(new int[] { 1, 1085, 65536, 974 });
		ZIndex maxZRange = new ZIndex(new int[] { 1952, 65535, 131071, 975 });
		minZRange.compareTo(maxZRange);

		/**
		 * Initialize Tree
		 */
		int n = 0;
		TreeSet<ZIndex> setA = new TreeSet<ZIndex>();
		BTreeMap<ZIndex, Integer> eventStore = BTreeMap.create();
		for (int i = 0; i < 2000; i = (int) (i + (Math.random() * 10)) + 100) {
			for (int j = 1085; j < 65535; j = (int) (j + (Math.random() * 10)) + 250) {
				for (int k = 65536; k < 131071; k = (int) (k + (Math.random() * 10)) + 500) {
					for (int l = 800; l < 974; l = (int) (l + (Math.random() * 10)) + 5) {
						// setA.add(new ZIndex(new int[] { i, j, k }));
						eventStore.put(new ZIndex(new int[] { i, j, k, l }), n++);
					}
				}
			}
		}
		System.out.println("total number of Zindex in the event store " + n);

		/**
		 * create Ranges Min Max
		 */
		// from[1, 1085, 65536, 974]====>[1952, 65535, 131071, 974]

		long beginSearch = System.nanoTime();
		rangeSearch(minZRange, maxZRange, eventStore);
		long endSearch = System.nanoTime();

		System.out.println("processing time of rangeSearch  " + (endSearch - beginSearch));
		ArrayList<ZIndex> val = new ArrayList<>();
		HashSet<ZIndex> hset = new HashSet<ZIndex>();
		// lastreported = setA.first();

		for (int i = 10; i < 100; i = i + 10) {

			System.out.println("error rate " + i);

			beginSearch = System.nanoTime();
			some_points_in_rectangle(minZRange, maxZRange, eventStore, eventStore.firstKey(), hset, i);
			endSearch = System.nanoTime();
			System.out.println("\n processing time of optimized rangeSearch " + (endSearch - beginSearch)
					+ "\n # Out Of range: " + globalerror + " # In range values: " + hset.size());
			globalerror = 0;
			globacorrect = 0;
			System.out.println("\n");
		}

	}

	public static void some_points_in_rectangle(ZIndex begin, ZIndex end, BTreeMap<ZIndex, Integer> setA,
			ZIndex lastreported, HashSet<ZIndex> val, int errorrate) throws CloneNotSupportedException {

		// System.out.println("\n from" + Arrays.toString(begin.getValues()) +
		// "====>" + Arrays.toString(end.getValues()));
		int dim, cmsb, mask;
		int error = 0;
		SortedMap<ZIndex, Integer> results;
		results = setA.subMap(begin, true, end, true);
		Iterator<ZIndex> iterator = results.keySet().iterator();

		while (iterator.hasNext()) {

			lastreported = iterator.next();
			if (inBox(lastreported.getValues(), begin.getValues(), end.getValues())) {
				// System.out.println(Arrays.toString(lastreported.getValues()));
				val.add(lastreported);
				error = 0;
				globacorrect++;
			} else {
				error++;
				globalerror++;
				if (error >= errorrate) {
					ZIndex litMax = (ZIndex) end.clone();
					ZIndex bigMin = (ZIndex) begin.clone();

					dim = dimToCompare(begin.getValues(), end.getValues());
					/**
					 * Common most significant bits
					 */
					cmsb = commonMSB(end.getValues()[dim], begin.getValues()[dim]);
					mask = (~((1 << (cmsb + 1)) - 1) & 0xFF);
					bigMin.setValueOfDim(dim, (end.getValues()[dim] & mask) | (1 << cmsb));
					litMax.setValueOfDim(dim, (end.getValues()[dim] & mask) | ((1 << cmsb) - 1));

					if (begin.compareTo(litMax) > 0) {
						// System.out.println(begin +""+(litMax));
						break;
					}
					if (lastreported.compareTo(litMax) <= 0) {
						points_in_rectangle(begin, litMax, setA, lastreported, val, errorrate);
					}
					some_points_in_rectangle(bigMin, end, setA, lastreported, val, errorrate);
					break;
				}
			}
		}
		lastreported = end;
		RangeSearchTest.lastreported = end;
		// System.out.println("end of " + Arrays.toString(begin.getValues()) +
		// "===>" + Arrays.toString(end.getValues()));
	}

	private static void points_in_rectangle(ZIndex begin, ZIndex end, BTreeMap<ZIndex, Integer> setA,
			ZIndex lastreported, HashSet<ZIndex> val, int errorrate) throws CloneNotSupportedException {
		ZIndex litMax = (ZIndex) end.clone();
		ZIndex bigMin = (ZIndex) begin.clone();
		int dim = dimToCompare(begin.getValues(), end.getValues());
		/**
		 * Common most significant bits example 01100101101 and 01100110010 will
		 * give 011001\\00000
		 */
		int cmsb = commonMSB(end.getValues()[dim], begin.getValues()[dim]);
		int mask = (~((1 << (cmsb + 1)) - 1) & 0xFF);
		bigMin.setValueOfDim(dim, (end.getValues()[dim] & mask) | (1 << cmsb));
		litMax.setValueOfDim(dim, (end.getValues()[dim] & mask) | ((1 << cmsb) - 1));
		if (lastreported.compareTo(litMax) < 0) {
			points_in_rectangle(begin, litMax, setA, lastreported, val, errorrate);
		}
		some_points_in_rectangle(bigMin, end, setA, lastreported, val, errorrate);

	}

	public static boolean inBox(int[] value, int[] begin, int[] end) {
		for (int i = 0; i < value.length; i++) {
			if (value[i] < begin[i] || value[i] > end[i])
				return false;
		}
		return true;
	}

	public static int commonMSB(int x, int x1) {
		return msb(x ^ x1);
	}

	public static int dimToCompare(int[] v1, int[] v2) {
		return recursiveoOneOptimized(v1, v2, 0, 1);
	}

	public static int recursiveoOneOptimized(int[] v1, int[] v2, int i, int j) {
		if (j == v1.length) {
			return i;
		}
		if ((v1[i] ^ v2[i]) < (v1[j] ^ v2[j]) && (v1[i] ^ v2[i]) < ((v1[i] ^ v2[i]) ^ (v1[j] ^ v2[j]))) {
			return recursiveoOneOptimized(v1, v2, j, j + 1);
		} else {
			return recursiveoOneOptimized(v1, v2, i, j + 1);
		}

	}

	public static int msb(int vn) {
		int ndx = 0;
		while (1 < vn) {
			vn = (vn >> 1);
			ndx++;
		}
		return ndx;
	}

	public static int findPositionOfMSB(int n) {
		int high = 31, low = 0;

		while (high - low > 1) {
			int mid = (high + low) / 2;
			int maskHigh = (1 << high) - (1 << mid);
			if ((maskHigh & n) > 0) {
				low = mid;
			} else {
				high = mid;
			}
		}

		return low;

	}

	private static void rangeSearch(ZIndex begin, ZIndex end, BTreeMap<ZIndex, Integer> setA) {
		int error = 0, correct = 0;
		SortedMap<ZIndex, Integer> results;
		results = (setA.subMap(begin, true, end, true));
		System.out.println(" #elements on the range:  " + results.keySet().size());
		Iterator<ZIndex> iterator = results.keySet().iterator();
		while (iterator.hasNext()) {
			if (inBox(iterator.next().getValues(), begin.getValues(), end.getValues())) {
				// System.out.println(Arrays.toString(lastreported.getValues()));
				correct++;
			} else {
				error++;
			}
		}
		System.out.println("# out of range values: " + error + "  # In range values: " + correct);
	}

	public ArrayList<ZIndex[]> decomposeReanges(ZIndex begin, ZIndex end) throws CloneNotSupportedException {

		ZIndex litMax = (ZIndex) end.clone();
		ZIndex bigMin = (ZIndex) begin.clone();

		ArrayList<ZIndex[]> queries = new ArrayList<>();

		while (bigMin.compareTo(end) < 0) {

			int dim = dimToCompare(begin.getValues(), end.getValues());
			/**
			 * Common most significant bits
			 */
			int cmsb = commonMSB(end.getValues()[dim], begin.getValues()[dim]);
			int mask = (~((1 << (cmsb + 1)) - 1) & 0xFFFFFF);
			bigMin.setValueOfDim(dim, (end.getValues()[dim] & mask) | (1 << cmsb));
			litMax.setValueOfDim(dim, (end.getValues()[dim] & mask) | ((1 << cmsb) - 1));

			if (begin.compareTo(litMax) > 0) {
				// queries.add(new ZIndex[] { (ZIndex) begin.clone(), (ZIndex)
				// bigMin.clone() });
				break;
			}
			queries.add(new ZIndex[] { (ZIndex) begin.clone(), (ZIndex) litMax.clone() });
			// queries.add(new ZIndex[] { (ZIndex) bigMin.clone(), (ZIndex)
			// end.clone() });

			begin = (ZIndex) bigMin.clone();
			litMax = (ZIndex) end.clone();
			// begin = litMax;

		}
		queries.add(new ZIndex[] { bigMin, end });
		return queries;

	}

	public ArrayList<ZIndex[]> decomposeintoTwo(ZIndex begin, ZIndex end) throws CloneNotSupportedException {
		ZIndex litMax = (ZIndex) end.clone();
		ZIndex bigMin = (ZIndex) begin.clone();
		ArrayList<ZIndex[]> re = new ArrayList<>();
		int dim = dimToCompare(begin.getValues(), end.getValues());
		/**
		 * Common most significant bits
		 */
		int cmsb = commonMSB(end.getValues()[dim], begin.getValues()[dim]);
		int mask = (~((1 << (cmsb + 1)) - 1) & 0xFFFFFF);
		bigMin.setValueOfDim(dim, (end.getValues()[dim] & mask) | (1 << cmsb));
		litMax.setValueOfDim(dim, (end.getValues()[dim] & mask) | ((1 << cmsb) - 1));
		re.add(new ZIndex[] { (ZIndex) begin, (ZIndex) litMax });
		re.add(new ZIndex[] { (ZIndex) bigMin, (ZIndex) end });

		return re;
		// return new ArrayList< ZIndex[]>().add( new ZIndex[] { (ZIndex)
		// begin.clone(), (ZIndex) litMax.clone() });
	}

	public ZIndex[][] decomposeintoTwo2(ZIndex begin, ZIndex end) throws CloneNotSupportedException {
		ZIndex litMax = (ZIndex) end.clone();
		ZIndex bigMin = (ZIndex) begin.clone();

		int dim = dimToCompare(begin.getValues(), end.getValues());
		/**
		 * Common most significant bits
		 */
		int cmsb = commonMSB(end.getValues()[dim], begin.getValues()[dim]);
		int mask = (~((1 << (cmsb + 1)) - 1) & 0xFFFFFF);
		bigMin.setValueOfDim(dim, (end.getValues()[dim] & mask) | (1 << cmsb));
		litMax.setValueOfDim(dim, (end.getValues()[dim] & mask) | ((1 << cmsb) - 1));

		ZIndex[][] q = new ZIndex[2][2];
		q[0] = new ZIndex[] { (ZIndex) begin, (ZIndex) litMax };
		q[1] = new ZIndex[] { (ZIndex) bigMin, (ZIndex) end };

		return q;
		// return new ArrayList< ZIndex[]>().add( new ZIndex[] { (ZIndex)
		// begin.clone(), (ZIndex) litMax.clone() });
	}
}
