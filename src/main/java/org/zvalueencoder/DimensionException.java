package org.zvalueencoder;

/**
 * 
 * @author 
 *
 */
public class DimensionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5932366055526957274L;

	public DimensionException(String message) {

		super(message);
	}

}
