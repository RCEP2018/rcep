package org.zvalueencoder;

import java.util.Arrays;

/**
 * Comparable ZIndex using the
 * 
 * @author 
 *
 */

public class ZIndex implements Comparable<ZIndex>, Cloneable {

	/**
	 * The incoming values to encode, these values are used using the XOR
	 * operation to see which one is larger than other. So no need for bit
	 * shuffling.
	 */
	public int[] values;// TODO: remove it later then
	public int id_a;
	public int id_b;
	// private BitSet group;
	public int group_check;

	public ZIndex(int[] v) {
		group_check = 0;
		this.values = v;
		// group = new BitSet(values.length);
		this.commonbit(v);
		/**
		 * generate the group this address belongs to
		 */

	}

	/// Dummy constructor which will be deleted later, for now there are so many
	/// errors in other classes

	public ZIndex(int[] values, int knowDim, int[] masks) {
		System.out.println("ERROR ERROR.. Don't Use this ZIndex Constructor, Check ZIndex Class");
	}

	/*
	 * @Override public int hashCode() { final int prime = 31; int result = 1;
	 * result = prime * result + Arrays.hashCode(values); return result; }
	 * 
	 * @Override public boolean equals(Object obj) { if (this == obj) return
	 * true; if (obj == null) return false; if (getClass() != obj.getClass())
	 * return false; ZIndex other = (ZIndex) obj; if (!Arrays.equals(values,
	 * other.values)) return false; return true; }
	 */
	public int[] getValues() {
		return values;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(values);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ZIndex other = (ZIndex) obj;
		if (!Arrays.equals(values, other.values))
			return false;
		return true;
	}

	public void setValues(int[] values) {
		this.values = values;
	}

	@Override
	public int compareTo(ZIndex o) {

		return recursiveoOneOptimized(values, o.values, 0, 1);

	}

	/*
	 * DO NOT DELETE IT YET
	 * 
	 * public int genericCompare(int[] v2) { int[] xors = new
	 * int[values.length]; for (int i = 0; i < values.length; i++) { xors[i] =
	 * values[i] ^ v2[i]; } return recursiveoOne(xors, values, v2, 0, 1); }
	 * 
	 * public int recursiveoOne(int[] xors, int[] v1, int[] v2, int i, int j) {
	 * if (j == xors.length) { // System.out.println("dimension to compare: " +
	 * i); if (v1[i] < v2[i]) { // System.out.println("v2 is greater "); return
	 * -1; } else { // System.out.println("v1 is greater "); return 1; } //
	 * return; } if (xors[i] < xors[j] && xors[i] < (xors[i] ^ xors[j])) {
	 * return recursiveoOne(xors, v1, v2, j, j + 1); } else { return
	 * recursiveoOne(xors, v1, v2, i, j + 1); }
	 * 
	 * }
	 */

	public int genericCompareOptimized(int[] v2) {
		return recursiveoOneOptimized(values, v2, 0, 1);
	}

	private int recursiveoOneOptimized(int[] v1, int[] v2, int i, int j) {
		if (j == v1.length) {
			// System.out.println("dimension to compare: " + i);
			if (v1[i] < v2[i]) {
				// System.out.println("v2 is greater ");
				return -1;
			} else if ((v1[i] == v2[i]))
				return 0;
			else {
				// System.out.println("v1 is greater ");
				return 1;
			}
			// return;
		}
		if ((values[i] ^ v2[i]) < (values[j] ^ v2[j])
				&& (values[i] ^ v2[i]) < ((values[i] ^ v2[i]) ^ (values[j] ^ v2[j]))) {
			return recursiveoOneOptimized(v1, v2, j, j + 1);
		} else {
			return recursiveoOneOptimized(v1, v2, i, j + 1);
		}

	}

	@Override
	public String toString() {
		return "ZIndex [values=" + Arrays.toString(values) + ", id_a=" + id_a + ", id_b=" + id_b + ", group_check="
				+ group_check + "]";
	}

	/**
	 * To check which group of bits it belong to
	 */
	public void commonbit(int[] v) {

		if (v.length == 0)
			return;

		// String bits = "";
		// int[] pos = new int[v.length];
		// for (int i = 0; i < v.length; i++) {
		// pos[i] = findPositionOfMSB(v[i]);
		//
		// }

		for (int i = 0; i < v.length; i++) {

			if (i != v.length - 1) {

				// bits = bits + " " + compare(pos[i], pos[i + 1], pos.length);

				if (compare(findPositionOfMSB(v[i]), findPositionOfMSB(v[i + 1]), v.length)) {

					/// set the position i in a number
					group_check |= 1 << i;
					// group.set(i);
				}

			} else {
				// int l = pos[i] >= pos[i - 1] ? 1 : 0;
				// bits = bits + " " + compare(pos[i], pos[i - 1], pos.length);
				if (compare(findPositionOfMSB(v[i]), findPositionOfMSB(v[i - 1]), v.length)) {
					group_check |= 1 << i;
					// group.set(i);
				}

			}

		}

		// System.out.println(bits);

	}

	public static int findPositionOfMSB(int n) {
		int high = 31, low = 0;

		while (high - low > 1) {
			int mid = (high + low) / 2;
			int maskHigh = (1 << high) - (1 << mid);
			if ((maskHigh & n) > 0) {
				low = mid;
			} else {
				high = mid;
			}
		}

		return low;

	}

	public static boolean compare(int a, int b, int bits) {

		if (a < b)
			return false;
		else if (a > b && a >= bits)
			return true;
		else if (a > b && a < bits)
			return false;

		if (a == b && a >= bits)
			return true;
		else
			return false;

	}

	public void setValueOfDim(int dim, int i) {
		// TODO Auto-generated method stub
		values[dim] = i;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		ZIndex o = null;
		o = (ZIndex) super.clone();
		o.setValues(values.clone());
		return o;
	}
}
