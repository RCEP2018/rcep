package org.zvalueencoder;

import java.util.ArrayList;
import java.util.Arrays;

import org.btreeUtils.Value;

public class Zregion {

	public ArrayList<ZIndex> values;
	// public ArrayList<ZIndex> keys;
	public int[] max;
	public int[] min;

	public int sorted; // 1: for yes , 2: for no
	/// To check if a list is already sorted or not

	public Zregion(int size) {

		values = new ArrayList<>();
		// keys = new ArrayList<>();
		max = new int[size];
		min = new int[size];
		sorted = 0;

		/// do I have to fill it or not?
		Arrays.fill(max, Integer.MIN_VALUE);
		Arrays.fill(min, Integer.MAX_VALUE);
	}

	public void add(Value v) {
		// this.values.add(v);

		this.updateMaxMin(this.max, v.values, this.min);

	}

	public void add(ZIndex key) {
		this.values.add(key);

		this.updateMaxMin(this.max, key.getValues(), this.min);

	}

	public ArrayList<ZIndex> getValues() {
		return values;
	}

	public void setValues(ArrayList<ZIndex> values) {
		this.values = values;
	}

	public int[] getMax() {
		return max;
	}

	public void setMax(int[] max) {
		this.max = max;
	}

	public int[] getMin() {
		return min;
	}

	public void setMin(int[] min) {
		this.min = min;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(max);
		result = prime * result + Arrays.hashCode(min);
		result = prime * result + ((values == null) ? 0 : values.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Zregion other = (Zregion) obj;
		if (!Arrays.equals(max, other.max))
			return false;
		if (!Arrays.equals(min, other.min))
			return false;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!values.equals(other.values))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Zregion [values=" + values + ", max=" + Arrays.toString(max) + ", min=" + Arrays.toString(min) + "]";
	}

	private void updateMaxMin(int[] max_values, int[] originals, int[] min_values) {

		for (int i = 0; i < originals.length; i++) {
			if (max_values[i] < originals[i]) {

				max_values[i] = originals[i];
			}

			if (min_values[i] > originals[i]) {

				min_values[i] = originals[i];
			}

		}

	}

}
