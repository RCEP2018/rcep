package org.zvalueencoder;

import java.util.ArrayList;

import uk.co.omegaprime.btreemap.BTreeMap;

public class QueriesGenerator {

	public ArrayList<ZIndex[]> queries;
	BTreeMap<ZIndex, Integer> tree;

	RangeSearchTest rs;

	public QueriesGenerator(BTreeMap<ZIndex, Integer> t, RangeSearchTest zc) {
		queries = new ArrayList<>();
		this.tree = t;
		this.rs = zc;
	}

	public void generateQueries(ZIndex z[]) throws CloneNotSupportedException {
		// create two queries and check if it can b/ should be further divided

		ZIndex[][] q = rs.decomposeintoTwo2(z[0], z[1]);

		if (tree.subMap(q[0][0], true, q[0][1], true).keySet().size() > 200 && q[0][0].compareTo(q[0][1]) < 0) {
			generateQueries(q[0]);
		} else {
			queries.add(q[0]);
			// return;
		}

		if (tree.subMap(q[1][0], true, q[1][1], true).keySet().size() > 200 && q[1][0].compareTo(q[1][1]) < 0) {
			generateQueries(q[1]);
		} else {
			queries.add(q[1]);
			// return;
		}

		return;

	}

}
