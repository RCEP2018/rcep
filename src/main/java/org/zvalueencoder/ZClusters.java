package org.zvalueencoder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.SortedMap;

public class ZClusters {
	static ZIndex lastreported;

	public static int msb(int vn) {
		int ndx = 0;
		while (1 < vn) {
			vn = (vn >> 1);
			ndx++;
		}
		return ndx;
	}

	public static int dimOfBitPosition(int position, int nbDim) {
		return (nbDim - 1) - (position % nbDim);
	}

	public static int dimToCompare(int[] v1, int[] v2) {
		return recursiveoOneOptimized(v1, v2, 0, 1);
	}

	public static int recursiveoOneOptimized(int[] v1, int[] v2, int i, int j) {
		if (j == v1.length) {
			return i;
		}
		if ((v1[i] ^ v2[i]) < (v1[j] ^ v2[j]) && (v1[i] ^ v2[i]) < ((v1[i] ^ v2[i]) ^ (v1[j] ^ v2[j]))) {
			return recursiveoOneOptimized(v1, v2, j, j + 1);
		} else {
			return recursiveoOneOptimized(v1, v2, i, j + 1);
		}

	}

	public static int commonMSB(int x, int x1) {
		return msb(x ^ x1);
	}

	public static void some_points_in_rectangle(ZIndex begin, ZIndex end, SortedMap<ZIndex, Integer> setA,
			ArrayList<ZIndex> val) throws CloneNotSupportedException {

		// System.out.println("\n from" + Arrays.toString(begin.getValues()) +
		// "====>" + Arrays.toString(end.getValues()));
		int dim, cmsb, mask;
		int error = 0;
		// SortedSet<ZIndex> results;
		// results = setA.subSet(begin, true, end, true);
		// Iterator it = b.keySet().iterator();
		Iterator<ZIndex> iterator = setA.keySet().iterator();
		while (iterator.hasNext()) {

			lastreported = iterator.next();

			if (isInrange(begin.values, end.values, lastreported.values)) {
				// System.out.println(Arrays.toString(lastreported.getValues()));

				val.add(lastreported);
				error = 0;
			} else {
				error++;
				// globalerror++;
				if (error >= 7) {
					ZIndex litMax = (ZIndex) end.clone();
					ZIndex bigMin = (ZIndex) begin.clone();
					dim = dimToCompare(begin.getValues(), end.getValues());
					/**
					 * Common most significant bits
					 */
					cmsb = commonMSB(end.getValues()[dim], begin.getValues()[dim]);
					mask = (~((1 << (cmsb + 1)) - 1) & 0xFF);
					bigMin.setValueOfDim(dim, (end.getValues()[dim] & mask) | (1 << cmsb));
					litMax.setValueOfDim(dim, (end.getValues()[dim] & mask) | ((1 << cmsb) - 1));
					if (lastreported.compareTo(litMax) <= 0) {
						points_in_rectangle(begin, litMax, setA, val);
					}
					some_points_in_rectangle(bigMin, end, setA, val);
					break;
				}
			}
		}
		// System.out.println("end of " + Arrays.toString(begin.getValues()) +
		// "===>" + Arrays.toString(end.getValues()));
	}

	private static void points_in_rectangle(ZIndex begin, ZIndex end, SortedMap<ZIndex, Integer> setA,
			ArrayList<ZIndex> val) throws CloneNotSupportedException {
		ZIndex litMax = (ZIndex) end.clone();
		ZIndex bigMin = (ZIndex) begin.clone();
		int dim = dimToCompare(begin.getValues(), end.getValues());
		/**
		 * Common most significant bits
		 */
		int cmsb = commonMSB(end.getValues()[dim], begin.getValues()[dim]);
		int mask = (~((1 << (cmsb + 1)) - 1) & 0xFF);
		bigMin.setValueOfDim(dim, (end.getValues()[dim] & mask) | (1 << cmsb));
		litMax.setValueOfDim(dim, (end.getValues()[dim] & mask) | ((1 << cmsb) - 1));
		if (lastreported.compareTo(litMax) < 0) {
			points_in_rectangle(begin, litMax, setA, val);
		}
		some_points_in_rectangle(bigMin, end, setA, val);

	}

	private static boolean isInrange(int[] min, int[] max, int[] point) {
		if (!(point[0] > min[0] && point[0] <= max[0]))
			return false;
		for (int i = 1; i < point.length - 1; i++) {

			if (!(point[i] >= min[i] && point[i] <= max[i]))
				return false;
		}

		if ((point[3] != min[3]))
			return false;

		return true;
	}

}
