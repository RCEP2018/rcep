package org.baseline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.lucene.util.OpenBitSet;
import org.event.SimpleEvent;
import org.queryprocessor.AbstractQueryProcessor;
import org.windowsemantics.AbstractWindow;
import org.zvalueencoder.DimensionException;
import org.zvalueencoder.ZIndex;
import org.zvalueencoder.Zregion;

public class Baseline_Query1 extends AbstractQueryProcessor {
	ArrayList<SimpleEvent> eventsA;
	ArrayList<SimpleEvent> eventsB;
	int test = 0;

	public Baseline_Query1(LinkedBlockingQueue<SimpleEvent> i, CountDownLatch l, AbstractWindow w, int d)
			throws IOException {
		super(i, l, w, d);
		eventsA = new ArrayList<>();
		eventsB = new ArrayList<>();
	}

	@Override
	protected void process(SimpleEvent e) throws IOException, DimensionException {

		window.updateWindow(e.values[0]);

		/**
		 * Delete the old events from A events' list
		 */
		windowComputation(e, this.eventsA);

		/**
		 * Delete the old events from B events' list
		 */
		windowComputation(e, this.eventsB);

		/**
		 * Run the Join and Output the Matches
		 */
		queryProcessor(e);

		/**
		 * Add the new event in both events' list depending on the predicates
		 */
		eventStorage(e);

		// System.out.println(++test);

	}

	private void queryProcessor(SimpleEvent c) {
		// For each event get the A and B that can match with the C
		// We've to store the intermediate result sets:
		if (c.values[2] > 150)
			return;

		if (this.eventsA.isEmpty() || this.eventsB.isEmpty())
			return;

		ArrayList<SimpleEvent> eventsBmatchedWithC = new ArrayList<>();

		for (SimpleEvent b : eventsB) {
			if (b.values[0] < c.values[0] && b.values[1] > c.values[1] && b.values[1] > 200 && b.values[2] > 150
					&& b.values[3] == c.values[3]) {
				eventsBmatchedWithC.add(b);
			}
		}
		// System.out.println("intermediate " + eventsBmatchedWithC.size());
		/// take each A and match it with all the eventsBmatchedWithC to get the

		for (SimpleEvent a : eventsA) {
			ArrayList<SimpleEvent> finalBs = new ArrayList<>();
			// ArrayList<SimpleEvent> finalAs = new ArrayList<>();
			for (SimpleEvent b : eventsBmatchedWithC) {
				if (a.values[0] < b.values[0] && a.values[1] < b.values[1] && a.values[1] % 50 == 0 && b.values[2] > 150
						&& b.values[3] == a.values[3]) {
					/// matched
					finalBs.add(b);
					// finalAs.add(a);
				}
			}

			// Kleene+ Matches
			if (finalBs.isEmpty())
				continue;
			// outPutComputation(kleenePlusComputation(finalBs, a, c), a, c);
			// newKleenePlus(finalBs, a, c);

			if (finalBs.size() == 15) {
				System.out.println(finalBs.size());
			}
			outPutComputation(this.powerset(finalBs), a, c);

		}
		// System.out.println("Size of A " + eventsA.size());
		// System.out.println("Size of B " + eventsB.size());
		/// for each arrived event, put that in both lists A and B

	}

	private void windowComputation(SimpleEvent c, ArrayList<SimpleEvent> events) {
		/// For each event check remove all the events in A and B whose
		/// timestamp is less than the window

		int eventsToDelete = c.values[0] - window.windowLength;
		if (eventsToDelete < 0)
			return;
		for (Iterator<SimpleEvent> iterator = events.iterator(); iterator.hasNext();) {
			SimpleEvent e = iterator.next();
			if (e.values[0] <= eventsToDelete) {

				iterator.remove();
			}
		}
	}

	private void outPutComputation(List<List<SimpleEvent>> powerSet, SimpleEvent a, SimpleEvent c) {
		powerSet.remove(0);
		// System.out.println(powerSet);
		Iterator<List<SimpleEvent>> iterator = powerSet.iterator();
		while (iterator.hasNext()) {
			StringBuilder sb = new StringBuilder();
			sb.append("a ");
			sb.append(Arrays.toString(a.values));
			sb.append(" ; ");

			List<SimpleEvent> bs = iterator.next();
			for (SimpleEvent b : bs) {
				sb.append("b ");
				sb.append(Arrays.toString(b.values));
				sb.append(" ; ");

			}
			sb.append("c ");
			sb.append(Arrays.toString(c.values));
			// sb.append(" ; ");
			// System.out.println(sb.toString());
		}

	}

	/*
	 * private void newKleenePlus(ArrayList<SimpleEvent> finalBs, SimpleEvent a,
	 * SimpleEvent c) { StringBuilder sb = new StringBuilder(); int allMasks =
	 * (1 << finalBs.size()); for (int i = 1; i < allMasks; i++) { sb = new
	 * StringBuilder(); sb.append("a "); sb.append(Arrays.toString(a.values));
	 * sb.append(" ; "); for (int j = 0; j < finalBs.size(); j++) if ((i & (1 <<
	 * j)) > 0) { // The j-th element is used // System.out.print((j + 1) + " "
	 * + finalBs.get(j));
	 * 
	 * sb.append("b "); sb.append(Arrays.toString(finalBs.get(j).values));
	 * sb.append(" ; "); } // System.out.println(); sb.append("c ");
	 * sb.append(Arrays.toString(c.values)); // sb.append(" ; "); //
	 * System.out.println(sb.toString()); } }
	 */

	public static <T> List<List<T>> powerset(Collection<T> list) {
		List<List<T>> ps = new ArrayList<List<T>>();
		ps.add(new ArrayList<T>()); // add the empty set

		// for every item in the original list
		for (T item : list) {
			List<List<T>> newPs = new ArrayList<List<T>>();

			for (List<T> subset : ps) {
				// copy all of the current powerset's subsets
				newPs.add(subset);

				// plus the subsets appended with the current item
				List<T> newSubset = new ArrayList<T>(subset);
				newSubset.add(item);
				newPs.add(newSubset);
			}

			// powerset is now powerset of list.subList(0, list.indexOf(item)+1)
			ps = newPs;
		}
		return ps;
	}

	private HashSet kleenePlusComputation(ArrayList<SimpleEvent> finalBs, SimpleEvent a, SimpleEvent c) {

		System.out.println("B Size " + finalBs.size());
		HashSet powerSet = new HashSet<List<SimpleEvent>>();

		buildPowerSet(finalBs, finalBs.size(), powerSet);

		return powerSet;

	}

	private static void buildPowerSet(List<SimpleEvent> list, int count, HashSet powerSet) {
		powerSet.add(list);

		for (int i = 0; i < list.size(); i++) {
			List<SimpleEvent> temp = new ArrayList<SimpleEvent>(list);
			temp.remove(i);
			buildPowerSet(temp, temp.size(), powerSet);
		}
	}

	public void eventStorage(SimpleEvent e) {

		// if (e.values[1] > 200 && e.values[2] > 150) {
		this.eventsB.add(e);
		// }

		// if (e.values[1] % 50 == 0) {
		this.eventsA.add(e);
		// }

	}

	@Override
	protected void joinSingleRegions(OpenBitSet[] listbitsets, Zregion r1, Zregion r2, String[] phis,
			ArrayList<ZIndex> valB) {
		// TODO Auto-generated method stub

	}

	@Override
	protected boolean isInrangeB(int[] min, int[] max, int[] point) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean isInrangeA(int[] min, int[] max, int[] point) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int dominates_generic(int[] max_r1, int[] min_r1, int[] max_r2, int[] min_r2, String[] phis) {
		// TODO Auto-generated method stub
		return 0;
	}

}
