package org.benchmark.joins;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.lucene.util.OpenBitSet;
import org.btreeUtils.MaxAndMinValues;
import org.event.SimpleEvent;
import org.eventstore.EventStore;
import org.outputmanager.QueryProcessorMeasure;
import org.queryprocessor.AbstractQueryProcessor;
import org.queryprocessor.Query1.TimeComparator;
import org.windowsemantics.AbstractWindow;
import org.zvalueencoder.DimensionException;
import org.zvalueencoder.ZIndex;
import org.zvalueencoder.Zregion;

public class JoinTest2Dim extends AbstractQueryProcessor {
	MaxAndMinValues store_1_maxmin;
	MaxAndMinValues store_2_maxmin;
	final static QueryProcessorMeasure measure = new QueryProcessorMeasure();
	IEJoinTest ieJoin;
	int total_events;
	private static int SIZE = 0;
	private static int JOIN_TYPE = 0;

	public JoinTest2Dim(LinkedBlockingQueue<SimpleEvent> i, CountDownLatch l, AbstractWindow w, int dimen, int size,
			int join_method) throws IOException {
		super(i, l, w, dimen);
		// TODO Auto-generated constructor stub
		store_1_maxmin = new MaxAndMinValues(this.dim);
		store_2_maxmin = new MaxAndMinValues(this.dim);
		ieJoin = new IEJoinTest();
		this.SIZE = size;
		this.JOIN_TYPE = join_method;
	}

	@Override
	protected void process(SimpleEvent e) throws IOException, DimensionException {

		if (e.values[0] == -1)
			return;
		total_events++;

		if (total_events <= SIZE) {
			correlatedDistributionJoins(e, 1);
		} else {
			correlatedDistributionJoins(e, 2);
		}

		// System.out.println(total_events);
		if (total_events == SIZE * 2) {
			try {
				measure.notifyStart(1);
				System.out.println("2 Dimensions");
				queryImplementation(e, JOIN_TYPE);

				measure.notifyFinish(1);
			} catch (CloneNotSupportedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			measure.outputMeasure();
			finish();
		}
	}

	private void correlatedDistributionJoins(SimpleEvent ce, int st) throws IOException {
		ZIndex key = new ZIndex(ce.values);

		// ADD to the store_1
		if (st == 1) {
			store_1_maxmin.updateMaxMin(ce.values);

			this.store_1.getT().put(key, 0);
		} else {
			/// ADD to the store_2

			store_2_maxmin.updateMaxMin(ce.values);

			store_2.getT().put(key, 0);
		}
	}

	private ZIndex[] createRangesFor(int store) {

		// B.price > C.price
		int[] min = new int[this.dim];
		int[] max = new int[this.dim];
		for (int i = 0; i < this.dim; i++) {
			if (store == 1)
				min[i] = this.store_1_maxmin.min[i];
			else
				min[i] = this.store_2_maxmin.min[i];

			if (store == 1)
				max[i] = this.store_1_maxmin.max[i];
			else
				max[i] = this.store_2_maxmin.max[i];
		}

		ZIndex minZRange = new ZIndex(min);

		ZIndex maxZRange = new ZIndex(max);

		return new ZIndex[] { minZRange, maxZRange };
	}

	private void queryImplementation(SimpleEvent ce, int algo_type) throws CloneNotSupportedException, IOException {

		ArrayList<ZIndex> valB = new ArrayList<>();
		// EventStore store = (store_1.getStoreType() == 0) ? store_1 : store_2;

		ZIndex[] ranges = createRangesFor(1);

		// get the maxi

		ArrayList<Zregion> regionsForB = new ArrayList<>(10);
		queryStores(ce, dim, valB, regionsForB, ranges, 2, store_1);

		ranges = createRangesFor(2);

		ArrayList<ZIndex> valA = new ArrayList<>();

		ArrayList<Zregion> regionsForA = new ArrayList<>(10);

		this.queryStores(ce, dim, valA, regionsForA, ranges, 1, store_2);

		OpenBitSet[] listbitsets = new OpenBitSet[valA.size()];

		/**
		 * new String[] { "<", "<", ">" } This means A.time < B.time, A.price <
		 * B.price and A.vol >B.vol
		 */
		/**
		 * Our join Algorithm
		 */
		QueryProcessorMeasure.costOfCrossProduct = QueryProcessorMeasure.costOfCrossProduct
				+ (valA.size() * valB.size());
		QueryProcessorMeasure.numOfJoins++;
		if (algo_type == 1)
			implementJoins(regionsForA, regionsForB, valA, valB, listbitsets, new String[] { "<", "<" });
		// implementJoins(regionsForA, regionsForB, valA, valB, listbitsets, new
		// String[] { "<", "<", "<", "<" });
		/**
		 * Nested Join Algorithm
		 */
		else if (algo_type == 2)
			nestedQueryJoin(valA, valB, ce, listbitsets);
		/**
		 * IE Join Algorithm from the VLDB paper
		 */
		else if (algo_type == 3)
			ieJoin.IEJoin2(valA, valB, null, this.measure);
		/**
		 * If there is a Kleene plus sign involved in the query
		 */

		// kleeneplusImpl(valA, valB, listbitsets, ce.values);
	}

	private void queryStores(SimpleEvent ce, int dim, ArrayList<ZIndex> val, ArrayList<Zregion> regionsFor,
			ZIndex[] ranges, int e_type, EventStore store) throws CloneNotSupportedException {

		SortedMap<ZIndex, Integer> b = null;

		ArrayList<ZIndex[]> queries = zc.decomposeintoTwo(ranges[0], ranges[1]);

		this.queryValues(val, queries, regionsFor, b, ranges, store, e_type);

	}

	private void queryValues(ArrayList<ZIndex> valB, ArrayList<ZIndex[]> queries, ArrayList<Zregion> regions,
			SortedMap<ZIndex, Integer> b, ZIndex[] ranges, EventStore store, int bORa) {

		for (ZIndex[] q : queries) {
			b = store.getT().subMap(q[0], true, q[1], true);
			if (b != null || !b.isEmpty()) {
				newCreateRegions(b, ranges, this.dim, bORa, valB, regions);
			}
		}

	}

	@Override
	protected boolean isInrangeB(int[] min, int[] max, int[] point) {
		for (int i = 0; i < point.length; i++) {

			if (!(point[i] >= min[i] && point[i] <= max[i]))
				return false;
		}

		return true;

	}

	@Override
	protected void joinSingleRegions(OpenBitSet[] listbitsets, Zregion r1, Zregion r2, String[] phis,
			ArrayList<ZIndex> valB) {
		/// Sort them acording to the timestamps to make it event sequences
		List<ZIndex> a = r1.getValues();
		List<ZIndex> b = r2.getValues();

		if (r1.sorted == 0) {
			Collections.sort(a, new TimeComparator());
			r1.sorted = 1;
		}

		if (r2.sorted == 0) {
			Collections.sort(b, new TimeComparator());
			r2.sorted = 1;
		}

		// int numofsetbits = 0;

		int start = b.size() - 1;
		/// loop over A and B

		for (int i = a.size() - 1; i >= 0; i--) {
			// numofsetbits = 0;

			/**
			 * Check if if this value can actually match with any other value in
			 * B or not using the max and min of region r2, Depending on the
			 * query, the phis will be different. Note that it supports strickly
			 * less than or greater than relation
			 */
			// TODO: something is not right over here
			if (!dominates_single(r2.getMax(), r2.getMin(), a.get(i).values, phis)) {
				// // System.out.println("Dominate Single..");
				continue;
			}

			if (i != a.size() - 1) {
				/**
				 * TODO: CHANGE QUERY PREDICATES WHEN REQUIRED. For each query
				 * change the predicate relations over here. For this query its
				 * A.price < B.price and A.vol > B.vol. And the lists are sorted
				 * on the Timestamps. For other queries change it over here
				 * [0]--> timestamps ; [1]--> Price ; [2] --> Volume
				 */
				if (a.get(i).values[1] <= a.get(i + 1).values[1] && listbitsets[a.get(i + 1).id_a] != null
						&& !listbitsets[a.get(i + 1).id_a].isEmpty()) {
					// && a.get(i).values[3] <= a.get(i + 1).values[3]
					// && a.get(i).values[2] <= a.get(i + 1).values[2]
					int limit = 0;
					if (listbitsets[a.get(i).id_a] == null)
						listbitsets[a.get(i).id_a] = new OpenBitSet(valB.size());

					while (limit <= b.size() - 1) {
						if (listbitsets[a.get(i + 1).id_a].get(b.get(limit).id_b))

							listbitsets[a.get(i).id_a].set(b.get(limit).id_b);
						/**
						 * TODO: CHANGE QUERY PREDICATES WHEN REQUIRED For this
						 * query A.price < B.price and A.vol < B.vol
						 */
						else if (b.get(limit).values[1] > a.get(i).values[1]) {
							// && b.get(limit).values[3] > a.get(i).values[3]
							// && b.get(limit).values[2] > a.get(i).values[2]
							listbitsets[a.get(i).id_a].set((b.get(limit).id_b));

						}
						limit++;
					}

				} else {

					start = b.size() - 1;

				}

			}

			while (start >= 0) {
				/**
				 * TODO: CHANGE QUERY PREDICATES WHEN REQUIRED For this query
				 * A.time< B.time A.price < B.price and A.vol > B.vol
				 */
				if (b.get(start).values[0] > a.get(i).values[0] && (b.get(start).values[1] > a.get(i).values[1])) {
					// && (b.get(start).values[2] > a.get(i).values[2])
					// System.out.println("id of b: " + b.get(start).id_b);
					// System.out.println("b volume :" + b.get(start).values[2]
					// + " a value " + a.get(i).values[2]);
					// && (b.get(start).values[3] > a.get(i).values[3])
					if (listbitsets[a.get(i).id_a] == null)
						listbitsets[a.get(i).id_a] = new OpenBitSet(valB.size());
					listbitsets[a.get(i).id_a].set(b.get(start).id_b);
					start--;

				} else if (b.get(start).values[0] < a.get(i).values[0]) {

					break;
				} else {
					start--;

				}
			}

		}

	}

	@Override
	protected boolean isInrangeA(int[] min, int[] max, int[] point) {

		for (int i = 0; i < point.length; i++) {

			if (!(point[i] >= min[i] && point[i] <= max[i]))
				return false;
		}

		return true;

	}

	@Override
	public int dominates_generic(int[] max_r1, int[] min_r1, int[] max_r2, int[] min_r2, String[] phis) {
		// the phis contains the condition for each dimensions
		int correct = 0;
		for (int i = 0; i < phis.length; i++) {

			if (phis[i].equals("<") && i != 0 && max_r1[i] <= min_r2[i]) {
				// max of region1 < min of region2
				correct++;
			} else if (phis[i].equals("<") && i == 0 && max_r1[i] < min_r2[i]) {
				correct++;
			}

			else if (phis[i].equals(">") && min_r1[i] >= max_r2[i]) {
				correct++;
			}

		}

		if (correct == phis.length)
			return 1;

		correct = 0;
		for (int i = 0; i < phis.length; i++) {

			if (phis[i].equals("<") && i != 0 && min_r1[i] <= max_r2[i]) {
				// max of region1 < min of region2
				correct++;
			} else if (phis[i].equals("<") && i == 0 && min_r1[i] < max_r2[i]) {
				correct++;
			}

			else if (phis[i].equals(">") && max_r1[i] >= min_r2[i]) {
				correct++;
			}

		}

		if (correct == phis.length)
			return 2;

		return 0;

	}

	/**
	 * A Nested Join comparative function for the
	 * 
	 * @param valA
	 * @param valB
	 */
	public void nestedQueryJoin(ArrayList<ZIndex> valA, ArrayList<ZIndex> valB, SimpleEvent c,
			OpenBitSet[] listbitsets) {
		// System.out.println("Size of A " + valA.size());
		// System.out.println("Size of B " + valB.size());
		for (int i = 0; i < valB.size(); i++) {
			for (int j = 0; j < valA.size(); j++) {
				if (valA.get(j).values[0] < valB.get(i).values[0] && valA.get(j).values[1] < valB.get(i).values[1]) {
					/**
					 * Over here simply output the events with the c event
					 */
					if (listbitsets[i] == null)
						listbitsets[i] = new OpenBitSet(valB.size());

					listbitsets[i].set(j);
				}
			}
		}

	}

}
