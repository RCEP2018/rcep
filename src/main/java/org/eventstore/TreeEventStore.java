package org.eventstore;

import java.io.IOException;

import org.apache.jdbm.BTree;
import org.apache.jdbm.DBAbstract;
import org.apache.jdbm.DBMaker;
import org.zvalueencoder.ZIndex;

import uk.co.omegaprime.btreemap.BTreeMap;

public class TreeEventStore<Key, Value> extends EventStore {

	public final BTree<Key, Value> tree;

	@SuppressWarnings("unchecked")
	public TreeEventStore(int stype, int stime, int etime, String struc, int id) throws IOException {
		super(stype, stime, etime, struc, id);
		tree = (BTree<Key, Value>) BTree.createInstance((DBAbstract) DBMaker.openMemory().make());

	}

	@Override
	public void refreshInfo(int currTime) {
		if (currTime > this.getEndTime())
			this.setStoreType(0);

	}

	public void closeTree() throws IOException {
		tree.clear();
	}

	@Override
	public BTree tree() {

		return tree;
	}

	@Override
	public BTreeMap<ZIndex, Integer> getT() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BTreeMap<ZIndex, Integer> delete() {
		// TODO Auto-generated method stub
		return null;
	}

}
