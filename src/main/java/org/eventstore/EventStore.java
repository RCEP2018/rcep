package org.eventstore;

import org.apache.jdbm.BTree;
import org.zvalueencoder.ZIndex;

import uk.co.omegaprime.btreemap.BTreeMap;

public abstract class EventStore {

	private final int ID;

	private int storeStatus; // /0 for secondary, 1 for primary

	private int startTime;

	private int endTime;
	/// not required
	private String storeStructure;

	public EventStore(int stype, int stime, int etime, String sstruc, int id) {
		this.storeStatus = stype;
		this.startTime = stime;
		this.endTime = etime;
		ID = id;
		this.storeStructure = sstruc;
	}

	public abstract void refreshInfo(int currTime);

	public abstract BTree tree();

	public abstract BTreeMap<ZIndex, Integer> getT();

	public abstract BTreeMap<ZIndex, Integer> delete();

	public int getStoreType() {
		return storeStatus;
	}

	public void setStoreType(int storeType) {
		this.storeStatus = storeType;
	}

	public int getStartTime() {
		return startTime;
	}

	public void setStartTime(int startTime) {
		this.startTime = startTime;
	}

	public int getEndTime() {
		return endTime;
	}

	public void setEndTime(int endTime) {
		this.endTime = endTime;
	}

	public String getStoreStructure() {
		return storeStructure;
	}

	public void setStoreStructure(String storeStructure) {
		this.storeStructure = storeStructure;
	}

}
