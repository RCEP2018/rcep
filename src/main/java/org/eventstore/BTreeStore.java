package org.eventstore;

import org.apache.jdbm.BTree;
import org.zvalueencoder.ZIndex;

import uk.co.omegaprime.btreemap.BTreeMap;

public class BTreeStore extends EventStore {
	BTreeMap<ZIndex, Integer> eventStore;

	public BTreeStore(int stype, int stime, int etime, String sstruc, int id) {
		super(stype, stime, etime, sstruc, id);
		eventStore = BTreeMap.create();
	}

	@Override
	public void refreshInfo(int currTime) {
		if (currTime > this.getEndTime())
			this.setStoreType(0);

	}

	@Override
	public BTree tree() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BTreeMap<ZIndex, Integer> getT() {

		return eventStore;
	}

	@Override
	public BTreeMap<ZIndex, Integer> delete() {
		eventStore = null;
		eventStore = BTreeMap.create();
		return null;
	}

}
