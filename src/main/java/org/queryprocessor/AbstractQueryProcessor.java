package org.queryprocessor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.lucene.util.OpenBitSet;
import org.btreeUtils.MaxAndMinValues;
import org.btreeUtils.Value;
import org.event.Event;
import org.event.EventType;
import org.event.SimpleEvent;
import org.eventstore.BTreeStore;
import org.eventstore.EventStore;
import org.eventstore.TreeEventStore;
import org.outputmanager.QueryProcessorMeasure;
import org.windowsemantics.AbstractWindow;
import org.zvalueencoder.DimensionException;
import org.zvalueencoder.RangeSearchTest;
import org.zvalueencoder.ZIndex;
import org.zvalueencoder.Zregion;

public abstract class AbstractQueryProcessor implements Runnable {

	// final static Logger logger =
	// LoggerFactory.getLogger(AbstractQueryProcessor.class);
	// private LinkedBlockingQueue<Event> _inputQueue = null;
	private LinkedBlockingQueue<SimpleEvent> _inputQueue2;
	private final LinkedBlockingQueue<String> _outputQueue = new LinkedBlockingQueue<>();
	private CountDownLatch latch;
	// BTreeMap<ZIndex, Integer> eventStore = BTreeMap.create();
	// TreeSet<ZIndex> ts = new TreeSet<>();
	// protected final BTree<Integer, Value> tree;

	// protected final BTree<Long, int[]> intermediate;

	protected final EventStore store_1;
	protected final EventStore store_2;

	// protected final EventStore pattern_store_1;
	// protected final EventStore pattern_store_2;
	// TODO: remove this

	protected RangeSearchTest zc;
	// protected ZGenerator zval;
	protected final int dim;
	// protected ZGenerator zvalFull;
	////
	protected MaxAndMinValues minmax;

	protected MaxAndMinValues minmaxInter;

	protected AbstractWindow window;
	// TODO: remove this
	// protected static int[] masks = null;

	// public static BitSet[] BitSetMasks;

	////
	public AbstractQueryProcessor(LinkedBlockingQueue<SimpleEvent> i, CountDownLatch l, AbstractWindow w, int dimen)
			throws IOException {
		this._inputQueue2 = i;

		latch = l;
		this.dim = dimen;
		store_1 = new BTreeStore(1, 0, w.windowLength, "tree", 1);
		store_2 = new BTreeStore(0, 0, w.windowLength, "tree", 2);

		window = w;

		minmax = new MaxAndMinValues(dimen);

		minmaxInter = new MaxAndMinValues(dimen);
		zc = new RangeSearchTest();

	}

	// TODO:remove this
	public AbstractQueryProcessor(LinkedBlockingQueue<Event> i, CountDownLatch l, AbstractWindow w) throws IOException {
		// this._inputQueue = i;
		latch = l;
		store_1 = new TreeEventStore<ZIndex, Value>(1, 0, w.windowLength, "tree", 1);
		store_2 = new TreeEventStore<ZIndex, Value>(0, 0, w.windowLength, "tree", 2);
		// pattern_store_1 = new TreeEventStore<Long, int[]>(1, 0,
		// w.windowLength, "tree", 3);
		this.dim = 0;
		// pattern_store_2 = new TreeEventStore<Long, int[]>(0, 0,
		// w.windowLength, "tree", 4);

		// intermediate = BTree.createInstance((DBAbstract)
		// DBMaker.openMemory().make());
		window = w;

		// /Make these generic later

		// zval = new ZGenerator(2);

		// zvalFull = new ZGenerator(3);

		minmax = new MaxAndMinValues(3);

		minmaxInter = new MaxAndMinValues(3);

		/// New Updates
		/*
		 * int dim = 6; for (int n = 0; n < 32; n++) { masks[n] = 1 << n; }
		 * 
		 * BitSetMasks = new BitSet[dim - 1]; for (int j = 0; j <
		 * BitSetMasks.length; j++) {
		 * 
		 * BitSetMasks[j] = new BitSet(dim * 32);
		 * 
		 * int knowndim = j + 1;
		 * 
		 * BitSetMasks[j] = generateMask(BitSetMasks[j], dim, knowndim, dim *
		 * 32);
		 * 
		 * }
		 */
	}

	@Override
	public void run() {
		// logger.info("Starting Query Process");
		for (;;) {
			SimpleEvent e = null;
			try {
				e = _inputQueue2.take();
			} catch (InterruptedException exception) {
				// TODO Auto-generated catch block
				exception.printStackTrace();
			}
			if (e.getValues() == null) {
				break;
			} else {
				try {
					process(e);
				} catch (IOException | DimensionException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}

		finish();

	}

	protected void finish() {

		_outputQueue.add(EventType.POISONPILL.toString());
		latch.countDown();
	}

	protected abstract void process(SimpleEvent e) throws IOException, DimensionException;

	public LinkedBlockingQueue<String> get_outputQueue() {
		return _outputQueue;
	}

	public BitSet generateMask(BitSet b, int nbOfDimension, int t, int start) {

		int s = nbOfDimension - t;
		int skipping = s;

		int pointer1 = start - 1;
		int pointer2 = pointer1 - t;

		int bp = start - 1;

		while (pointer2 >= -t) {

			while (pointer1 > pointer2 && pointer1 >= 0) {

				b.set(bp, true);
				bp--;
				pointer1--;
			}
			while (skipping > 0) {
				if (bp > 0)
					b.set(bp, false);
				bp--;
				skipping--;
			}
			skipping = s;

			pointer2 = pointer2 - s;
			pointer1 = pointer2;
			pointer2 = pointer2 - t;
		}
		return b;

	}

	public EventStore getStore(SimpleEvent c) throws IOException {

		// EventStore store = (store_1.getStoreType() == 0) ? store_1 : store_2;
		// // System.out.println("Start Time " + window.startTimeofWindow + "
		// // EndTime " + store.getEndTime());
		//
		// if (c.values[0] - store.getStartTime() > window.windowLength) {
		//
		// // }
		//
		// // if (window.startTimeofWindow > store.getEndTime() * 2 &&
		// // store.getT().size() > 0) {
		//
		// // store.getT().clear(); // /change it later, making it null and
		// store.set();
		// store.setStoreType(0);
		//
		// System.out.println("Start Time " + window.startTimeofWindow + "
		// EndTime " + store.getEndTime());
		// // store= null;
		// // store = new BTreeStore(0, 0, w.windowLength, "tree", 1);
		//
		// } // reinitialising a new tree

		// EventStore store = (store_1.getStoreType() == 0) ? store_1 : store_2;
		//
		// if (c.values[0] - store.getStartTime() > window.windowLength) {
		// store.set();
		// EventStore primary_store = (store_1.getStoreType() == 1) ? store_1 :
		// store_2;
		// primary_store.setStoreType(0);
		// store.setStoreType(1);
		// return store;
		// }

		/**
		 * store type 0 --> secondary store type 1 --> primary
		 *
		 */
		EventStore store = (store_1.getStoreType() == 0) ? store_1 : store_2;
		// if (window.startTimeofWindow > store.getEndTime() * 2 &&
		// store.getT().size() > 0) {
		//
		// System.out.println("");
		// }

		if (c.values[0] - store.getEndTime() >= window.windowLength) {
			store.delete();
			EventStore primary_store = (store_1.getStoreType() == 1) ? store_1 : store_2;

			primary_store.setStoreType(0);
			store.setStoreType(1);
			store.setStartTime(c.values[0]);
			store.setEndTime(c.values[0] + window.windowLength);
			return store;
		}

		store = (store_1.getStoreType() == 1) ? store_1 : store_2;

		if (c.values[0] >= store.getEndTime()) {
			// /stop putting in this tree and select the other tree
			EventStore primary_store = (store_1.getStoreType() == 0) ? store_1 : store_2;

			store.setStoreType(0); // /make it a secondary store since the new
									// event will be outside its defined window
			primary_store.setStoreType(1); // make it the primary one and put
											// the event in this one
			primary_store.setStartTime(c.values[0]);
			primary_store.setEndTime(c.values[0] + window.windowLength);

			store = primary_store;
		}

		return store;

	}

	@SuppressWarnings("unchecked")
	public ArrayList<Value> queryStore(ZIndex[] ranges) throws IOException {

		// /search over both the stores if they are not empty, first select the
		// secondary store

		EventStore store = (store_1.getStoreType() == 0) ? store_1 : store_2;

		ArrayList<Value> val = null;
		if (!store.tree().hasData()) {

			val = store.tree().searchRange(ranges[0], ranges[1], minmaxInter);
		}

		// /select the primary datasource
		store = (store_1.getStoreType() == 1) ? store_1 : store_2;

		if (val != null)
			val.addAll(store.tree().searchRange(ranges[0], ranges[1], minmaxInter));
		else
			val = store.tree().searchRange(ranges[0], ranges[1], minmaxInter);

		return val;
	}

	// TODO: Dont forget to change at line 308 for each query.
	public abstract int dominates_generic(int[] max_r1, int[] min_r1, int[] max_r2, int[] min_r2, String[] phis);

	/**
	 * To create the Z-regions
	 */

	public ArrayList<Zregion> newCreateRegions(SortedMap<ZIndex, Integer> b, ZIndex[] ranges, int dim, int e_type,
			ArrayList<ZIndex> val, ArrayList<Zregion> finallist) {
		// ArrayList<Zregion> finallist = new ArrayList<>();

		Iterator it = b.keySet().iterator();

		ZIndex z = null;
		ZIndex zprev = null;
		int dis = 0;
		int i = val.size();
		int test = 0;
		if (!it.hasNext())
			return finallist;

		if (finallist.isEmpty())
			finallist.add(new Zregion(this.dim));

		Zregion r1 = finallist.get(finallist.size() - 1);
		// System.out.println("Key Set Size " + b.keySet().size());

		do {
			z = (ZIndex) it.next();

			if (e_type == 1 && !isInrangeA(ranges[0].getValues(), ranges[1].getValues(), z.getValues())) {
				continue;
			} else if (e_type != 1 && !isInrangeB(ranges[0].getValues(), ranges[1].getValues(), z.getValues())) {
				continue;
			}

			// if (isInrange(ranges[0].getValues(), ranges[1].getValues(),
			// z.getValues())) {
			if (dis >= 5 || zprev != null && z.group_check != zprev.group_check) {
				/// create a new regions
				// dis >= 10 ||
				finallist.add(new Zregion(dim));
				r1 = finallist.get(finallist.size() - 1);
				dis = 0;
			}

			dis++;
			this.minmaxInter.updateMaxMin(z.getValues());
			if (e_type == 1)
				z.id_a = i;
			else
				z.id_b = i;

			val.add(z);
			r1.add(z);
			i = val.size();
			zprev = z;

			// }
			test++;
		} while (it.hasNext());

		// System.out.println("Original " + test);
		// System.out.println("Needed " + i);
		return finallist;
	}

	protected abstract boolean isInrangeB(int[] min, int[] max, int[] point);

	/**
	 * Setting all the bits once
	 */
	public void setBits(OpenBitSet[] listbitsets, int _startRegion1, int _endRegion1, int _startRegion2,
			int _endRegion2, int size) {
		for (int i = _startRegion1; i <= _endRegion1; i++) {
			// For each such subregion, i.e. an entry i, set the bits in the
			// given range.

			if (listbitsets[i] == null)
				listbitsets[i] = new OpenBitSet(size);
			listbitsets[i].set(_startRegion2, _endRegion2 + 1);

		}
	}

	/**
	 * Regions based joins Algo 3 from the paper
	 */
	protected void implementJoins(ArrayList<Zregion> region1, ArrayList<Zregion> region2, ArrayList<ZIndex> valA,
			ArrayList<ZIndex> valB, OpenBitSet[] listbitsets, String[] phis) {

		// where each bitset is equal to the size of the fullregion 2;

		// for (int i = 0; i < listbitsets.length; i++) {
		// listbitsets[i] = new OpenBitSet(valB.size());
		// }

		// Arrays.fill(listbitsets, new BitSet(valB.size()));
		/**
		 * r1 is the outer relation and r2 is the inner one.
		 */
		int _startRegion1 = 0, _startRegion2 = 0;

		int _endRegion1 = 0, _endRegion2 = 0;
		for (Zregion r1 : region1) {

			_endRegion1 = _startRegion1 + r1.getValues().size() - 1;

			/**
			 * If the previous region, i.e. prev(r1) dominates this one then we
			 * can reuse its results as well. Add this if statement in a new
			 * function over here
			 */
			_startRegion2 = 0;
			for (Zregion r2 : region2) {

				_endRegion2 = _startRegion2 + (r2.getValues().size() - 1);

				/**
				 * Still have to edit it for the region that cannot be joined
				 * together
				 */
				int dom = dominates_generic(r1.max, r1.min, r2.max, r2.min, phis);
				// System.out.println("Dominance " + dom);
				if (dom == 1) {

					// System.out.println("Here Setting Bits");
					/// set all the bits to 1 for each entry in r1 for r2
					this.setBits(listbitsets, _startRegion1, _endRegion1, _startRegion2, _endRegion2, valB.size());

					// QueryProcessorMeasure.costOfA = valA.size() -
					// r1.values.size();// (_endRegion1
					// -
					// _startRegion1
					// +
					// 1);
					// QueryProcessorMeasure.costOfB = valB.size() -
					// r2.values.size();// (_endRegion2
					// -
					// _startRegion2
					// +
					// 1);
					QueryProcessorMeasure.numofBatchSetOperations++;
					QueryProcessorMeasure.batchCost = QueryProcessorMeasure.batchCost + r1.values.size();
					// + (QueryProcessorMeasure.costOfB *
					// QueryProcessorMeasure.costOfA)
					// + QueryProcessorMeasure.costOfA;

				} else if (dom == 2) {

					// else if (dom > 0) /// TODO: Not sure about this?

					// {

					// then employ the standard join algorithm
					// send the region r1 and r2 to the older function which
					// will
					// join it in a pariwise manner.
					QueryProcessorMeasure.costOfA = r1.values.size();
					QueryProcessorMeasure.costOfB = r2.values.size();

					QueryProcessorMeasure.batchCost = QueryProcessorMeasure.batchCost
							+ (QueryProcessorMeasure.costOfB * QueryProcessorMeasure.costOfA);

					joinSingleRegions(listbitsets, r1, r2, phis, valB);

					// }
				} else {
					QueryProcessorMeasure.costOfA = r1.values.size();// (_endRegion1
																		// -
																		// _startRegion1
																		// +
																		// 1);
					QueryProcessorMeasure.costOfB = r2.values.size();// (_endRegion2
																		// -
																		// _startRegion2
					QueryProcessorMeasure.nunOfBatchUnsetOperations++; // +
					// 1);

					// QueryProcessorMeasure.batchCost =
					// QueryProcessorMeasure.batchCost
					// + (QueryProcessorMeasure.costOfB *
					// QueryProcessorMeasure.costOfA);

				}
				_startRegion2 = _endRegion2 + 1;
			}

			_startRegion1 = _endRegion1 + 1;
		}

	}

	protected abstract void joinSingleRegions(OpenBitSet[] listbitsets, Zregion r1, Zregion r2, String[] phis,
			ArrayList<ZIndex> valB);

	protected abstract boolean isInrangeA(int[] min, int[] max, int[] point);

	public boolean dominates_single(int[] max_r, int[] min_r, int[] point, String[] phis) {
		// the phis contains the condition for each dimensions

		for (int i = 0; i < phis.length; i++) {

			if (phis[i].equals("<") && !(point[i] <= max_r[i])) {
				return false;
			} else if (phis[i].equals(">") && !(point[i] >= min_r[i])) {
				return false;
			}

		}

		return true;

	}

	protected void updateMaxMin(int[] val) {

		for (int i = 0; i < val.length; i++) {
			if (val[i] > minmax.max[i]) {

				minmax.max[i] = val[i];
			}

			if (val[i] < minmax.min[i]) {

				minmax.min[i] = val[i];
			}
		}

	}
}
