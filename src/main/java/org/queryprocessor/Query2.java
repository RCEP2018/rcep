package org.queryprocessor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.lucene.util.OpenBitSet;
import org.event.SimpleEvent;
import org.queryprocessor.Query1.TimeComparator;
import org.windowsemantics.AbstractWindow;
import org.zvalueencoder.DimensionException;
import org.zvalueencoder.QueriesGenerator;
import org.zvalueencoder.RangeSearchTest;
import org.zvalueencoder.ZIndex;
import org.zvalueencoder.Zregion;

import com.github.davidmoten.rtree.Entry;
import com.github.davidmoten.rtree.RTree;
import com.github.davidmoten.rtree.geometry.Geometries;
import com.github.davidmoten.rtree.geometry.Geometry;

import uk.co.omegaprime.btreemap.BTreeMap;

public class Query2 extends AbstractQueryProcessor {
	int test_counter = 0;
	RangeSearchTest zc;
	RTree<Integer, Geometry> tree;

	QueriesGenerator qg;

	public Query2(LinkedBlockingQueue<SimpleEvent> i, CountDownLatch l, AbstractWindow w, int dimen)
			throws IOException {
		super(i, l, w, dimen);

		zc = new RangeSearchTest();
		tree = RTree.create();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int dominates_generic(int[] max_r1, int[] min_r1, int[] max_r2, int[] min_r2, String[] phis) {
		// the phis contains the condition for each dimensions
		int correct = 0;
		for (int i = 0; i < phis.length; i++) {

			if (phis[i].equals("<") && i != 0 && max_r1[i] <= min_r2[i]) {
				// max of region1 < min of region2
				correct++;
			} else if (phis[i].equals("<") && i == 0 && max_r1[i] < min_r2[i]) {
				correct++;
			}
			// TODO: change for different types of comparison between
			else if (phis[i].equals(">") && min_r1[i] >= max_r2[i]) {
				correct++;
			}

		}

		return correct;

	}

	@Override
	protected void process(SimpleEvent ce) throws IOException, DimensionException {

		// QueryImplementationRTree(ce, this.dim);
		// try {
		// QueryImplementation(ce, this.dim);
		// // QueryImplementation2(ce, this.dim);
		// } catch (CloneNotSupportedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		ZIndex key = new ZIndex(ce.values);

		// window.updateWindow(ce.values[0]);
		// Entry<Object, Rectangle> entry1 = e(1);
		// tree = tree.add(test_counter, Geometries.point(ce.values[0],
		// ce.values[1]));

		// eventStore.put(key, 0);
		// ts.add(key);
		// System.out.println("Height of the Tree is: " +
		// store.tree().get_height());
		updateMaxMin(ce.values);

		test_counter++;
		// System.out.println(test_counter);

	}

	/**
	 * Test with query generation
	 * 
	 * @throws CloneNotSupportedException
	 **/

	// private void QueryImplementationGen(SimpleEvent ce, int dim) throws
	// CloneNotSupportedException {
	// if (eventStore.size() < 2)
	// return;
	//
	// if (eventStore.size() < 2)
	// return;
	//
	// if (eventStore.size() == 7) {
	// System.out.println();
	// }
	// ZIndex[] ranges = createRangesForB(ce);
	//
	// qg = new QueriesGenerator(this.eventStore, zc);
	// qg.generateQueries(ranges);
	// SortedMap<ZIndex, Integer> b = null;
	// ArrayList<Zregion> regionsForB = new ArrayList<>(10);
	// ArrayList<ZIndex> valB = new ArrayList<>();
	// for (ZIndex[] q : qg.queries) {
	// b = eventStore.subMap(q[0], true, q[1], true);
	// if (b != null || !b.isEmpty()) {
	// newCreateRegions(b, ranges, this.dim, 2, valB, regionsForB);
	// }
	// }
	//
	// }

	/**
	 * Main implemantation of the Query
	 * 
	 * @throws CloneNotSupportedException
	 */

	// private void QueryImplementation2(SimpleEvent ce, int dim)
	// throws IOException, DimensionException, CloneNotSupportedException {
	// if (eventStore.size() < 2)
	// return;
	//
	// if (eventStore.size() == 7) {
	// System.out.println();
	// }
	// ZIndex[] ranges = createRangesForB(ce);
	//
	// ArrayList<ZIndex> valB = createRegionsOptimised(ranges, eventStore);
	// if (valB.isEmpty())
	// return;
	// }

	private void QueryImplementationRTree(SimpleEvent ce, int dim) {

		// System.out.println("Size: " + tree.size());
		if (tree.size() < 2)
			return;

		ZIndex[] ranges = createRangesForB(ce);

		// rx.Observable<Entry<Integer, Geometry>> re =
		// tree.search(Geometries.rectangle(ranges[0].values[0],
		// ranges[0].values[1], ranges[1].values[0], ranges[1].values[1]));
		try {
			Iterable<Entry<Integer, Geometry>> it = tree.search(Geometries.rectangle(ranges[0].values[0],
					ranges[0].values[1], ranges[1].values[0], ranges[1].values[1])).toBlocking().toIterable();
			Iterator<Entry<Integer, Geometry>> itr1 = it.iterator();

			while (itr1.hasNext()) {
				Entry<Integer, Geometry> er = itr1.next();

				// System.out.println(er.geometry());

			}
		} catch (Exception ec) {

		}
		// Observable<Entry<T, Geometry>> results =
		// tree.search(Geometries.rectangle(0, 0, 2, 2));
	}

	// private void QueryImplementation(SimpleEvent ce, int dim)
	// throws IOException, DimensionException, CloneNotSupportedException {
	// // We are having two range queries for each event tooo MUCH!!
	// // Range queries are the bottle neck of the system
	// if (eventStore.size() < 2)
	// return;
	//
	// if (eventStore.size() == 7) {
	// System.out.println();
	// }
	// ZIndex[] ranges = createRangesForB(ce);
	// // ranges[0].values
	// SortedMap<ZIndex, Integer> b = null;
	// // b = eventStore.subMap(ranges[0], true, ranges[1], true);
	//
	// ArrayList<ZIndex> valB = new ArrayList<>();
	// ArrayList<ZIndex[]> queries = zc.decomposeintoTwo(ranges[0], ranges[1]);
	//
	// queries.addAll(zc.decomposeintoTwo(queries.get(0)[0],
	// queries.get(0)[1]));
	// queries.addAll(zc.decomposeintoTwo(queries.get(1)[0],
	// queries.get(1)[1]));
	// queries.remove(0);
	// queries.remove(1);
	// ArrayList<Zregion> regionsForB = new ArrayList<>(10);
	//
	// for (ZIndex[] q : queries) {
	// b = eventStore.subMap(q[0], true, q[1], true);
	// if (b != null || !b.isEmpty()) {
	// newCreateRegions(b, ranges, this.dim, 2, valB, regionsForB);
	// }
	// }
	//
	// // b.keySet().removeIf(e -> isInrange(ranges[0].values,
	// // ranges[1].values, e.values));
	//
	// // ArrayList<Zregion> regionsForB = newCreateRegions(b, ranges,
	// // this.dim, 2, valB);
	//
	// // NavigableSet<ZIndex> b2 = ts.subSet(ranges[0], true, ranges[1],
	// // true);
	// // regionsForB = newCreateRegions2(b2, ranges, this.dim, 2, valB);
	// // createRegionsOptimised(ranges, eventStore);
	// if (valB.isEmpty())
	// return;
	//
	// ranges = createRangesForA(null);
	//
	// ArrayList<ZIndex> valA = new ArrayList<>();
	// queries = zc.decomposeintoTwo(ranges[0], ranges[1]);
	//
	// queries.addAll(zc.decomposeintoTwo(queries.get(0)[0],
	// queries.get(0)[1]));
	// queries.addAll(zc.decomposeintoTwo(queries.get(1)[0],
	// queries.get(1)[1]));
	// queries.remove(0);
	// queries.remove(1);
	// ArrayList<Zregion> regionsForA = new ArrayList<>(10);
	//
	// SortedMap<ZIndex, Integer> a = null;
	//
	// for (ZIndex[] q : queries) {
	// a = eventStore.subMap(q[0], true, q[1], true);
	// if (a != null || !a.isEmpty()) {
	// newCreateRegions(a, ranges, this.dim, 2, valA, regionsForA);
	// }
	// }
	//
	// if (valA.isEmpty())
	// return;
	// // System.out.println("size B " + valB.size());
	// // System.out.println("size A " + valA.size());
	// /*
	// * SortedMap<ZIndex, Integer> b = eventStore.subMap(ranges[0],
	// * ranges[1]);
	// *
	// * if (b == null || b.isEmpty()) return; ArrayList<ZIndex> valB =
	// * createRegionsOptimised(ranges, b); // new // ArrayList<>();
	// * ArrayList<Zregion> regionsForB = new ArrayList<>(); // =
	// * newCreateRegions(b, ranges, this.dim, 2, valB); if (valB.isEmpty())
	// * return;
	// *
	// *
	// *
	// * if (a.isEmpty()) return; // System.out.println("Size of A " +
	// * a.size()); ArrayList<ZIndex> valA = new ArrayList<>(); //
	// * ArrayList<>(); ArrayList<Zregion> regionsForA = new ArrayList<>(); //
	// * newCreateRegions(a, ranges, dim, 1, valA); /// While going through A
	// * create the First Region and join it?? // ArrayList<Value> valA =
	// * queryStore(ranges); if (valA.isEmpty()) return;
	// */
	// /**
	// * Create the regions based on the various bits
	// */
	// /*
	// * System.out.println("size of A regions: " + valA.size());
	// *
	// * System.out.println("sie of B regions: " + valB.size());
	// *
	// * if (valA.size() == 1479) { System.out.println(); }
	// */
	// OpenBitSet[] listbitsets = new OpenBitSet[valA.size()];
	//
	// // FastBitSet[] listbitsets = new FastBitSet[valA.size()];
	// // long[] ba = new long[10];
	// // long[][] bits = new long[valA.size()][valB.size()];
	//
	// // int[][] n_listbitsets = new int[valA.size()][valB.size()];
	// /**
	// * new String[] { "<", "<", ">" } This means A.time < B.time, A.price <
	// * B.price and A.vol >B.vol
	// */
	// /**
	// * Our join Algorithm
	// */
	// implementJoins(regionsForA, regionsForB, valA, valB, listbitsets, new
	// String[] { "<", "<" });
	//
	// /**
	// * Nested Join Algorithm
	// */
	// // nestedQueryJoin(valA, valB, ce, listbitsets);
	// /**
	// * IE Join Algorithm from the VLDB paper
	// */
	// // ieJoin.IEJoin3(valA, valB, null);
	// /**
	// * If there is a Kleene plus sign involved in the query
	// */
	//
	// kleeneplusImpl(valA, valB, listbitsets, ce.values);
	// }

	/**
	 * Regions based joins Algo 3 from the paper
	 */
	protected void implementJoins(ArrayList<Zregion> region1, ArrayList<Zregion> region2, ArrayList<ZIndex> valA,
			ArrayList<ZIndex> valB, OpenBitSet[] listbitsets, String[] phis) {

		// where each bitset is equal to the size of the fullregion 2;

		// for (int i = 0; i < listbitsets.length; i++) {
		// listbitsets[i] = new OpenBitSet(valB.size());
		// }

		// Arrays.fill(listbitsets, new BitSet(valB.size()));
		/**
		 * r1 is the outer relation and r2 is the inner one.
		 */
		int _startRegion1 = 0, _startRegion2 = 0;

		int _endRegion1 = 0, _endRegion2 = 0;
		for (Zregion r1 : region1) {

			_endRegion1 = _startRegion1 + r1.getValues().size() - 1;

			/**
			 * If the previous region, i.e. prev(r1) dominates this one then we
			 * can reuse its results as well. Add this if statement in a new
			 * function over here
			 */
			_startRegion2 = 0;
			for (Zregion r2 : region2) {

				_endRegion2 = _startRegion2 + (r2.getValues().size() - 1);

				/**
				 * Still have to edit it for the region that cannot be joined
				 * together
				 */
				int dom = dominates_generic(r1.max, r1.min, r2.max, r2.min, phis);
				// System.out.println("Dominance " + dom);
				if (dom == phis.length) {

					// System.out.println("Here Setting Bits");
					/// set all the bits to 1 for each entry in r1 for r2
					this.setBits(listbitsets, _startRegion1, _endRegion1, _startRegion2, _endRegion2, valB.size());
					// setBitsFunction
				}

				else if (dom > 0) /// TODO: Not sure about this?

				{

					// then employ the standard join algorithm
					// send the region r1 and r2 to the older function which
					// will
					// join it in a pariwise manner.
					joinSingleRegions(listbitsets, r1, r2, phis, valB);

				}

				_startRegion2 = _endRegion2 + 1;
			}

			_startRegion1 = _endRegion1 + 1;
		}

	}

	/**
	 * Simple Algo 1 from the paper
	 * 
	 * @return
	 */

	public void joinSingleRegions(OpenBitSet[] listbitsets, Zregion r1, Zregion r2, String[] phis,
			ArrayList<ZIndex> valB) {

		/// Sort them acording to the timestamps to make it event sequences
		List<ZIndex> a = r1.getValues();
		List<ZIndex> b = r2.getValues();

		if (r1.sorted == 0) {
			Collections.sort(a, new TimeComparator());
			r1.sorted = 1;
		}

		if (r2.sorted == 0) {
			Collections.sort(b, new TimeComparator());
			r2.sorted = 1;
		}

		// int numofsetbits = 0;

		int start = b.size() - 1;
		/// loop over A and B

		for (int i = a.size() - 1; i >= 0; i--) {
			// numofsetbits = 0;

			/**
			 * Check if if this value can actually match with any other value in
			 * B or not using the max and min of region r2, Depending on the
			 * query, the phis will be different. Note that it supports strickly
			 * less than or greater than relation
			 */
			// TODO: something is not right over here
			if (!dominates_single(r2.getMax(), r2.getMin(), a.get(i).values, phis)) {
				// // System.out.println("Dominate Single..");
				continue;
			}

			if (i != a.size() - 1) {
				/**
				 * TODO: CHANGE QUERY PREDICATES WHEN REQUIRED. For each query
				 * change the predicate relations over here. For this query its
				 * A.price < B.price and A.vol > B.vol. And the lists are sorted
				 * on the Timestamps. For other queries change it over here
				 * [0]--> timestamps ; [1]--> Price ; [2] --> Volume
				 */
				if (a.get(i).values[1] <= a.get(i + 1).values[1] && a.get(i).values[2] >= a.get(i + 1).values[2]
						&& listbitsets[a.get(i + 1).id_a] != null && !listbitsets[a.get(i + 1).id_a].isEmpty()) {

					int limit = 0;
					if (listbitsets[a.get(i).id_a] == null)
						listbitsets[a.get(i).id_a] = new OpenBitSet(valB.size());

					while (limit <= b.size() - 1) {
						if (listbitsets[a.get(i + 1).id_a].get(b.get(limit).id_b))

							listbitsets[a.get(i).id_a].set(b.get(limit).id_b);
						/**
						 * TODO: CHANGE QUERY PREDICATES WHEN REQUIRED For this
						 * query A.price < B.price and A.vol > B.vol
						 */
						else if (b.get(limit).values[1] > a.get(i).values[1]
								&& b.get(limit).values[2] < a.get(i).values[2]) {

							listbitsets[a.get(i).id_a].set((b.get(limit).id_b));

						}
						limit++;
					}

				} else {

					start = b.size() - 1;

				}

			}

			while (start >= 0) {
				/**
				 * TODO: CHANGE QUERY PREDICATES WHEN REQUIRED For this query
				 * A.time< B.time A.price < B.price and A.vol > B.vol
				 */
				if (b.get(start).values[0] > a.get(i).values[0] && (b.get(start).values[1] > a.get(i).values[1])
						&& (b.get(start).values[2] < a.get(i).values[2])) {

					// System.out.println("id of b: " + b.get(start).id_b);
					// System.out.println("b volume :" + b.get(start).values[2]
					// + " a value " + a.get(i).values[2]);
					if (listbitsets[a.get(i).id_a] == null)
						listbitsets[a.get(i).id_a] = new OpenBitSet(valB.size());
					listbitsets[a.get(i).id_a].set(b.get(start).id_b);
					start--;
					// numofsetbits++;
				} else if (b.get(start).values[0] < a.get(i).values[0]) {

					break;
				} else {
					start--;

				}
			}

		}

	}

	/**
	 * This query is V-shaped pattern over only the stock prices and each
	 * matched pattern is for a specific company, i.e. the id of the company
	 */

	private void kleeneplusImpl(ArrayList<ZIndex> valA, ArrayList<ZIndex> valB, OpenBitSet[] listbitsets, int[] valC) {

		// for each value of A create the Kleene+ matches using the bitset list
		// and event int[] valC
		// valA.parallelStream().
		// int i = 0;
		// valA.stream().parallel().forEach((k) -> {
		//
		// if (listbitsets[k.id_a] != null && !listbitsets[k.id_a].isEmpty())
		// kleeneplus(k.getValues(), listbitsets[k.id_a], valC, valB);
		//
		// });

		for (int i = 0; i < valA.size(); i++) {

			if (listbitsets[i] != null && !listbitsets[i].isEmpty())
				kleeneplus(valA.get(i).getValues(), listbitsets[i], valC, valB);

		}

	}

	public void checkNextOperator(OpenBitSet b, ArrayList<ZIndex> valB) {
		int index = 0;
		List<ZIndex> sortedBs = new ArrayList<>();
		for (int j = b.nextSetBit(b.length()); j >= 0; j = b.nextSetBit(j - 1)) {
			sortedBs.add(valB.get(index));

			if (j == Integer.MIN_VALUE) {
				break; // or (i+1) would overflow
			}
		}
		if (sortedBs.isEmpty())
			return;

		Collections.sort(sortedBs, new TimeComparator());
		ZIndex older = sortedBs.get(0);
		for (int i = 1; i < sortedBs.size(); i++) {

			if (older.values[1] > sortedBs.get(i).values[1]) {
				b.fastClear(sortedBs.get(i).id_b);
			} else {
				older = sortedBs.get(i);
			}
		}

	}

	/**
	 * To generate Kleene+ matches
	 */

	public void kleeneplus(int[] a, OpenBitSet b, int[] c, ArrayList<ZIndex> valB) {

		checkNextOperator(b, valB);
		if (b.cardinality() > 8) {
			System.out.println("FUCK: Kleene_+ " + b.cardinality());
			return;
		}
		int max = 1 << b.cardinality();
		StringBuilder sb = new StringBuilder();
		for (int i = 1; i < max; i++) {
			int index = 0;
			sb = new StringBuilder();
			sb.append("a ");
			sb.append(Arrays.toString(a));
			sb.append(" ; ");
			int prev_price = Integer.MIN_VALUE;
			// get_outputQueue().add(Integer.toString(a[0]));
			for (int j = i; j > 0; j >>= 1) {
				if ((j & 1) == 1) {

					int index_2 = 0;

					for (int k = 0; k <= index; k++) {
						index_2 = b.nextSetBit(index_2);
						index_2++;
					}

					/**
					 * Implementation of Next operator
					 */

					// if (valB.get(index_2 - 1).getValues()[1] >= prev_price) {
					// sb.append("b ");
					// sb.append(Arrays.toString(valB.get(index_2 -
					// 1).getValues()));
					// sb.append(" ; ");
					// prev_price = valB.get(index_2 - 1).getValues()[1];
					// }

				}
				index++;
			}
			// sb.append(" ; ");
			sb.append("c ");
			sb.append(Arrays.toString(c));
			sb.append("\n");
			// this.total_matches++;
			// System.out.println("Total Matches: " + this.total_matches);
			// System.out.println(sb.toString());
			// get_outputQueue().add(sb.toString());
			// get_outputQueue().add("\n");
			// test_counter++;

		}
	}

	public boolean dominates_single(int[] max_r, int[] min_r, int[] point, String[] phis) {
		// the phis contains the condition for each dimensions

		for (int i = 0; i < phis.length; i++) {

			if (phis[i].equals("<") && !(point[i] < max_r[i])) {
				return false;
			} else if (phis[i].equals(">") && !(point[i] > min_r[i])) {
				return false;
			}

		}

		return true;

	}

	private ZIndex[] createRangesForB(SimpleEvent ce) {

		// B.price > C.price
		ZIndex minZRange = new ZIndex(new int[] { minmax.min[0], ce.values[1] - 1, minmax.min[2], ce.values[3] });
		// ZIndex minZRange = new ZIndex(new int[] { minmax.min[0], ce.values[1]
		// - 1, minmax.min[2], ce.values[3] });

		// ZIndex minZRange = new ZIndex(new int[] { minmax.min[0], ce.values[1]
		// - 1 });

		// ce.values[0]-1 (or +1 for minrange) is added because in B+tree the
		// comparison is based on
		// <= or >=
		ZIndex maxZRange = new ZIndex(new int[] { ce.values[0] - 1, minmax.max[1], minmax.max[2], ce.values[3] });
		// ZIndex maxZRange = new ZIndex(new int[] { ce.values[0] - 1,
		// minmax.max[1] });
		// ZIndex maxZRange = new ZIndex(new int[] { minmax.max[0],
		// minmax.max[1], ce.values[2], ce.values[3] - 1 });
		return new ZIndex[] { minZRange, maxZRange };
	}

	private ZIndex[] createRangesForA(int[] valb) {
		// /For now hardcoded
		/**
		 * For A Sequence; the event of A has timestamp less than B, price is
		 * less than B and from the same company, i.e. value[3]
		 **/
		ZIndex minZRange = new ZIndex(new int[] { minmax.min[0], minmax.min[1], minmax.min[2], minmaxInter.max[3] });
		// ce.values[0]-1 is added beacuse in B+tree the comparison is based on
		// <= or >=
		ZIndex maxZRange = new ZIndex(
				new int[] { minmaxInter.max[0] - 1, minmaxInter.max[1] - 1, minmax.max[2], minmaxInter.max[3] });

		return new ZIndex[] { minZRange, maxZRange };

	}

	// private void updateMaxMin(int[] val) {
	//
	// if (val[0] > minmax.max[0]) {
	//
	// minmax.max[0] = val[0];
	// }
	//
	// if (val[0] < minmax.min[0]) {
	//
	// minmax.min[0] = val[0];
	// }
	//
	// if (val[1] > minmax.max[1]) {
	//
	// minmax.max[1] = val[1];
	// }
	//
	// if (val[1] < minmax.min[1]) {
	//
	// minmax.min[1] = val[1];
	// }
	//
	// if (val[2] > minmax.max[2]) {
	//
	// minmax.max[2] = val[2];
	// }
	//
	// if (val[2] < minmax.min[2]) {
	//
	// minmax.min[2] = val[2];
	// }
	//
	// if (val[3] > minmax.max[3]) {
	//
	// minmax.max[3] = val[3];
	// }
	//
	// if (val[3] < minmax.min[3]) {
	//
	// minmax.min[3] = val[3];
	// }
	//
	// }

	private ArrayList<ZIndex> createRegionsOptimised(ZIndex[] ranges, BTreeMap<ZIndex, Integer> allVal)
			throws CloneNotSupportedException {
		/// create a new ArrayList
		HashSet<ZIndex> hset = new HashSet<ZIndex>();
		// ArrayList<ZIndex> val = new ArrayList<>();

		ZIndex lastreported = (ZIndex) ranges[0].clone();
		zc.some_points_in_rectangle(ranges[0], ranges[1], allVal, lastreported, hset, 50);
		// System.out.println("Number of ZIndex: " + hset.size());
		return new ArrayList<ZIndex>(hset);
	}

	protected boolean isInrangeB(int[] min, int[] max, int[] point) {
		if (!(point[0] > min[0] && point[0] <= max[0]))
			return false;
		for (int i = 1; i < point.length - 1; i++) {

			if (!(point[i] >= min[i] && point[i] <= max[i]))
				return false;
		}

		if ((point[3] != min[3]))
			return false;

		return true;
	}

	@Override
	protected boolean isInrangeA(int[] min, int[] max, int[] point) {
		// TODO Auto-generated method stub
		return false;
	}
}
