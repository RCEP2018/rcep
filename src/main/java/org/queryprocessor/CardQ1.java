package org.queryprocessor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.SortedMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.lucene.util.OpenBitSet;
import org.event.SimpleEvent;
import org.eventstore.EventStore;
import org.outputmanager.QueryProcessorMeasure;
import org.queryprocessor.Query1.TimeComparator;
import org.rcep.AbstractApp;
import org.windowsemantics.AbstractWindow;
import org.zvalueencoder.DimensionException;
import org.zvalueencoder.ZIndex;
import org.zvalueencoder.Zregion;

public class CardQ1 extends AbstractQueryProcessor {
	int event_processed = 0;

	public CardQ1(LinkedBlockingQueue<SimpleEvent> i, CountDownLatch l, AbstractWindow w, int dimen)
			throws IOException {
		super(i, l, w, dimen);

	}

	@Override
	protected void process(SimpleEvent e) throws IOException, DimensionException {
		try {
			queryProcessor(e);
		} catch (CloneNotSupportedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ZIndex key = new ZIndex(e.values);

		window.updateWindow(e.values[0]);
		// Entry<Object, Rectangle> entry1 = e(1);
		// tree = tree.add(test_counter, Geometries.point(ce.values[0],
		// ce.values[1]));

		EventStore store = getStore(e);

		store.getT().put(key, 0);

		updateMaxMin(e.values);

		// test_counter++;

	}

	private void addLastB(ArrayList<ZIndex> valB, ArrayList<Zregion> regionsForB, SimpleEvent b) {
		minmaxInter.updateMaxMin(b.values);
		ZIndex newb = new ZIndex(b.values);
		newb.id_b = valB.size();
		valB.add(newb);

		if (!regionsForB.isEmpty())
			regionsForB.get(regionsForB.size() - 1).add(newb);
		else {

			Zregion r = new Zregion(this.dim);
			r.add(newb);
			regionsForB.add(r);
		}
	}

	private void queryProcessor(SimpleEvent b) throws CloneNotSupportedException {
		ArrayList<ZIndex> valB = new ArrayList<>(1000);

		if (b.values[0] == 1513496192) {
			System.out.println();
		}
		ZIndex[] ranges = createRangesForB(b);
		this.minmaxInter.reset();
		ArrayList<Zregion> regionsForB = new ArrayList<>(10);

		queryStores(b, dim, valB, regionsForB, ranges, 2);
		// add the newly arrived b in here
		addLastB(valB, regionsForB, b);
		// if (valB.isEmpty())
		// return;

		ranges = createRangesForA(b);

		ArrayList<ZIndex> valA = new ArrayList<>(1000);

		ArrayList<Zregion> regionsForA = new ArrayList<>(10);

		queryStores(b, dim, valA, regionsForA, ranges, 1);

		if (valA.isEmpty())
			return;

		QueryProcessorMeasure.costOfCrossProduct = QueryProcessorMeasure.costOfCrossProduct
				+ (valA.size() * valB.size());
		QueryProcessorMeasure.numOfJoins++;

		OpenBitSet[] listbitsets = new OpenBitSet[valA.size()];

		/**
		 * new String[] { "<", "<", ">" } This means A.time < B.time, A.price <
		 * B.price and A.vol >B.vol
		 */
		/**
		 * Our join Algorithm
		 */
		implementJoins(regionsForA, regionsForB, valA, valB, listbitsets, new String[] { "<", ">" });

		/**
		 * Nested Join Algorithm
		 */
		// nestedQueryJoin(valA, valB, ce, listbitsets);
		/**
		 * IE Join Algorithm from the VLDB paper
		 */
		// ieJoin.IEJoin3(valA, valB, null);
		/**
		 * If there is a Kleene plus sign involved in the query
		 */

		kleeneplusImpl(valA, valB, listbitsets, b.values);
	}

	private void kleeneplusImpl(ArrayList<ZIndex> valA, ArrayList<ZIndex> valB, OpenBitSet[] listbitsets, int[] valC) {

		for (int i = 0; i < valA.size(); i++) {

			if (listbitsets[i] != null && !listbitsets[i].isEmpty())
				kleeneplus(valA.get(i).getValues(), listbitsets[i], valC, valB);

		}

	}

	private void checkNextOperator(OpenBitSet b, ArrayList<ZIndex> valB) {
		int index = 0;
		List<ZIndex> sortedBs = new ArrayList<>();
		for (int j = b.nextSetBit(b.length()); j >= 0; j = b.nextSetBit(j - 1)) {
			sortedBs.add(valB.get(index));

			if (j == Integer.MIN_VALUE) {
				break; // or (i+1) would overflow
			}
		}
		if (sortedBs.isEmpty())
			return;

		Collections.sort(sortedBs, new TimeComparator());
		ZIndex older = sortedBs.get(0);
		for (int i = 1; i < sortedBs.size(); i++) {

			if (older.values[1] > sortedBs.get(i).values[1]) {
				b.fastClear(sortedBs.get(i).id_b);
			} else {
				older = sortedBs.get(i);
			}
		}

	}

	private void kleeneplus(int[] a, OpenBitSet b, int[] c, ArrayList<ZIndex> valB) {

		checkNextOperator(b, valB);
		// System.out.println("FUCK: Kleene_+ " + b.cardinality());
		// if (b.cardinality() > 14) {
		// // System.out.println("FUCK: Kleene_+ " + b.cardinality());
		// return;
		// }
		int max = 1 << b.cardinality();
		StringBuilder sb = new StringBuilder();
		for (int i = 1; i < max; i++) {
			int index = 0;
			sb = new StringBuilder();
			sb.append("a ");
			sb.append(Arrays.toString(a));
			sb.append(" ; ");
			int prev_price = Integer.MIN_VALUE;
			// get_outputQueue().add(Integer.toString(a[0]));
			for (int j = i; j > 0; j >>= 1) {
				if ((j & 1) == 1) {

					int index_2 = 0;

					for (int k = 0; k <= index; k++) {
						index_2 = b.nextSetBit(index_2);
						index_2++;
					}

					sb.append("b ");
					sb.append(Arrays.toString(valB.get(index_2 - 1).getValues()));
					sb.append(" ; ");

				}
				index++;
			}

			sb.append("\n");
			// System.out.println(sb.toString());
			AbstractApp.num0fMatches++;
			// this.event_processed++;
			// System.out.println("Matches: " + event_processed);

		}
	}

	private ZIndex[] createRangesForB(SimpleEvent b) {

		int time = b.values[0] > window.windowLength ? b.values[0] - window.windowLength : minmax.min[0];

		ZIndex minZRange = new ZIndex(new int[] { time, b.values[1] + 1, b.values[2] });

		ZIndex maxZRange = new ZIndex(new int[] { b.values[0] - 1, minmax.max[1], b.values[2] });

		return new ZIndex[] { minZRange, maxZRange };
	}

	private ZIndex[] createRangesForA(SimpleEvent b) {

		// System.out.println(minmaxInter.min[1]);

		ZIndex minZRange = null;

		ZIndex maxZRange = null;
		int time = b.values[0] > window.windowLength ? b.values[0] - window.windowLength : minmax.min[0];
		// if (minmaxInter.min[1] == Integer.MAX_VALUE) {
		// minZRange = new ZIndex(new int[] { time, b.values[1] * 10,
		// b.values[2] });
		// minmaxInter.max[1] = b.values[1];
		//
		// maxZRange = new ZIndex(new int[] { b.values[0], b.values[1],
		// b.values[2] });
		// } else {
		minZRange = new ZIndex(new int[] { time, minmaxInter.min[1] * 10, b.values[2] });

		maxZRange = new ZIndex(new int[] { minmaxInter.max[0], minmax.max[1], b.values[2] });
		// }

		return new ZIndex[] { minZRange, maxZRange };
	}

	@Override
	protected boolean isInrangeB(int[] min, int[] max, int[] point) {

		if (!(point[0] >= min[0] && point[0] <= max[0]))
			return false;
		if ((point[2] != min[2]))
			return false;
		for (int i = 1; i < point.length - 1; i++) {

			if (!(point[i] >= min[i] && point[i] <= max[i]))
				return false;
		}

		return true;
	}

	@Override
	protected void joinSingleRegions(OpenBitSet[] listbitsets, Zregion r1, Zregion r2, String[] phis,
			ArrayList<ZIndex> valB) {
		/// Sort them according to the timestamps to make it event sequences
		List<ZIndex> a = r1.getValues();
		List<ZIndex> b = r2.getValues();

		if (r1.sorted == 0) {
			Collections.sort(a, new TimeComparator());
			r1.sorted = 1;
		}

		if (r2.sorted == 0) {
			Collections.sort(b, new TimeComparator());
			r2.sorted = 1;
		}

		int start = b.size() - 1;
		/// loop over A and B

		for (int i = a.size() - 1; i >= 0; i--) {

			/**
			 * Check if if this value can actually match with any other value in
			 * B or not using the max and min of region r2, Depending on the
			 * query, the phis will be different. Note that it supports strickly
			 * less than or greater than relation
			 */
			// TODO: something is not right over here
			if (!dominates_single(r2.getMax(), r2.getMin(), a.get(i).values, phis)) {
				// // System.out.println("Dominate Single..");
				continue;
			}

			if (i != a.size() - 1) {
				/**
				 * TODO: CHANGE QUERY PREDICATES WHEN REQUIRED. For each query
				 * change the predicate relations over here. For this query its
				 * A.price < B.price and A.vol > B.vol. And the lists are sorted
				 * on the Timestamps. For other queries change it over here
				 * [0]--> timestamps ; [1]--> Price ; [2] --> Volume
				 */
				if (a.get(i).values[1] >= a.get(i + 1).values[1] && listbitsets[a.get(i + 1).id_a] != null
						&& !listbitsets[a.get(i + 1).id_a].isEmpty()) {

					int limit = 0;
					if (listbitsets[a.get(i).id_a] == null)
						listbitsets[a.get(i).id_a] = new OpenBitSet(valB.size());

					while (limit <= b.size() - 1) {
						if (listbitsets[a.get(i + 1).id_a].get(b.get(limit).id_b))

							listbitsets[a.get(i).id_a].set(b.get(limit).id_b);
						/**
						 * TODO: CHANGE QUERY PREDICATES WHEN REQUIRED For this
						 * query A.amount > B.amount *10
						 */
						else if (b.get(limit).values[1] * 10 <= a.get(i).values[1]) {

							listbitsets[a.get(i).id_a].set((b.get(limit).id_b));

						}
						limit++;
					}

				} else {

					start = b.size() - 1;

				}

			}

			while (start >= 0) {
				/**
				 * TODO: CHANGE QUERY PREDICATES WHEN REQUIRED For this query
				 * A.time< B.time A.price < B.price and A.vol > B.vol
				 */
				if (b.get(start).values[0] > a.get(i).values[0]
						&& (b.get(start).values[1] * 10) <= a.get(i).values[1]) {

					if (listbitsets[a.get(i).id_a] == null)
						listbitsets[a.get(i).id_a] = new OpenBitSet(valB.size());
					listbitsets[a.get(i).id_a].set(b.get(start).id_b);
					start--;

				} else if (b.get(start).values[0] < a.get(i).values[0]) {

					break;
				} else {
					start--;

				}
			}

		}

	}

	@Override
	protected boolean isInrangeA(int[] min, int[] max, int[] point) {
		if (!(point[0] >= min[0] && point[0] <= max[0]))
			return false;
		if ((point[2] != min[2]))
			return false;
		for (int i = 1; i < point.length - 1; i++) {

			if (!(point[i] >= min[i] && point[i] <= max[i]))
				return false;
		}

		return true;
	}

	private void queryStores(SimpleEvent ce, int dim, ArrayList<ZIndex> val, ArrayList<Zregion> regionsFor,
			ZIndex[] ranges, int e_type) throws CloneNotSupportedException {

		SortedMap<ZIndex, Integer> b = null;

		ArrayList<ZIndex[]> queries = zc.decomposeintoTwo(ranges[0], ranges[1]);

		EventStore store = (store_1.getStoreType() == 0) ? store_1 : store_2;

		if (store.getT().size() > 0) {
			this.queryValues(val, queries, regionsFor, b, ranges, store, e_type);
		}

		store = (store_1.getStoreType() == 1) ? store_1 : store_2;

		if (store.getT().size() > 0) {
			this.queryValues(val, queries, regionsFor, b, ranges, store, e_type);
		}
	}

	private void queryValues(ArrayList<ZIndex> valB, ArrayList<ZIndex[]> queries, ArrayList<Zregion> regions,
			SortedMap<ZIndex, Integer> b, ZIndex[] ranges, EventStore store, int bORa) {

		for (ZIndex[] q : queries) {
			b = store.getT().subMap(q[0], true, q[1], true);
			if (b != null || !b.isEmpty()) {
				newCreateRegions(b, ranges, this.dim, bORa, valB, regions);
			}
		}

	}

	@Override
	public int dominates_generic(int[] max_r1, int[] min_r1, int[] max_r2, int[] min_r2, String[] phis) {
		// the phis contains the condition for each dimensions
		int correct = 0;
		for (int i = 0; i < phis.length; i++) {

			if (phis[i].equals("<") && i != 0 && max_r1[i] <= min_r2[i]) {
				// max of region1 < min of region2
				correct++;
			} else if (phis[i].equals("<") && i == 0 && max_r1[i] < min_r2[i]) {
				correct++;
			}
			// TODO: change for different types of comparison between
			else if (phis[i].equals(">") && min_r1[i] >= max_r2[i] * 10) {
				correct++;
			}

		}

		if (correct == phis.length)
			return 1;

		correct = 0;
		for (int i = 0; i < phis.length; i++) {

			if (phis[i].equals("<") && i != 0 && min_r1[i] <= max_r2[i]) {
				// max of region1 < min of region2
				correct++;
			} else if (phis[i].equals("<") && i == 0 && min_r1[i] < max_r2[i]) {
				correct++;
			}

			else if (phis[i].equals(">") && max_r1[i] * 10 >= min_r2[i]) {
				correct++;
			}

		}

		if (correct == phis.length)
			return 2;

		return 0;

	}

}
