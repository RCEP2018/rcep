package org.queryprocessor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.lucene.util.OpenBitSet;
import org.event.SimpleEvent;
import org.eventstore.BTreeStore;
import org.eventstore.EventStore;
import org.windowsemantics.AbstractWindow;
import org.zvalueencoder.DimensionException;
import org.zvalueencoder.ZIndex;
import org.zvalueencoder.Zregion;

public class StockSlidingWindow extends AbstractQueryProcessor {

	static int test_counter = 0;
	static int event_processed = 0;
	final EventStore store_3;
	final int ALGO_TYPE;

	public StockSlidingWindow(LinkedBlockingQueue<SimpleEvent> i, CountDownLatch l, AbstractWindow w, int dimen,
			int algo_type) throws IOException {
		super(i, l, w, dimen);
		store_3 = new BTreeStore(1, 0, w.windowLength, "tree", 3);
		ALGO_TYPE = algo_type;
	}

	@Override
	protected void process(SimpleEvent ce) throws IOException, DimensionException {

		ZIndex key = new ZIndex(ce.values);

		window.updateWindow(ce.values[0]);

		if (ALGO_TYPE == 2) {
			store_3.getT().put(key, 0);
			try {
				/**
				 * Memory Frendly Method
				 */
				deltefromTree(ce);
			} catch (CloneNotSupportedException e) {

				e.printStackTrace();
			}
		} else {
			/**
			 * CPU Friendly Method
			 */
			EventStore store = getStore(ce);

			store.getT().put(key, 0);
		}

		updateMaxMin(ce.values);

		// test_counter++;
		// this.event_processed++;
		// System.out.println("Event Processed: " + event_processed);

	}

	private void deltefromTree(SimpleEvent ce) throws CloneNotSupportedException {

		/// delete the events that are outside the window

		if (ce.values[0] - store_3.getEndTime() > 0) {
			// event_processed++;
			// System.out.println("remove operation " + event_processed);
			try {
				removeOlderEntries(ce);
			} catch (Exception ex) {

			}
			// EventStore primary_store = (store_1.getStoreType() == 1) ?
			// store_1 : store_2;
			//
			// primary_store.setStoreType(0);
			// store.setStoreType(1);
			store_3.setStartTime(ce.values[0]);
			store_3.setEndTime(ce.values[0]);

		}

		// ZIndex minZRange = new ZIndex(new int[] { ce.values[0],
		// minmax.min[1], minmax.min[2], minmax.min[3] });
		//
		// ZIndex maxZRange = new ZIndex(new int[] { ce.values[0] - 1,
		// minmax.max[1], minmax.max[2], minmax.max[3] });
		//
		// // store_3.getT().remove(key)
		//
		// ArrayList<ZIndex[]> queries = zc.decomposeintoTwo(minZRange,
		// maxZRange);
		// SortedMap<ZIndex, Integer> b = null;
		// for (ZIndex[] q : queries) {
		// b = store_3.getT().subMap(q[0], true, q[1], true);
		// if (b != null || !b.isEmpty()) {
		// Iterator it = b.keySet().iterator();
		//
		// while (it.hasNext()) {
		// ZIndex z = (ZIndex) it.next();
		//
		// if (isInrangeA(q[0].values, q[1].values, z.values)) {
		// /// delete it
		//
		// store_3.getT().remove(z);
		// }
		// }
		// }
		// }

	}

	public void removeOlderEntries(SimpleEvent ce) throws CloneNotSupportedException {
		ZIndex minZRange = new ZIndex(new int[] { minmax.min[0], minmax.min[1], minmax.min[2], minmax.min[3] });

		ZIndex maxZRange = new ZIndex(
				new int[] { ce.values[0] - window.windowLength, minmax.max[1], minmax.max[2], minmax.max[3] });

		// store_3.getT().remove(key)

		ArrayList<ZIndex[]> queries = zc.decomposeintoTwo(minZRange, maxZRange);
		SortedMap<ZIndex, Integer> b = null;
		for (ZIndex[] q : queries) {
			b = store_3.getT().subMap(q[0], true, q[1], true);
			if (b != null || !b.isEmpty()) {
				Iterator it = b.keySet().iterator();
				if (it == null)
					continue;

				while (it.hasNext()) {
					ZIndex z = null;

					z = (ZIndex) it.next();

					if (isInrangeA(q[0].values, q[1].values, z.values)) {
						/// delete it

						store_3.getT().remove(z);
					}
				}

			}
		}
	}

	@Override
	protected void joinSingleRegions(OpenBitSet[] listbitsets, Zregion r1, Zregion r2, String[] phis,
			ArrayList<ZIndex> valB) {
		/// Sort them acording to the timestamps to make it event sequences

	}

	@Override
	protected boolean isInrangeB(int[] min, int[] max, int[] point) {
		if (!(point[0] >= min[0] && point[0] <= max[0]))
			return false;

		// if ((point[1] % 50 != 0))
		// return false;

		for (int i = 1; i < point.length - 1; i++) {

			if (!(point[i] >= min[i] && point[i] <= max[i]))
				return false;
		}

		if ((point[3] != min[3]))
			return false;

		return true;
	}

	@Override
	protected boolean isInrangeA(int[] min, int[] max, int[] point) {

		for (int i = 0; i <= point.length - 1; i++) {

			if (!(point[i] >= min[i] && point[i] <= max[i]))
				return false;
		}

		return true;

	}

	@Override
	public int dominates_generic(int[] max_r1, int[] min_r1, int[] max_r2, int[] min_r2, String[] phis) {
		// the phis contains the condition for each dimensions

		return 0;

	}

	public boolean dominates_single(int[] max_r, int[] min_r, int[] point, String[] phis) {
		// the phis contains the condition for each dimensions

		return true;

	}

}
