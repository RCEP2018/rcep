package org.queryprocessor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.lucene.util.OpenBitSet;
import org.baseline.RTree;
import org.event.SimpleEvent;
import org.windowsemantics.AbstractWindow;
import org.zvalueencoder.DimensionException;
import org.zvalueencoder.ZIndex;
import org.zvalueencoder.Zregion;

public class StockRTree extends AbstractQueryProcessor {

	static int evets_processed = 0;

	// RTree<Integer, Geometry> tree;
	static int tree_type;

	RTree<SimpleEvent> tr;
	static int stream_size;

	public StockRTree(LinkedBlockingQueue<SimpleEvent> i, CountDownLatch l, AbstractWindow w, int dimen, int tree_type,
			int size) throws IOException {
		super(i, l, w, dimen);
		tr = new RTree<>(50, 3, 4);
		this.tree_type = tree_type;
		this.stream_size = size;

		// TODO Auto-generated constructor stub
	}

	@Override
	protected void process(SimpleEvent ce) throws IOException, DimensionException {

		/**
		 * Rtee Insertion and Querying
		 */
		// check if the events in the Rtree are still in the window?

		if (tree_type == 2) {
			// window.updateWindow(ce.values[0]);
			// windowRtree(ce);
			// System.out.println("Before " + tr.size());
			QueryImplementationRTree(ce, this.dim);
			tr.insert(new float[] { ce.values[0], ce.values[1], ce.values[2], ce.values[3] }, ce);
			// tree = tree.add(test_counter, Geometries.line(ce.values[0],
			// ce.values[1], ce.values[2], ce.values[3]));
		} else {
			/**
			 * Event Tree Insertion and Querying
			 */
			try {
				QueryImplementationStores(ce, this.dim);

			} catch (CloneNotSupportedException e) {

				e.printStackTrace();
			}

			ZIndex key = new ZIndex(ce.values);

			// window.updateWindow(ce.values[0]);

			this.store_1.getT().put(key, 0);

		}

		updateMaxMin(ce.values);

		evets_processed++;

		if (evets_processed == this.stream_size) {
			// System.out.println("Event Processed: " + evets_processed);
			// System.out.println("Size is " + store_1.getT().size());
			this.finish();

		}
		// this.event_processed++;

	}

	private void windowRtree(SimpleEvent ce) {
		// if (ce.values[0] - window.windowLength > 0) {
		// System.out.println("Before " + tr.size());
		List<SimpleEvent> toDelete = tr.search(
				new float[] { minmax.min[0], minmax.min[1], minmax.min[2], minmax.min[3] },
				new float[] { (ce.values[0] - window.windowLength), minmax.max[1], minmax.max[2], minmax.max[3] });

		for (SimpleEvent e : toDelete) {
			tr.delete(new float[] { minmax.min[0], minmax.min[1], minmax.min[2], minmax.min[3] },
					new float[] { (ce.values[0] - window.windowLength), minmax.max[1], minmax.max[2], minmax.max[3] },
					e);
		}
		// System.out.println("After " + tr.size());
		// }
	}

	private ZIndex[] createRangesForB(SimpleEvent ce) {

		int time = ce.values[0] > window.windowLength ? ce.values[0] - window.windowLength : minmax.min[0];

		ZIndex minZRange = new ZIndex(new int[] { time, ce.values[1] + 1, minmax.min[2], ce.values[3] });

		ZIndex maxZRange = new ZIndex(new int[] { ce.values[0] - 1, minmax.max[1], minmax.max[2], ce.values[3] });

		return new ZIndex[] { minZRange, maxZRange };
	}

	public void QueryImplementationStores(SimpleEvent ce, int dim) throws CloneNotSupportedException {

		ZIndex[] ranges = createRangesForB(ce);

		ArrayList<ZIndex[]> queries = zc.decomposeintoTwo(ranges[0], ranges[1]);

		for (ZIndex[] q : queries) {
			store_1.getT().subMap(q[0], true, q[1], true);

		}

	}

	private void QueryImplementationRTree(SimpleEvent ce, int dim) {

		// if (tr.size() < 2)
		// return;

		ZIndex[] ranges = createRangesForB(ce);

		tr.search(new float[] { ranges[0].values[0], ranges[0].values[1], ranges[0].values[2], ranges[0].values[3] },
				new float[] { ranges[1].values[0], ranges[1].values[1], ranges[1].values[2], ranges[1].values[3] });

	}

	@Override
	protected void joinSingleRegions(OpenBitSet[] listbitsets, Zregion r1, Zregion r2, String[] phis,
			ArrayList<ZIndex> valB) {

	}

	@Override
	protected boolean isInrangeB(int[] min, int[] max, int[] point) {

		return true;
	}

	@Override
	protected boolean isInrangeA(int[] min, int[] max, int[] point) {

		return true;

	}

	@Override
	public int dominates_generic(int[] max_r1, int[] min_r1, int[] max_r2, int[] min_r2, String[] phis) {
		// the phis contains the condition for each dimensions

		return 0;

	}

	public boolean dominates_single(int[] max_r, int[] min_r, int[] point, String[] phis) {

		return true;

	}

}
