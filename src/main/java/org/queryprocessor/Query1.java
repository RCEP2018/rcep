package org.queryprocessor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.lucene.util.OpenBitSet;
import org.benchmark.joins.IEJoinTest;
import org.btreeUtils.Value;
import org.event.SimpleEvent;
import org.eventstore.EventStore;
import org.windowsemantics.AbstractWindow;
import org.zvalueencoder.DimensionException;
import org.zvalueencoder.RangeSearchTest;
import org.zvalueencoder.ZIndex;
import org.zvalueencoder.Zregion;

import uk.co.omegaprime.btreemap.BTreeMap;

public class Query1 extends AbstractQueryProcessor {

	int test_counter = 0;
	// TreeMap<ZIndex, Value> map = new TreeMap<>();
	IEJoinTest ieJoin = new IEJoinTest();
	int total_matches;
	RangeSearchTest zc;
	BTreeMap<ZIndex, Integer> eventStore;

	public Query1(LinkedBlockingQueue<SimpleEvent> i, CountDownLatch l, AbstractWindow w) throws IOException {
		// TODO:The last argument 3 is the number of dimensions for minmax
		super(i, l, w, 3);
		zc = new RangeSearchTest();

	}

	@Override
	protected void process(SimpleEvent ce) throws IOException, DimensionException {
		// window.initliaseWindow();
		// System.out.println(test_counter);

		// QueryImplementation(ce, dim);

		try {
			QueryImplementation2(ce, dim);
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// QueryImplementation_IndexJoin(ce, dim);
		ZIndex key = new ZIndex(ce.values);

		window.updateWindow(ce.values[0]);

		eventStore.put(key, 0);

		// System.out.println("Height of the Tree is: " +
		// store.tree().get_height());
		updateMaxMin(ce.values);

		test_counter++;
		System.out.println(test_counter);

	}

	/**
	 * A simple test function to see if the results from the range query are
	 * sent back properly
	 * 
	 * @throws IOException
	 */
	public void testRangeQueryFunc() throws IOException {
		ZIndex minZRange = new ZIndex(new int[] { 4, 3, 2 });
		// ce.values[0]-1 (or +1 for minrange) is added because in B+tree the
		// comparison is based on
		// <= or >=
		ZIndex maxZRange = new ZIndex(new int[] { 9, 6, 5 });
		// TODO: Use another Tree.. May be?
		EventStore store = (store_1.getStoreType() == 1) ? store_1 : store_2;
		ArrayList<ZIndex> valA = new ArrayList<>();
		SortedMap<ZIndex, Integer> b = eventStore.subMap(minZRange, true, maxZRange, true);
		ArrayList<Zregion> regionsForA = newCreateRegions(b, new ZIndex[] { minZRange, maxZRange }, dim, 1, valA);
		ArrayList<Value> val = null;
		if (!store.tree().hasData()) {

			val = store.tree().searchRange(minZRange, maxZRange, minmaxInter);
		}

		// System.out.println();
	}

	private void QueryImplementation2(SimpleEvent ce, int dim) throws CloneNotSupportedException {
		if (eventStore.size() < 2)
			return;

		ZIndex[] ranges = createRangesForB(ce);
		SortedMap<ZIndex, Integer> b = null;
		// b = eventStore.subMap(ranges[0], true, ranges[1], true);

		ArrayList<ZIndex> valB = new ArrayList<>();
		ArrayList<ZIndex[]> queries = zc.decomposeintoTwo(ranges[0], ranges[1]);

		queries.addAll(zc.decomposeintoTwo(queries.get(0)[0], queries.get(0)[1]));
		queries.addAll(zc.decomposeintoTwo(queries.get(1)[0], queries.get(1)[1]));
		queries.remove(0);
		queries.remove(1);
		ArrayList<Zregion> regionsForB = new ArrayList<>(10);

		for (ZIndex[] q : queries) {
			b = eventStore.subMap(q[0], true, q[1], true);
			if (b != null && !b.isEmpty()) {
				regionsForB.addAll(newCreateRegions(b, ranges, this.dim, 2, valB));
			}
		}
		if (valB.isEmpty())
			return;
		/// get the values for A now
		ranges = createRangesForA(null);

		queries = zc.decomposeintoTwo(ranges[0], ranges[1]);
		queries.addAll(zc.decomposeintoTwo(queries.get(0)[0], queries.get(0)[1]));
		queries.addAll(zc.decomposeintoTwo(queries.get(1)[0], queries.get(1)[1]));
		queries.remove(0);
		queries.remove(1);

		ArrayList<ZIndex> valA = new ArrayList<>();

		SortedMap<ZIndex, Integer> a = null;

		ArrayList<Zregion> regionsForA = new ArrayList<>(20);
		for (ZIndex[] q : queries) {
			a = eventStore.subMap(q[0], true, q[1], true);
			if (a != null && !a.isEmpty()) {
				regionsForA.addAll(newCreateRegions(a, ranges, dim, 1, valA));
			}
		}

		if (valA.isEmpty())
			return;

		OpenBitSet[] listbitsets = new OpenBitSet[valA.size()];

		implementJoins(regionsForA, regionsForB, valA, valB, listbitsets, new String[] { "<", "<", ">" });

	}

	private void QueryImplementation(SimpleEvent ce, int dim) throws IOException, DimensionException {
		// We are having two range queries for each event tooo MUCH!!
		// Range queries are the bottle neck of the system
		if (eventStore.size() < 2)
			return;
		ZIndex[] ranges = createRangesForB(ce);

		SortedMap<ZIndex, Integer> b = eventStore.subMap(ranges[0], ranges[1]);

		if (b == null || b.isEmpty())
			return;
		ArrayList<ZIndex> valB = new ArrayList<>();
		ArrayList<Zregion> regionsForB = newCreateRegions(b, ranges, this.dim, 2, valB);
		if (valB.isEmpty())
			return;

		ranges = createRangesForA(null);
		SortedMap<ZIndex, Integer> a = eventStore.subMap(ranges[0], ranges[1]);

		if (a.isEmpty())
			return;
		// System.out.println("Size of A " + a.size());
		ArrayList<ZIndex> valA = new ArrayList<>();
		ArrayList<Zregion> regionsForA = newCreateRegions(a, ranges, dim, 1, valA);
		/// While going through A create the First Region and join it??
		// ArrayList<Value> valA = queryStore(ranges);
		if (valA.isEmpty())
			return;

		/**
		 * Create the regions based on the various bits
		 */
		// System.out.println(
		// "sie of A regions: " + regionsForA.size() + " region Size " +
		// regionsForA.get(0).values.size());

		// System.out.println(
		// "sie of B regions: " + regionsForB.size() + " region Size " +
		// regionsForA.get(0).values.size());

		OpenBitSet[] listbitsets = new OpenBitSet[valA.size()];

		// FastBitSet[] listbitsets = new FastBitSet[valA.size()];
		// long[] ba = new long[10];
		// long[][] bits = new long[valA.size()][valB.size()];

		// int[][] n_listbitsets = new int[valA.size()][valB.size()];
		/**
		 * new String[] { "<", "<", ">" } This means A.time < B.time, A.price <
		 * B.price and A.vol >B.vol
		 */
		/**
		 * Our join Algorithm
		 */
		implementJoins(regionsForA, regionsForB, valA, valB, listbitsets, new String[] { "<", "<", ">" });

		/**
		 * Nested Join Algorithm
		 */
		// nestedQueryJoin(valA, valB, ce, listbitsets);
		/**
		 * IE Join Algorithm from the VLDB paper
		 */
		// ieJoin.IEJoin3(valA, valB, null);
		/**
		 * If there is a Kleene plus sign involved in the query
		 */

		// kleeneplusImpl(valA, valB, listbitsets, ce.values);
	}

	public void QueryImplementation_IndexJoin(SimpleEvent ce, int dim) {
		if (eventStore.size() < 2)
			return;
		ZIndex[] ranges = createRangesForB(ce);

		SortedMap<ZIndex, Integer> b = eventStore.subMap(ranges[0], ranges[1]);

		if (b == null || b.isEmpty())
			return;
		ArrayList<ZIndex> valB = new ArrayList<>();
		ArrayList<Zregion> regionsForB = newCreateRegions(b, ranges, this.dim, 2, valB);
		if (valB.isEmpty())
			return;

		for (ZIndex z : valB) {
			/// use the index join

			this.indexJoin(z.values);
		}
	}

	public ArrayList<Zregion> newCreateRegions(SortedMap<ZIndex, Integer> b, ZIndex[] ranges, int dim, int e_type,
			ArrayList<ZIndex> val) {
		ArrayList<Zregion> finallist = new ArrayList<>();

		finallist.add(new Zregion(dim));
		Iterator it = b.keySet().iterator();

		Zregion r1 = finallist.get(finallist.size() - 1);
		ZIndex z = null;
		ZIndex zprev = null;
		int dis = 0;
		int i = 0;
		int test = 0;
		do {
			z = (ZIndex) it.next();
			if (isInrangeB(ranges[0].getValues(), ranges[1].getValues(), z.getValues())) {
				if (dis >= 5 || zprev != null && z.group_check != zprev.group_check) {
					/// create a new regions
					// dis >= 10 ||
					finallist.add(new Zregion(dim));
					r1 = finallist.get(finallist.size() - 1);
					dis = 0;
				}

				dis++;
				this.minmaxInter.updateMaxMin(z.getValues());
				if (e_type == 1)
					z.id_a = i;
				else
					z.id_b = i;

				val.add(z);
				r1.add(z);
				i++;
				zprev = z;

			}
			test++;
		} while (it.hasNext());

		// System.out.println("Original " + test);
		// System.out.println("Needed " + i);
		return finallist;
	}

	private void kleeneplusImpl(ArrayList<ZIndex> valA, ArrayList<ZIndex> valB, OpenBitSet[] listbitsets, int[] valC) {

		// for each value of A create the Kleene+ matches using the bitset list
		// and event int[] valC

		for (int i = 0; i < valA.size(); i++) {

			if (listbitsets[i] != null && !listbitsets[i].isEmpty())
				kleeneplus(valA.get(i).getValues(), listbitsets[i], valC, valB);

		}

	}

	/**
	 * Create the regions using the distance between them
	 * 
	 * @return
	 */

	public ArrayList<Zregion> createRegions(List<Value> results, int dim, int e_type) {

		ArrayList<Zregion> finallist = new ArrayList<>();
		/// TODO: once the region is decided is we don't have to find the region
		/// again. Add this information in the B+tree
		Zregion r1 = new Zregion(dim);
		if (e_type == 1)
			results.get(0).id_a = 0;
		else
			results.get(0).id_b = 0;
		r1.add(results.get(0));

		finallist.add(r1);

		Value v = results.get(0);
		for (int i = 1; i < results.size(); i++) {

			if (results.get(i).getGroup().equals(v.getGroup())) {
				if (e_type == 1)
					results.get(i).id_a = i;
				else
					results.get(i).id_b = i;
				finallist.get(finallist.size() - 1).add(results.get(i));
			} else {

				Zregion r2 = new Zregion(dim);
				if (e_type == 1)
					results.get(i).id_a = i;
				else
					results.get(i).id_b = i;
				r2.add(results.get(i));
				finallist.add(r2);

				v = results.get(i);
			}

		}

		return finallist;

	}

	/**
	 * Regions based joins Algo 3 from the paper
	 */
	protected void implementJoins(ArrayList<Zregion> region1, ArrayList<Zregion> region2, ArrayList<ZIndex> valA,
			ArrayList<ZIndex> valB, OpenBitSet[] listbitsets, String[] phis) {

		// where each bitset is equal to the size of the fullregion 2;

		// for (int i = 0; i < listbitsets.length; i++) {
		// listbitsets[i] = new OpenBitSet(valB.size());
		// }

		// Arrays.fill(listbitsets, new BitSet(valB.size()));
		/**
		 * r1 is the outer relation and r2 is the inner one.
		 */
		int _startRegion1 = 0, _startRegion2 = 0;

		int _endRegion1 = 0, _endRegion2 = 0;
		for (Zregion r1 : region1) {

			_endRegion1 = _startRegion1 + r1.getValues().size() - 1;

			/**
			 * If the previous region, i.e. prev(r1) dominates this one then we
			 * can reuse its results as well. Add this if statement in a new
			 * function over here
			 */
			_startRegion2 = 0;
			for (Zregion r2 : region2) {

				_endRegion2 = _startRegion2 + (r2.getValues().size() - 1);

				/**
				 * Still have to edit it for the region that cannot be joined
				 * together
				 */
				int dom = dominates_generic(r1.max, r1.min, r2.max, r2.min, phis);
				// System.out.println("Dominance " + dom);
				if (dom == phis.length) {

					// System.out.println("Here Setting Bits");
					/// set all the bits to 1 for each entry in r1 for r2
					this.setBits(listbitsets, _startRegion1, _endRegion1, _startRegion2, _endRegion2, valB.size());
					// setBitsFunction
				}

				else if (dom > 0) /// TODO: Not sure about this?

				{

					// then employ the standard join algorithm
					// send the region r1 and r2 to the older function which
					// will
					// join it in a pariwise manner.
					joinSingleRegions(listbitsets, r1, r2, phis, valB);

				}

				_startRegion2 = _endRegion2 + 1;
			}

			_startRegion1 = _endRegion1 + 1;
		}

	}

	/**
	 * Simple Algo 1 from the paper
	 * 
	 * @return
	 */

	public void joinSingleRegions(OpenBitSet[] listbitsets, Zregion r1, Zregion r2, String[] phis,
			ArrayList<ZIndex> valB) {

		/// Sort them acording to the timestamps to make it event sequences
		List<ZIndex> a = r1.getValues();
		List<ZIndex> b = r2.getValues();

		if (r1.sorted == 0) {
			Collections.sort(a, new TimeComparator());
			r1.sorted = 1;
		}

		if (r2.sorted == 0) {
			Collections.sort(b, new TimeComparator());
			r2.sorted = 1;
		}

		// int numofsetbits = 0;

		int start = b.size() - 1;
		/// loop over A and B

		for (int i = a.size() - 1; i >= 0; i--) {
			// numofsetbits = 0;

			/**
			 * Check if if this value can actually match with any other value in
			 * B or not using the max and min of region r2, Depending on the
			 * query, the phis will be different. Note that it supports strickly
			 * less than or greater than relation
			 */
			// TODO: something is not right over here
			if (!dominates_single(r2.getMax(), r2.getMin(), a.get(i).values, phis)) {
				// // System.out.println("Dominate Single..");
				continue;
			}

			if (i != a.size() - 1) {
				/**
				 * TODO: CHANGE QUERY PREDICATES WHEN REQUIRED. For each query
				 * change the predicate relations over here. For this query its
				 * A.price < B.price and A.vol > B.vol. And the lists are sorted
				 * on the Timestamps. For other queries change it over here
				 * [0]--> timestamps ; [1]--> Price ; [2] --> Volume
				 */
				if (a.get(i).values[1] <= a.get(i + 1).values[1] && a.get(i).values[2] >= a.get(i + 1).values[2]
						&& listbitsets[a.get(i + 1).id_a] != null && !listbitsets[a.get(i + 1).id_a].isEmpty()) {

					int limit = 0;
					if (listbitsets[a.get(i).id_a] == null)
						listbitsets[a.get(i).id_a] = new OpenBitSet(valB.size());

					while (limit <= b.size() - 1) {
						if (listbitsets[a.get(i + 1).id_a].get(b.get(limit).id_b))

							listbitsets[a.get(i).id_a].set(b.get(limit).id_b);
						/**
						 * TODO: CHANGE QUERY PREDICATES WHEN REQUIRED For this
						 * query A.price < B.price and A.vol > B.vol
						 */
						else if (b.get(limit).values[1] > a.get(i).values[1]
								&& b.get(limit).values[2] < a.get(i).values[2]) {

							listbitsets[a.get(i).id_a].set((b.get(limit).id_b));

						}
						limit++;
					}

				} else {

					start = b.size() - 1;

				}

			}

			while (start >= 0) {
				/**
				 * TODO: CHANGE QUERY PREDICATES WHEN REQUIRED For this query
				 * A.time< B.time A.price < B.price and A.vol > B.vol
				 */
				if (b.get(start).values[0] > a.get(i).values[0] && (b.get(start).values[1] > a.get(i).values[1])
						&& (b.get(start).values[2] < a.get(i).values[2])) {

					// System.out.println("id of b: " + b.get(start).id_b);
					// System.out.println("b volume :" + b.get(start).values[2]
					// + " a value " + a.get(i).values[2]);
					if (listbitsets[a.get(i).id_a] == null)
						listbitsets[a.get(i).id_a] = new OpenBitSet(valB.size());
					listbitsets[a.get(i).id_a].set(b.get(start).id_b);
					start--;
					// numofsetbits++;
				} else if (b.get(start).values[0] < a.get(i).values[0]) {

					break;
				} else {
					start--;

				}
			}

		}

	}

	private ZIndex[] createRangesForA(int[] valb) {
		// /For now hardcoded
		/**
		 * For A Sequence; the event of A has timestamp less than B, price is
		 * less than B and vol greater than B
		 **/
		ZIndex minZRange = new ZIndex(new int[] { minmax.min[0], minmax.min[1], minmaxInter.min[2] + 1 });
		// ce.values[0]-1 is added beacuse in B+tree the comparison is based on
		// <= or >=
		ZIndex maxZRange = new ZIndex(new int[] { minmaxInter.max[0] - 1, minmaxInter.max[1] - 1, minmax.max[2] });

		return new ZIndex[] { minZRange, maxZRange };

	}

	private ZIndex[] createRangesForB(SimpleEvent ce) {

		// B.price > C.price
		ZIndex minZRange = new ZIndex(new int[] { minmax.min[0], ce.values[1] + 1, minmax.min[2] });
		// ce.values[0]-1 (or +1 for minrange) is added because in B+tree the
		// comparison is based on
		// <= or >=
		ZIndex maxZRange = new ZIndex(new int[] { ce.values[0] - 1, minmax.max[1], minmax.max[2] });

		return new ZIndex[] { minZRange, maxZRange };
	}
	//
	// private void updateMaxMin(int[] val) {
	//
	// /*
	// * if (val[0] > minmax.max[0]) {
	// *
	// * minmax.max[0] = val[0]; }
	// *
	// * if (val[0] < minmax.min[0]) {
	// *
	// * minmax.min[0] = val[0]; }
	// *
	// * if (val[1] > minmax.max[1]) {
	// *
	// * minmax.max[1] = val[1]; }
	// *
	// * if (val[1] < minmax.min[1]) {
	// *
	// * minmax.min[1] = val[1]; }
	// *
	// * if (val[2] > minmax.max[2]) {
	// *
	// * minmax.max[2] = val[2]; }
	// *
	// * if (val[2] < minmax.min[2]) {
	// *
	// * minmax.min[2] = val[2]; }
	// */
	//
	// for (int i = 0; i < val.length; i++) {
	// if (val[i] > minmax.max[i]) {
	//
	// minmax.max[i] = val[i];
	// }
	//
	// if (val[i] < minmax.min[i]) {
	//
	// minmax.min[i] = val[i];
	// }
	// }
	//
	// }

	public void setBits(OpenBitSet[] listbitsets, int _startRegion1, int _endRegion1, int _startRegion2,
			int _endRegion2, int size) {
		for (int i = _startRegion1; i <= _endRegion1; i++) {
			// For each such subregion, i.e. an entry i, set the bits in the
			// given range.

			if (listbitsets[i] == null)
				listbitsets[i] = new OpenBitSet(size);
			listbitsets[i].set(_startRegion2, _endRegion2 + 1);

		}
	}

	public void setBitsLong(long[][] listbitsets, int _startRegion1, int _endRegion1, int _startRegion2,
			int _endRegion2) {
		for (int i = _startRegion1; i <= _endRegion1; i++) {
			// For each such subregion, i.e. an entry i, set the bits in the
			// given range.

			for (int j = _startRegion2; j <= _endRegion2; i++) {
				listbitsets[i][j] = 1;
			}
			/// listbitsets[i].set(_startRegion2, _endRegion2 + 1);

		}
	}

	public static class TimeComparator implements Comparator<ZIndex> {

		@Override
		public int compare(ZIndex o1, ZIndex o2) {

			return o1.values[0] - o2.values[0] > 0 ? 1 : -1;

		}
	}

	public boolean dominates_single(int[] max_r, int[] min_r, int[] point, String[] phis) {
		// the phis contains the condition for each dimensions

		for (int i = 0; i < phis.length; i++) {

			if (phis[i].equals("<") && !(point[i] < max_r[i])) {
				return false;
			} else if (phis[i].equals(">") && !(point[i] > min_r[i])) {
				return false;
			}

		}

		return true;

	}

	public void kleeneplus(int[] a, OpenBitSet b, int[] c, ArrayList<ZIndex> valB) {

		int max = 1 << b.cardinality();
		StringBuilder sb = new StringBuilder();
		for (int i = 1; i < max; i++) {
			int index = 0;
			sb = new StringBuilder();
			sb.append("a ");
			sb.append(Arrays.toString(a));
			sb.append(" ; ");
			int prev_price = Integer.MIN_VALUE;
			// get_outputQueue().add(Integer.toString(a[0]));
			for (int j = i; j > 0; j >>= 1) {
				if ((j & 1) == 1) {

					int index_2 = 0;

					for (int k = 0; k <= index; k++) {
						index_2 = b.nextSetBit(index_2);
						index_2++;
					}

					/**
					 * Implementation of Next operator
					 */

					if (valB.get(index_2 - 1).getValues()[1] >= prev_price) {
						sb.append("b ");
						sb.append(Arrays.toString(valB.get(index_2 - 1).getValues()));
						sb.append(" ; ");
						prev_price = valB.get(index_2 - 1).getValues()[1];
					}

				}
				index++;
			}
			// sb.append(" ; ");
			sb.append("c ");
			sb.append(Arrays.toString(c));
			sb.append("\n");
			this.total_matches++;
			System.out.println("Total Matches: " + this.total_matches);
			// System.out.println(sb.toString());
			// get_outputQueue().add(sb.toString());
			// get_outputQueue().add("\n");
			// test_counter++;

		}
	}

	/**
	 * A Nested Join comparative function for the
	 * 
	 * @param valA
	 * @param valB
	 */
	public void nestedQueryJoin(ArrayList<ZIndex> valA, ArrayList<ZIndex> valB, SimpleEvent c,
			OpenBitSet[] listbitsets) {
		// System.out.println("Size of A " + valA.size());
		// System.out.println("Size of B " + valB.size());
		for (int i = 0; i < valA.size(); i++) {
			for (int j = 0; j < valB.size(); j++) {
				if (valA.get(i).values[0] < valB.get(j).values[0] && valA.get(i).values[1] < valB.get(j).values[1]
						&& valA.get(i).values[2] > valB.get(j).values[2]) {
					/**
					 * Over here simply output the events with the c event
					 */
					if (listbitsets[i] == null)
						listbitsets[i] = new OpenBitSet(valB.size());

					listbitsets[i].set(j);
				}
			}
		}

	}

	protected boolean isInrangeB(int[] min, int[] max, int[] point) {
		if (!(point[0] > min[0] && point[0] < max[0]))
			return false;
		for (int i = 1; i < point.length; i++) {

			if (!(point[i] >= min[i] && point[i] <= max[i]))
				return false;
		}

		return true;
	}

	public void indexJoin(int[] valb) {
		/// create the range
		ZIndex[] ranges = createRangesForA_Index_Join(valb);

		// send the range to the tree

		SortedMap<ZIndex, Integer> a = eventStore.subMap(ranges[0], ranges[1]);

		if (a.isEmpty())
			return;

		/// for each a check if its matches with B, if yes then

		Iterator it = a.keySet().iterator();
		///
		ArrayList<ZIndex> matched_as = new ArrayList<>();
		while (it.hasNext()) {
			ZIndex z = (ZIndex) it.next();

			if (z.values[0] < valb[0] && z.values[1] < valb[1] && z.values[2] > valb[2]) {
				matched_as.add(z);
			}
		}

	}

	private ZIndex[] createRangesForA_Index_Join(int[] valb) {
		// /For now hardcoded
		/**
		 * For A Sequence; the event of A has timestamp less than B, price is
		 * less than B and vol greater than B
		 **/
		ZIndex minZRange = new ZIndex(new int[] { minmax.min[0], minmax.min[1], valb[2] + 1 });
		// ce.values[0]-1 is added beacuse in B+tree the comparison is based on
		// <= or >=
		ZIndex maxZRange = new ZIndex(new int[] { valb[0] - 1, valb[1] - 1, minmax.max[2] });

		return new ZIndex[] { minZRange, maxZRange };

	}

	@Override
	protected boolean isInrangeA(int[] min, int[] max, int[] point) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int dominates_generic(int[] max_r1, int[] min_r1, int[] max_r2, int[] min_r2, String[] phis) {
		// TODO Auto-generated method stub
		return 0;
	}

}
