package org.btreeUtils;

import java.util.Arrays;
import java.util.BitSet;

import org.event.Event;
import org.event.EventType;
import org.event.StockEvent;

public class Value {

	// WTF is this?

	// public int[] max;
	// public int[] min;
	///////
	public int id_a;
	public int id_b;
	// Only use this and group, deletes everything else
	public int[] values;
	// private int _value;
	private int timestamp;

	// private List<Integer> _window;
	/**
	 * To store information that which bit group this Z belongs to
	 */
	private BitSet group;

	public Event event;
	// private int _numOfEvents;

	public Value(int value, int timestamp) {
		// this._value = value;
		this.timestamp = timestamp;

	}

	// TODO:remove
	public int getPredicate(int pred) {
		if (this.event.eventType == EventType.STOCKEVENT) {
			StockEvent se = (StockEvent) this.event;
			if (pred == 1) // get the price
				return se.price;
			else
				return se.vol;

		}
		return 1;
	}

	// TODO: remove
	public int compareTo(Value o, int pred) {

		/**
		 * Add more types for other event types
		 */
		if (this.event.eventType == EventType.STOCKEVENT) {
			StockEvent se = (StockEvent) this.event;
			if (pred == 1) { // get the price

				if (se.price == ((StockEvent) o.event).price)
					return 0;
				return se.price > ((StockEvent) o.event).price ? 1 : -1;

			} else if (pred == 2) {
				if (se.vol == ((StockEvent) o.event).vol)
					return 0;
				return se.vol > ((StockEvent) o.event).vol ? 1 : -1;
			} else {
				if (se.timestamp == ((StockEvent) o.event).timestamp)
					return 0;
				return se.timestamp > ((StockEvent) o.event).timestamp ? 1 : -1;
			}
		}

		return 0;
	}

	// TODO: remove
	public int compareToEvent(Event event, int pred) {
		if (this.event.eventType == EventType.STOCKEVENT) {
			StockEvent se = (StockEvent) this.event;
			if (pred == 1) { // get the price

				if (se.price == ((StockEvent) event).price)
					return 0;
				return se.price > ((StockEvent) event).price ? 1 : -1;

			} else {
				if (se.vol == ((StockEvent) event).vol)
					return 0;
				return se.vol > ((StockEvent) event).vol ? 1 : -1;
			}
		}

		return 0;

	}

	// TODO:remove this later
	public Value(Event e, int t) {

		event = e;
		timestamp = t;

	}

	// TODO: remove this later
	public Value(Event e, int[] values, int t) {

		group = new BitSet(values.length);
		this.event = e;
		this.values = values;
		commonbit(values);

		timestamp = t;

	}

	public Value(int[] values) {

		group = new BitSet(values.length);

		this.values = values;
		commonbit(values);

	}

	public int getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(int timestamp) {
		this.timestamp = timestamp;
	}
	//
	// public void updateMaxMin(int... args) {
	// if (max == null || min == null) {
	// max = new int[args.length];
	// min = new int[args.length];
	// Arrays.fill(min, Integer.MAX_VALUE);
	// }
	// for (int i = 0; i < args.length; i++) {
	// if (args[i] > max[i]) {
	//
	// max[i] = args[i];
	// }
	//
	// if (args[i] < min[i]) {
	//
	// min[i] = args[i];
	// }
	// }
	//
	// }

	// @Override

	/**
	 * The good stuff
	 */
	/**
	 * To check which group of bits it belong to
	 */
	public void commonbit(int[] v) {

		if (v.length == 0)
			return;

		// String bits = "";
		int[] pos = new int[v.length];
		for (int i = 0; i < v.length; i++) {
			pos[i] = findPositionOfMSB(v[i]);

		}

		for (int i = 0; i < pos.length; i++) {

			if (i != pos.length - 1) {

				// bits = bits + " " + compare(pos[i], pos[i + 1], pos.length);

				if (compare(pos[i], pos[i + 1], pos.length)) {
					group.set(i);
				} else
					group.clear(i);

			} else {
				// int l = pos[i] >= pos[i - 1] ? 1 : 0;
				// bits = bits + " " + compare(pos[i], pos[i - 1], pos.length);
				if (compare(pos[i], pos[i - 1], pos.length)) {
					group.set(i);
				} else
					group.clear(i);

			}

		}

		// System.out.println(bits);

	}

	public static boolean compare(int a, int b, int bits) {

		if (a < b)
			return false;
		else if (a > b && a >= bits)
			return true;
		else if (a > b && a < bits)
			return false;

		if (a == b && a >= bits)
			return true;
		else
			return false;

	}

	public static int findPositionOfMSB(int n) {
		int high = 31, low = 0;

		while (high - low > 1) {
			int mid = (high + low) / 2;
			int maskHigh = (1 << high) - (1 << mid);
			if ((maskHigh & n) > 0) {
				low = mid;
			} else {
				high = mid;
			}
		}
		// System.out.println(n + ": MSB at " + low); // ". Between " +
		// (int)Math.pow(2, low) + "
		return low; // and " + (int)Math.pow(2,
		// low+1));

	}

	public BitSet getGroup() {
		return group;
	}

	public void setGroup(BitSet group) {
		this.group = group;
	}

	@Override
	public String toString() {
		return "Value [id_a=" + id_a + ", id_b=" + id_b + ", values=" + Arrays.toString(values) + ", group=" + group
				+ "]";
	}

	//
	// @Override
	// public String toString() {
	// return "Value [_values=" + Arrays.toString(_values) + ", _window=" +
	// _window + ", _numOfRuns=" + _numOfRuns
	// + "]";
	// }
}
