package org.btreeUtils;

import java.io.IOException;
import java.util.Random;

import org.apache.jdbm.BTree;
import org.apache.jdbm.DB;
import org.apache.jdbm.DBAbstract; //change back its visibility
import org.apache.jdbm.DBMaker;

public class Test {

	public static void main(String[] args) throws IOException {

		// DB db = DBMaker.openMemory().make();
		// BTree<Integer, Integer> tree = BTree.createInstance((DBAbstract) db);
		//
		// tree.insert(1, 1, true);
		//
		// tree.insert(5, 5, true);
		//
		// tree.insert(8, 8, true);
		//
		// tree.insert(9, 9, true);
		//
		// tree.insert(7, 7, true);
		//
		// tree.insert(3, 3, true);
		//
		// tree.insert(2, 2, true);
		// //
		// // for (int i = 0; i < 1000; i++) {
		// //
		// // tree.insert(i + 2, i + 2, true);
		// // }
		// //
		// // // System.out.println(tree.get(3));
		// //
		// System.out.println(tree.searchRange(5, 12));

		// db.close();
		int[] masks = new int[32];
		for (int n = 0; n < 32; n++) {
			masks[n] = 1 << n;
		}
		DB db = DBMaker.openMemory().make();
		// BTree<ZIndex, Integer> tree = BTree.createInstance((DBAbstract) db);
		//
		// for (int i = 0; i < 100; i++) {
		// ZIndex key = new ZIndex(new int[] { i, i + 1, i + 2 }, 2, masks);
		// tree.insertUB(key, i, false);
		//
		//
		// }

		int[] factorial = { 1, 1, 2, 6, 24, 120, 720, 5040 };

		// integer test
		Random rn = new Random();
		BTree<Integer, Integer> tree = BTree.createInstance((DBAbstract) db);

		for (int i = 0; i < 100; i++) {

			// tree.insertUB( ( 10 + (int)(Math.random() * 1000)), i, false);

		}

		int height = tree.get_height();

		tree.getRoot().dumpRecursive((height), 0);
		// db.close();
	}

}
