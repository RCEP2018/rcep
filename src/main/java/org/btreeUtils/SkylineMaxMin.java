package org.btreeUtils;

import java.util.ArrayList;

public class SkylineMaxMin {
	public ArrayList<Value> regionValues;

	public MaxAndMinValues maxmin;

	public SkylineMaxMin() {
		super();
		regionValues = new ArrayList<>();
		maxmin = new MaxAndMinValues(3);
	}

	public void addvalueandUpdate(Value v) {
		regionValues.add(v);
		maxmin.updateMaxMin(v.getTimestamp(), v.getPredicate(1), v.getPredicate(2));

		// (Value) start._values[indexLow]).getTimestamp(),
		// ((Value) start._values[indexLow]).getPredicate(1),
		// ((Value) start._values[indexLow]).getPredicate(2)
	}
	
	
	

}
