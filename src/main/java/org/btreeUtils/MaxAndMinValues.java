package org.btreeUtils;

public class MaxAndMinValues {

	public int[] max;

	public int[] min;

	public MaxAndMinValues(int n) {

		max = new int[n];

		min = new int[n];

		for (int i = 0; i < min.length; i++) {

			max[i] = Integer.MIN_VALUE;
			min[i] = Integer.MAX_VALUE;
		}
	}

	public void reset() {

		for (int i = 0; i < min.length; i++) {

			max[i] = Integer.MIN_VALUE;
			min[i] = Integer.MAX_VALUE;
		}
	}

	public void updateMaxMin(int[] values) {

		/*
		 * if (values[0] > max[0]) {
		 * 
		 * max[0] = values[0]; }
		 * 
		 * if (values[0] < min[0]) {
		 * 
		 * min[0] = values[0]; }
		 * 
		 * if (values[1] > max[1]) {
		 * 
		 * max[1] = values[1]; }
		 * 
		 * if (values[1] < min[1]) {
		 * 
		 * min[1] = values[1]; }
		 * 
		 * if (values[2] > max[2]) {
		 * 
		 * max[2] = values[2]; }
		 * 
		 * if (values[2] < min[2]) {
		 * 
		 * min[2] = values[2]; }
		 */

		for (int i = 0; i < values.length; i++) {
			if (values[i] > max[i]) {

				max[i] = values[i];
			}

			if (values[i] < min[i]) {

				min[i] = values[i];
			}
		}
		/*
		 * if (values[3] > max[3]) {
		 * 
		 * max[3] = values[3]; }
		 * 
		 * if (values[3] < min[3]) {
		 * 
		 * min[3] = values[3]; }
		 */

	}

	public void updateMaxMin(int t, int p, int v) {

		if (t > max[0]) {

			max[0] = t;
		}

		if (t < min[0]) {

			min[0] = t;
		}

		if (p > max[1]) {

			max[1] = p;
		}

		if (p < min[1]) {

			min[1] = p;
		}

		if (v > max[2]) {

			max[2] = v;
		}

		if (v < min[2]) {

			min[2] = v;
		}

	}

}
