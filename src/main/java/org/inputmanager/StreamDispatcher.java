package org.inputmanager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

import org.event.EventType;
import org.event.SimpleEvent;

public class StreamDispatcher extends AbstractDispatcher {

	BufferedReader _reader;
	int i = 0;
	static int dim;
	private final LinkedBlockingQueue<SimpleEvent> _inputQueue;
	private final EventType et;
	private String dataToParse;

	public StreamDispatcher(final String string, int capacity, EventType e, int d, String dataToParse) {
		super(string);
		et = e;
		this.dim = d;
		_inputQueue = new LinkedBlockingQueue<>(capacity);
		this.dataToParse=dataToParse;

	}

	/**
	 * Open the readers for the file
	 * 
	 * @throws FileNotFoundException
	 */
	private void openReader() throws FileNotFoundException {

		_reader = new BufferedReader(new FileReader(this.fileLocation));

	}

	public void run() {
		// TODO Auto-generated method stub
		// Event e = new PoisonPill(0, 0, EventType.POISONPILL);
		String line = "";

		try {
			openReader();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {

			while (!(line = _reader.readLine()).equalsIgnoreCase("end")) {
				SimpleEvent e = new SimpleEvent(); // Event.parse(line, et);
				if(dataToParse.toLowerCase().equals("stockdata"))
				 e.parseRealStock(line);
				 if(dataToParse.toLowerCase().equals("windowdata"))
				e.parseForTree(line);
				 if(dataToParse.toLowerCase().equals("joindata"))
				e.parseJoin(line);
				 if(dataToParse.toLowerCase().equals("creditcarddata"))
				 e.parseCard(line, ++i);
				_inputQueue.add(e);
				this.records++;
			}
			SimpleEvent end = new SimpleEvent();

			_inputQueue.add(end);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public LinkedBlockingQueue<SimpleEvent> get_inputQueue() {
		return _inputQueue;
	}
}
