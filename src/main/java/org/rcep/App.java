package org.rcep;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import javax.jws.WebParam.Mode;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.pattern.IntegerPatternConverter;

public class App extends AbstractApp {

	/*
	 * *************************************************************** DATA
	 * MEMBERS ***************************************************************
	 */

	// Main parsing options

	private static String dataFile = "dataFile";
	private static String windowSize = "windowSize";
	private static String algorithme = "algorithme";
	private static String queryToRun = "queryToRun";
	private static String mode = "mode";
	private static String dimension = "dimension";
	private static String size = "size";
	private static String HELP = "help";

	// Mode options
	private static String dataDescription = "dataDescription";
	private static final String OPT_SMART_FULL = "smart_full";
	private static final String OPT_RRS = "rrs";
	private static final String OPT_SMART_RRS = "smart_rrs";

	// Scheduler options
	private static final String SCH_BASIC = "basic";
	private static final String SCH_ADVANCED = "advanced";

	public static void main(String[] args) throws IOException {
		CommandLine line = parseAndValidateInput(args);

		// Print out instructions details if asked for
		if (line.hasOption(HELP)) {
			printUsage(System.out, true);
			System.exit(0);
		}

		//
		//
		// Options options = new Options();
		// options.addOption("h", "help", false, "prints the help content");
		// options.addOption(OptionBuilder
		// .withArgName("file")
		// .hasArg()
		// .isRequired()
		// .withDescription("input file")
		// .withLongOpt("input")
		// .create("i"));
		// options.addOption(OptionBuilder
		// .withArgName("file")
		// .hasArg()
		// .withDescription("output file")
		// .withLongOpt("output")
		// .create("o"));
		// options.addOption(OptionBuilder
		// .withArgName("file")
		// .hasArg()
		// .withDescription("configuration file")
		// .withLongOpt("config")
		// .create("c"));

		System.out.println("System Started.....");
		// System.in.read();
		// String filename, int window, int size, int algo, int dim
		// start(args[0], 0, Integer.parseInt(args[1]),
		// Integer.parseInt(args[2]), Integer.parseInt(args[3]));

		/**
		 * start for Treetest
		 */
		// start(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]));
		/**
		 * start for sliding window test start(String DATAFILE,int
		 * WINDOWSIZE,int Algorithme) for the algorithms we can use
		 * Memory-Friendly-Methode (1) or CPU-FRIENDLY-Methode(2)
		 */

		start(line);
		// start(line.getOptionValue(dataFile),
		// Integer.parseInt(line.getOptionValue(windowSize)),
		// Integer.parseInt(line.getOptionValue(algorithme)));

		/**
		 * start for credit card test
		 */
		// start(args[0], Integer.parseInt(args[1]), 0);

	}

	private static void printUsage(PrintStream out, boolean details) {

		out.println();
		out.println("Usage:");
		out.println(" jar rcep.jar <parameters>");
		out.println("");
		out.println(" To test the sliding window thechniques, parameters must be:");
		out.println(
				" -Mode windowTest -dataDescription windowdata -dataFile <file> -windowSize <int> -algorithm <int>");

		out.println("");
		out.println("  -help");
		out.println("");
		out.println("----------- Mode & dataDescription are fix ----------");
		out.println(" dataFile ");
		out.println("Data file location");
		out.println("	--window test data: .src/main/resources/streamDataFiles/windowTest.stream");
		out.println(" WindowSize ");
		out.println("Size of the window");
		out.println("");
		out.println("Description of algorithms:");
		out.println("  1  " + "CPU Friendly Methode");
		out.println("  2  " + "Memory Friendly Methode");

		out.println(" To test the differentimplemented queries, parameters must be:");
		out.println("  -Mode queryTest -dataFile <file> -queryToRun <Q1 or Q2> ");

		out.println("");
		out.println("  -help");

		out.println("");
		out.println("Description of datafile:");
		out.println("");
		out.println("Data file location:");
		out.println("");
		out.println("	--1M Generated Stock events: .src/main/resources/streamDataFiles/GeneratedStock1M.stream");
		out.println("	--Real Stock events: .src/main/resources/streamDataFiles/RealStockEvent.stream");
		out.println("	--Real Credit Card events: .src/main/resources/streamDataFiles/RealCreditCard.stream");
		out.println("");
		out.println("");
		out.println("Description of queryToRun:");
		out.println("");
		out.println("  Q1  "
				+ "PATTERN SEQ (a, b+, c)\nWHERE a.id = b.id \nAND b.id = c.id \nAND a.price < b.price \nAND b.price < NEXT(b.price) \nAND c.price < FIRST(b.price)\nWITHIN 30 minutes SLIDE 2 minutes");
		out.println("");
		out.println("  Q2  "
				+ "PATTERN SEQ (a, b+)\nWHERE a.id = b.id \nAND a.amount ≥ b.amount * 5 \nAND b.amount > NEXT(b.amount) \nWITHIN 30 minutes SLIDE 5 minutes");

		out.println("");
		out.println("");
		out.println(" To test the join algorithme, parameters must be:");
		out.println("  -Mode joinTest -dataFile <file> -dimension <int>  -size <int> -algorithm <int>");
		out.println("");
		out.println(" -dataFile  \nData file location:");
		out.println("");
		out.println("	--Join Test Event: .src/main/resources/streamDataFiles/JoinTest.stream");
		out.println("");
		out.println(" -dimension \nNumber of dimensions: between 2 and 4");
		out.println("");
		out.println("size \nHow many elements to parse: 10000 or 100000");
		out.println("");

		out.println("Description of algorithms:");
		out.println("  1  " + "Hybrid Algorithme");
		out.println("  2  " + "Nested Join Algorithme");
		out.println("  3  " + "IEjoin Algorithme");

	}

	/**
	 * Parse and validate the input arguments
	 * 
	 * @param args
	 *            the input arguments
	 * @return the parsed command line
	 */
	private static CommandLine parseAndValidateInput(String[] args) {

		// Make sure we have some
		if (args == null || args.length == 0) {
			printUsage(System.out);
			System.exit(0);
		}

		// Parse the arguments
		Options opts = buildOptimizerOptions();
		CommandLineParser parser = new GnuParser();
		CommandLine line = null;
		try {
			line = parser.parse(opts, args, true);
		} catch (ParseException e) {
			System.err.println("Unable to parse the input arguments");
			System.err.println(e.getMessage());
			printUsage(System.err);
			System.exit(-1);
		}

		// Ensure we don't have any extra input arguments
		if (line.getArgs() != null && line.getArgs().length > 0) {
			System.err.println("Unsupported input arguments:");
			for (String arg : line.getArgs()) {
				System.err.println(arg);
			}
			printUsage(System.err);
			System.exit(-1);
		}

		// If the user asked for help, nothing else to do
		if (line.hasOption(HELP)) {
			return line;
		}

		if (line.hasOption(mode)) {
			if (line.getOptionValue(mode).equals("windowtest")) {
				if (line.hasOption(windowSize) && line.hasOption(algorithme) && line.hasOption(dataFile)) {
					ensureFileExists(line.getOptionValue(dataFile));
				} else {
					System.err.println("The 'algorithme' and 'windowSize' options are required: check windowTest mode");
					printUsage(System.err);
					System.exit(-1);
				}
			} else if (line.getOptionValue(mode).equals("querytest")) {
				if (line.hasOption(queryToRun) && line.hasOption(dataFile)) {
					ensureFileExists(line.getOptionValue(dataFile));
				} else {
					System.err.println("The 'queryToRun' and 'dataFile' options are required : check queyTest mode");
					printUsage(System.err);
					System.exit(-1);
				}
			} else if (line.getOptionValue(mode).equals("jointest")) {
				if (line.hasOption(dimension) && line.hasOption(dataFile)) {
					ensureFileExists(line.getOptionValue(dataFile));
					int dim = Integer.parseInt(line.getOptionValue(dimension));
					if (dim < 1 || dim > 4) {
						System.err.println("The Number of dimensions must be between 2 and 4");
						System.exit(-1);
					}
					int sizeof = Integer.parseInt(line.getOptionValue(size));		
				} else {
					System.err.println("the 'dimension', 'datafile' and  the 'size' are required: check the joinTest");
					printUsage(System.err);
					System.exit(-1);
				}
			}
		}

		return line;
	}

	private static void printUsage(PrintStream out) {
		printUsage(out, false);
	}

	@SuppressWarnings("static-access")
	private static Options buildOptimizerOptions() {

		// Build the options

		Option profileOption = OptionBuilder.withArgName(dataFile).hasArg().withDescription("The data file")
				.create(dataFile);
		Option confOption = OptionBuilder.withArgName(windowSize).hasArg().withDescription("The window size")
				.create(windowSize);
		Option inputOption = OptionBuilder.withArgName(algorithme).hasArg().withDescription("The algorithm to use")
				.create(algorithme);
		Option modeOption = OptionBuilder.withArgName(mode).hasArg().withDescription("The mode to use").create(mode);
		Option dataDescriptionOption = OptionBuilder.withArgName(dataDescription).hasArg()
				.withDescription("The Description of the data to use").create(dataDescription);
		Option dimensionOption = OptionBuilder.withArgName(dimension).hasArg()
				.withDescription("The number of dimensions").create(dimension);

		Option queryOption = OptionBuilder.withArgName(queryToRun).hasArg().withDescription("The query to test")
				.create(queryToRun);

		Option sizeOption = OptionBuilder.withArgName(size).hasArg().withDescription("how many element to parse")
				.create(size);
		// Option schedulerOption =
		// OptionBuilder.withArgName(SCHEDULER).hasArg()
		// .withDescription("The taskScheduler").create(SCHEDULER);
		//
		// Option clusterOption = OptionBuilder.withArgName(CLUSTER).hasArg()
		// .withDescription("The cluster specifications file")
		// .create(CLUSTER);
		// Option modeOption = OptionBuilder.withArgName(MODE).hasArg()
		// .withDescription("Optimization mode options").create(MODE);
		//
		// Option outputOption = OptionBuilder.withArgName("filepath").hasArg()
		// .withDescription("An output file to print to").create(OUTPUT);
		// Option helpOption = OptionBuilder.withArgName("help").create(HELP);

		// Declare the options
		Options opts = new Options();
		opts.addOption(modeOption);
		opts.addOption(profileOption);
		opts.addOption(confOption);
		opts.addOption(dimensionOption);
		opts.addOption(inputOption);
		opts.addOption(modeOption);
		opts.addOption(dataDescriptionOption);
		opts.addOption(queryOption);
		opts.addOption(sizeOption);

		return opts;
	}

	/**
	 * Ensure the file exists otherwise exit the application
	 * 
	 * @param fileName
	 *            the file name
	 */
	private static void ensureFileExists(String fileName) {
		File file = new File(fileName);
		if (!file.exists()) {
			System.err.println("The file does not exist: " + file.getAbsolutePath());
			System.exit(-1);
		}
	}
}
