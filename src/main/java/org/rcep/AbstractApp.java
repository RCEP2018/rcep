package org.rcep;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import org.apache.commons.cli.CommandLine;
import org.benchmark.joins.JoinQTest;
import org.benchmark.joins.JoinTest2Dim;
import org.benchmark.joins.JoinTest4Dim;
import org.event.EventType;
import org.inputmanager.StreamDispatcher;
import org.outputmanager.QueryProcessorMeasure;
import org.queryprocessor.AbstractQueryProcessor;
import org.queryprocessor.CardQ1;
import org.queryprocessor.StockQ1;
import org.queryprocessor.StockQ210K;
import org.queryprocessor.StockRealDataQ1;
import org.queryprocessor.StockSlidingWindow;
import org.windowsemantics.AbstractWindow;
import org.windowsemantics.SlidingWindow;

public class AbstractApp {
	// final static Logger logger = LoggerFactory.getLogger(AbstractApp.class);

	private static String INPUT_FILE = "./src/main/resources/iofiles/data_test.txt";
	// private static String INPUT_FILE =
	// "/Users//Documents/Post-Doc/Post-tests/card.txt";
	private static String OUTPUT_DIRC = "./src/main/java/org/labhc/iofiles/";
	private static String DATATOUSE = "TreeTest";
	final static QueryProcessorMeasure measure = new QueryProcessorMeasure();
	public static int num0fMatches = 0;

	public static void start(CommandLine line) throws IOException {

	

		final CountDownLatch latch = new CountDownLatch(1); /// 2
		StreamDispatcher sd=null;
		AbstractQueryProcessor qp=null;;
		
			// INPUT_FILE = line.getOptionValue("dataFile");
		/**
		 * the 4th arg (nb of dimensions) is not used in this test
		 */
		

	

		// AbstractQueryProcessor qp = new CardQ1(sd.get_inputQueue(), latch, w,
		// 3);
		
		if (line.getOptionValue("mode").toLowerCase().equals("windowtest")){
			 sd = new StreamDispatcher(line.getOptionValue("dataFile"), 2000000, EventType.STOCKEVENT,0,line.getOptionValue("dataDescription"));
			AbstractWindow w = new SlidingWindow(Integer.parseInt(line.getOptionValue("windowSize")), 1);
		 qp = new StockSlidingWindow(sd.get_inputQueue(), latch, w, 4, Integer.parseInt(line.getOptionValue("algorithme")));
		}else
			if(line.getOptionValue("mode").toLowerCase().equals("querytest")){
				 sd = new StreamDispatcher(line.getOptionValue("dataFile"), 2000000, EventType.STOCKEVENT,0,line.getOptionValue("dataDescription"));
				
				if(line.getOptionValue("queryToRun").toLowerCase().equals("Q2"))
		          {	AbstractWindow w = new SlidingWindow(Integer.parseInt(line.getOptionValue("windowSize")), 1);
					qp = new CardQ1(sd.get_inputQueue(), latch, w, 3);}
				if(line.getOptionValue("queryToRun").toLowerCase().equals("Q1"))
		          {	AbstractWindow w = new SlidingWindow(Integer.parseInt(line.getOptionValue("windowSize")), 1);
					qp = new StockQ1(sd.get_inputQueue(), latch, w, 3);}
				if(line.getOptionValue("queryToRun").toLowerCase().equals("q2stockevent"))
		          {	AbstractWindow w = new SlidingWindow(Integer.parseInt(line.getOptionValue("windowSize")), 1);
					qp = new StockRealDataQ1(sd.get_inputQueue(), latch, w, 3);}
				if(line.getOptionValue("queryToRun").toLowerCase().equals("qcreditcard"))
		          {	AbstractWindow w = new SlidingWindow(Integer.parseInt(line.getOptionValue("windowSize")), 1);
					qp = new CardQ1(sd.get_inputQueue(), latch, w, 3);}
				
				
				
			}
			
			else if(line.getOptionValue("mode").toLowerCase().equals("jointest")){
				
				
				
				AbstractWindow w = new SlidingWindow(Integer.parseInt(line.getOptionValue("windowSize")), 1);
				int dim=Integer.parseInt(line.getOptionValue("dimension"));
				 sd = new StreamDispatcher(line.getOptionValue("dataFile"), 2000000, EventType.STOCKEVENT,dim,line.getOptionValue("dataDescription"));
				if (dim == 2)
					qp = new JoinTest2Dim(sd.get_inputQueue(), latch, w, dim, Integer.parseInt(line.getOptionValue("size")), Integer.parseInt(line.getOptionValue("algorithme")));
				else if (dim == 3)
					qp = new JoinQTest(sd.get_inputQueue(), latch, w, dim, Integer.parseInt(line.getOptionValue("size")), Integer.parseInt(line.getOptionValue("algorithme")));
				else if (dim == 4) {
					qp = new JoinTest4Dim(sd.get_inputQueue(), latch, w, dim, Integer.parseInt(line.getOptionValue("size")), Integer.parseInt(line.getOptionValue("algorithme")));
				}
				
			}
			
		
		// ResultWriter rw = new ResultWriter(1, qp.get_outputQueue(),
		// OUTPUT_DIRC, latch);
		//
		// Thread writer = new Thread(rw);
		//
		// writer.start();

		Thread queryProc = new Thread(qp);

		queryProc.start();

		Thread inputProc = new Thread(sd);

		inputProc.start();

		// System.out.println("here");
		measure.notifyStart(1);

		try {
			latch.await();
		} catch (final InterruptedException e) {

			System.out.println("Error");
			// logger.error("Error while waiting for the program to end", e);
		}
		/// System.out.println("Error");
		measure.notifyFinish(1);
		measure.setProcessedRecords(sd.getRecords());
		System.out.println("Num of Total Matches: " + num0fMatches);
		measure.outputMeasure();
	}

}
