package org.rcep;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import org.event.EventType;
import org.inputmanager.StreamDispatcher;
import org.outputmanager.QueryProcessorMeasure;
import org.queryprocessor.AbstractQueryProcessor;
import org.queryprocessor.StockRTree;
import org.windowsemantics.AbstractWindow;
import org.windowsemantics.SlidingWindow;

public class AbstractAppForTree {
	// final static Logger logger = LoggerFactory.getLogger(AbstractApp.class);

	private static String INPUT_FILE = "./src/main/java/org/labhc/iofiles/data_test.txt";
	// private static String INPUT_FILE =
	// "/Users/sydgillani/Documents/Post-Doc/Post-tests/card.txt";
	private static String OUTPUT_DIRC = "./src/main/java/org/labhc/iofiles/";

	final static QueryProcessorMeasure measure = new QueryProcessorMeasure();
	public static int num0fMatches = 0;

	public static void start(String filename, int window, int algo) throws IOException {

		// INPUT_FILE = file;

		final CountDownLatch latch = new CountDownLatch(1); /// 2
		StreamDispatcher sd = new StreamDispatcher(filename, 2000000, EventType.STOCKEVENT, 3);

		AbstractWindow w = new SlidingWindow(window, 1);

		AbstractQueryProcessor qp = new StockRTree(sd.get_inputQueue(), latch, w, 4, algo, window);
		/**
		 * test for the Dimensions
		 */

		// ResultWriter rw = new ResultWriter(1, qp.get_outputQueue(),
		// OUTPUT_DIRC, latch);
		//
		// Thread writer = new Thread(rw);
		//
		// writer.start();

		Thread queryProc = new Thread(qp);

		queryProc.start();

		Thread inputProc = new Thread(sd);

		inputProc.start();

		// System.out.println("here");
		measure.notifyStart(1);

		try {
			latch.await();
		} catch (final InterruptedException e) {

			System.out.println("Error");
			// logger.error("Error while waiting for the program to end", e);
		}
		/// System.out.println("Error");
		measure.notifyFinish(1);
		measure.setProcessedRecords(sd.getRecords());
		System.out.println("Num of Total Matches: " + num0fMatches);
		measure.outputMeasure();
	}

}
