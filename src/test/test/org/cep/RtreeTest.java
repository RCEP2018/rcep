package test.org.cep;

import org.baseline.RTree;

public class RtreeTest {

	public static void main(String args[]) {
		RTree<Integer> r = new RTree<>(50, 3, 3);

		r.insert(new float[] { 1, 2, 6 }, 5);
		r.insert(new float[] { 2, 4, 8 }, 6);
		r.insert(new float[] { 2, 4, 9 }, 8);
		System.out.println(r.search(new float[] { 2, 3, 6 }, new float[] { 2, 4, 8 }));

		// r.search(coords, dimensions)

	}

}
