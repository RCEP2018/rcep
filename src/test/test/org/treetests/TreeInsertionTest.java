package test.org.treetests;

//import static org.labhc.queryprocessor.AbstractQueryProcessor.masks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.lucene.util.OpenBitSet;
import org.btreeUtils.Value;
import org.event.Event;
import org.event.SimpleEvent;
import org.event.StockEvent;
import org.eventstore.EventStore;
import org.queryprocessor.AbstractQueryProcessor;
import org.windowsemantics.AbstractWindow;
import org.zvalueencoder.DimensionException;
import org.zvalueencoder.ZIndex;
import org.zvalueencoder.Zregion;

public class TreeInsertionTest extends AbstractQueryProcessor {
	int i = 0;
	ZIndex lower = null;

	ZIndex higher = null;

	ZregionJoinTest joint = new ZregionJoinTest();

	public TreeInsertionTest(LinkedBlockingQueue<Event> i, CountDownLatch l, AbstractWindow w) throws IOException {
		super(i, l, w);
		// TODO Auto-generated constructor stub
	}

	protected void process(Event e) throws IOException, DimensionException {
		StockEvent ce = (StockEvent) e;

		int[] values = new int[] { ce.price, ce.vol };

		ZIndex key = new ZIndex(values);
		i++;
		if (i == 1) {
			lower = new ZIndex(new int[] { 1, 1 });
			;
		} else if (i == 14) {
			higher = new ZIndex(new int[] { 7, 7 });
		}

		// System.out.println(i);

		EventStore store = getStore(ce);

		// store.tree().insert(key, new Value(ce, ce.timestamp), false);
		// System.out.println(++i);
		store.tree().insert(key, new Value(ce, values, ce.timestamp), true);

		if (i == 14) {
			// create range
			ArrayList<Value> results = store.tree().searchRange(lower, higher, minmaxInter);

			ArrayList<Zregion> finallist = new ArrayList<>();

			Zregion r1 = new Zregion(2);
			results.get(0).id_a = 0;
			r1.add(results.get(0));
			// r1.add(results.get(1));

			finallist.add(r1);
			// int diff = results.get(1).getNextCommonBits();
			Value v = results.get(0);
			for (int i = 1; i < results.size(); i++) {

				if (results.get(i).getGroup().equals(v.getGroup())) {
					results.get(i).id_a = i;
					finallist.get(finallist.size() - 1).add(results.get(i));
				} else {

					Zregion r2 = new Zregion(2);
					results.get(i).id_a = i;
					r2.add(results.get(i));
					finallist.add(r2);

					v = results.get(i);
				}

			}

			System.out.println();

			joint.implementJoins(finallist, finallist, results, results, new String[] { "<", "<" });
		}

		// Check if the insertion provides the right behavior

	}

	private EventStore getStore(StockEvent c) throws IOException {

		EventStore store = (store_1.getStoreType() == 0) ? store_1 : store_2;

		if (window.startTimeofWindow > store.getEndTime() && !store.tree().hasData()) {

			store.tree().clear(); // /change it later, making it null and
		} // reinitialising a new tree

		store = (store_1.getStoreType() == 1) ? store_1 : store_2;

		if (c.timestamp > store.getEndTime()) {
			// /stop putting in this tree and select the other tree
			EventStore primary_store = (store_1.getStoreType() == 0) ? store_1 : store_2;

			store.setStoreType(0); // /make it a secondary store since the new
									// event will be outside its defined window
			primary_store.setStoreType(1); // make it the primary one and put
											// the event in this one
			primary_store.setStartTime(c.timestamp);
			primary_store.setEndTime(c.timestamp + window.windowLength);

			store = primary_store;
		}

		return store;

	}

	@Override
	protected void process(SimpleEvent e) throws IOException, DimensionException {
		// TODO Auto-generated method stub

	}

	@Override
	protected boolean isInrangeB(int[] min, int[] max, int[] point) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected void joinSingleRegions(OpenBitSet[] listbitsets, Zregion r1, Zregion r2, String[] phis,
			ArrayList<ZIndex> valB) {
		// TODO Auto-generated method stub

	}

	@Override
	protected boolean isInrangeA(int[] min, int[] max, int[] point) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int dominates_generic(int[] max_r1, int[] min_r1, int[] max_r2, int[] min_r2, String[] phis) {
		// TODO Auto-generated method stub
		return 0;
	}

}
