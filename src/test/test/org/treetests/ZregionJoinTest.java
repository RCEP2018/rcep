package test.org.treetests;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.btreeUtils.Value;
import org.zvalueencoder.Zregion;

public class ZregionJoinTest {
	public static void main(String[] args) {

		ArrayList<Zregion> regions = new ArrayList<>();

		Zregion z1 = new Zregion(2);

	}

	public void implementJoins(ArrayList<Zregion> region1, ArrayList<Zregion> region2, ArrayList<Value> fullregion1,
			ArrayList<Value> fullregion2, String[] phis) {
		BitSet[] listbitsets = new BitSet[fullregion1.size()];

		// where each bitset is equal to the size of the fullregion 2;

		for (int i = 0; i < listbitsets.length; i++) {
			listbitsets[i] = new BitSet(fullregion2.size());
		}
		/// r1 is the outer relation and r2 is the inner one.
		int _startRegion1 = 0, _startRegion2 = 0;

		int _endRegion1 = 0, _endRegion2 = 0;
		for (Zregion r1 : region1) {

			_endRegion1 = _startRegion1 + r1.getValues().size() - 1;

			/**
			 * If the previous region, i.e. prev(r1) dominates this one then we
			 * can reuse its results as well. Add this if statement in a new
			 * function over here
			 */

			for (Zregion r2 : region2) {

				_endRegion2 = _startRegion2 + (r2.getValues().size() - 1);

				int dom = dominates_generic(r1.max, r1.min, r2.max, r2.min, phis);
				if (dom == phis.length) {
					/// set all the bits to 1 for each entry in r1 for r2
					this.setBits(listbitsets, _startRegion1, _endRegion1, _startRegion2, _endRegion2);
					// setBitsFunction
				}
				// } else if (dom == 0) {
				// this.UnsetBits(listbitsets, _startRegion1, _endRegion1,
				// _startRegion2, _endRegion2);
				//
				// }

				else

				{

					// then employ the standard join algorithm
					// send the region r1 and r2 to the older function which
					// will
					// join it in a pariwise manner.

					joinSingleRegions(listbitsets, r1, r2, phis);

				}

				_startRegion2 = _endRegion2 + 1;
			}

			_startRegion1 = _endRegion1 + 1;
		}
		System.out.println();

		kleeneplus(4, listbitsets[3]);

		// System.out.println(listbitsets[0].toLongArray());

		// System.out.println(listbitsets[0].toString());

	}

	public void joinSingleRegions(BitSet[] listbitsets, Zregion r1, Zregion r2, String[] phis) {

		/// Sort them acording to the timestamps to make it event sequences
		List<Value> a = null;// r1.getValues();
		List<Value> b = null;// r2.getValues();
		Collections.sort(a, new TimeComparator());

		Collections.sort(b, new TimeComparator());

		// int _ns1 = _s1, _ns2 = _s2, _ne1 = _e1, _ne2 = _e2;

		int numofsetbits = 0;
		/// create a list of bitsets, with size of A and each elements of size b

		/// send the size of b and a from the top
		int start = b.size() - 1;
		/// loop over A and B

		for (int i = a.size() - 1; i >= 0; i--) {
			numofsetbits = 0;
			//// if the event dominates the other one
			// System.out.println(a.get(i).event.toString());

			/**
			 * Check if if this value can actually match with any other value in
			 * B or not using the max and min of region r2
			 */

			if (!dominates_single(r2.getMax(), r2.getMin(), a.get(i).values, phis)) {
				continue;
			}

			if (i != a.size() - 1) {
				// System.out.println("Price i " + a.get(i).values[1] + " " + "
				// Price-- i+1 " + a.get(i + 1).values[1]);
				if (a.get(i).values[1] < a.get(i + 1).values[1] && listbitsets[a.get(i + 1).id_a] != null
						&& !listbitsets[a.get(i + 1).id_a].isEmpty()) {

					int limit = 0;

					while (limit <= b.size() - 1) {
						if (listbitsets[a.get(i + 1).id_a].get(b.get(limit).id_a))
							listbitsets[a.get(i).id_a].set(b.get(limit).id_a);
						else if (b.get(limit).values[1] > a.get(i).values[1]) {

							listbitsets[a.get(i).id_a].set((b.get(limit).id_a));

							// } else {
							// listbitsets[a.get(i).id].clear((b.get(limit).id));
							// }

						}
						limit++;
					}

				} else {

					start = b.size() - 1;
					// _ne2 = _e2;
				}
				// _e2--;

			}

			while (start >= 0) {
				// System.out.println("Time B " + b.get(start).values[0] + "
				// Time A " + a.get(i).values[0]);
				// System.out.println("Price B " + b.get(start).values[1] + "
				// Price A " + a.get(i).values[1]);

				if (b.get(start).values[0] > a.get(i).values[0] && (b.get(start).values[1] > a.get(i).values[1])) {

					// if (listbitsets[i] == null) {
					// listbitsets[i] = new BitSet(b.size());
					// }

					listbitsets[a.get(i).id_a].set(b.get(start).id_a);

					start--;
					// _ne2--;
					numofsetbits++;
				} else if (b.get(start).values[0] < a.get(i).values[0]) {

					break;
				} else {
					start--;
					// _ne2--;
				}
			}
			// _ne1--;

		}

	}

	public void setBits(BitSet[] listbitsets, int _startRegion1, int _endRegion1, int _startRegion2, int _endRegion2) {
		for (int i = _startRegion1; i <= _endRegion1; i++) {
			// For each such subregion, i.e. an entry i, set the bits in the
			// given range.
			listbitsets[i].set(_startRegion2, _endRegion2 + 1);

		}
	}

	public void UnsetBits(BitSet[] listbitsets, int _startRegion1, int _endRegion1, int _startRegion2,
			int _endRegion2) {
		for (int i = _startRegion1; i <= _endRegion1; i++) {
			// For each such subregion, i.e. an entry i, set the bits in the
			// given range.
			listbitsets[i].clear(_startRegion2, _endRegion2);

		}
	}

	public int dominates_generic(int[] max_r1, int[] min_r1, int[] max_r2, int[] min_r2, String[] phis) {
		// the phis contains the condition for each dimensions
		int correct = 0;
		for (int i = 0; i < phis.length; i++) {

			if (phis[i].equals("<") && max_r1[i] <= min_r2[i]) {
				// max of region1 < min of region2
				correct++;
			} else if (phis[i].equals(">") && min_r1[i] >= max_r2[i]) {
				correct++;
			}

		}

		return correct;

	}

	public boolean dominates_single(int[] max_r, int[] min_r, int[] point, String[] phis) {
		// the phis contains the condition for each dimensions

		for (int i = 0; i < phis.length; i++) {

			if (phis[i].equals("<") && !(point[i] < max_r[i])) {
				return false;
			} else if (phis[i].equals(">") && !(point[i] > min_r[i])) {
				return false;
			}

		}

		return true;

	}

	public int not_dominates_generic(int[] max_r1, int[] min_r1, int[] max_r2, int[] min_r2, String[] phis) {
		// the phis contains the condition for each dimensions
		int incorrect = 0;
		for (int i = 0; i < phis.length; i++) {

			if (phis[i].equals("<") && max_r1[i] >= min_r2[i]) {
				// max of region1 < min of region2
				incorrect++;
			} else if (phis[i].equals(">") && min_r1[i] <= max_r2[i]) {
				incorrect++;
			}

		}

		return incorrect;

	}

	public boolean dominates(int[] m1, int[] m2, String theta) {
		// If time is less and price is greater

		if (m1[0] < m2[0] && m1[1] > m2[1]) {
			return true;
		}

		return false;

	}

	public static class TimeComparator implements Comparator<Value> {

		@Override
		public int compare(Value o1, Value o2) {

			return o1.values[0] - o2.values[0] > 0 ? 1 : -1;

		}
	}

	public void kleeneplus(int bits, BitSet b) {
		// int[] setbits = new int[b.cardinality()];
		// int l = 0;
		// for (int i = b.nextSetBit(0); i != -1; i = b.nextSetBit(i + 1)) {
		// setbits[l] = i;
		// l++;
		// }

		int max = 1 << bits;

		for (int i = 1; i < max; i++) {
			int index = 0;
			for (int j = i; j > 0; j >>= 1) {
				if ((j & 1) == 1) {

					// System.out.print(b.nextSetBit(index + 1) + " " + "Correct
					// " + setbits[index]);
					// if (b.get(index))
					// System.out.print(index + " " + "Correct " +
					// setbits[index]);
					// else
					// System.out.print(b.nextSetBit(index) + "Correct " +
					// setbits[index]);
					// get this bit if its not set then get next set bit?
					// int k = 0;
					int index_2 = 0;
					// while (k <= index) {
					// index2 = b.nextSetBit(index2);
					// index2++;
					// k++;
					// }

					for (int k = 0; k <= index; k++) {
						index_2 = b.nextSetBit(index_2);
						index_2++;
					}

					System.out.print(index_2 - 1 + "  "); // + " " + "Correct "
															// +
															// setbits[index]);

				}
				index++;
			}

			System.out.println();

		}
	}
}
