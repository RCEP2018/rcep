package test.org.treetests;

import java.math.BigInteger;
import java.util.BitSet;

import org.zvalueencoder.RangeSearchTest;
import org.zvalueencoder.ZIndex;

import com.github.davidmoten.rtree.RTree;
import com.github.davidmoten.rtree.geometry.Geometries;
import com.github.davidmoten.rtree.geometry.Geometry;

import javolution.util.FastBitSet;

public class ZregionTest {
	static RangeSearchTest st = new RangeSearchTest();

	public static void main(String args[]) {
		// xorsoultion();
		// xorsoultion(8, 2, 7, 3);

		RTree<Integer, Geometry> tree = RTree.star().maxChildren(6).create();

		tree = tree.add(0, Geometries.point(10, 20));

		FastBitSet fbs = new FastBitSet();
		// msbBig(7);
		// bitvector();
		// splitRanges();
		ZIndex z1 = new ZIndex(new int[] { 5, 5 });
		ZIndex z2 = new ZIndex(new int[] { 9, 8 });
		ZIndex z3 = new ZIndex(new int[] { 3, 5 });

		try {
			// cmsbarrays(z1, z2);
			st.decomposeintoTwo(z1, z2);
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(common_bits(2, 4));

		int[] v = { 7, 8, 9 };

		BitSet bitset1 = new BitSet(8);
		BitSet bitset2 = new BitSet(8);
		BitSet bitset3 = new BitSet(8);

		bitset1.set(0);
		bitset1.set(6);

		bitset2.set(5);
		bitset2.set(6);

		bitset3.set(0);
		bitset3.set(6);

		if (!bitset1.equals(bitset2))
			System.out.println("Ok Not equal");

		if (bitset1.equals(bitset3))
			System.out.println("OK Its equal");

		// commonbitFinal(v, new BitSet(v.length));

		/*
		 * multidimxor(); int[] v1 = { 5, 3, 2 }; int[] v2 = { 4, 2, 4 };
		 * genericXor(v1, v2); // multidimxor(); int[] values = { 1, 5, 6 };
		 * 
		 * int max = -1; int id = 0;
		 * 
		 * for (int i = 0; i < values.length; i++) { int msb =
		 * findPositionOfMSB(values[i]); if (msb > max) { max = msb; id = i; } }
		 * 
		 * /// to get the vitual z-address msb, its the maxmsb + the number of
		 * /// dimensions before it.
		 * 
		 * int msbofzaddress = max + id;
		 * 
		 * System.out.println("MSB of Z is " + msbofzaddress);
		 */
		// int bit = x & 0X00002000 >> 18;
		// findPositionOfMSB(x);
		// System.out.println(getBit(5, 0));

	}

	public static void cmsbarrays(ZIndex begin, ZIndex end) throws CloneNotSupportedException {
		ZIndex litMax = (ZIndex) end.clone();
		ZIndex bigMin = (ZIndex) begin.clone();
		int dim = st.dimToCompare(begin.getValues(), end.getValues());
		/**
		 * Common most significant bits example 01100101101 and 01100110010 will
		 * give 011001\\00000
		 */
		int cmsb = st.commonMSB(end.getValues()[dim], begin.getValues()[dim]);
		int mask = (~((1 << (cmsb + 1)) - 1) & 0xFF);
		bigMin.setValueOfDim(dim, (end.getValues()[dim] & mask) | (1 << cmsb));
		litMax.setValueOfDim(dim, (end.getValues()[dim] & mask) | ((1 << cmsb) - 1));
	}

	public static void msbBig(int num) {
		int n = 7;
		BigInteger r = BigInteger.valueOf(num);
		BigInteger f = r.shiftRight(r.bitLength() - n);
		Byte result = Byte.valueOf(f.toString());
		// System.out.println(result);
		// System.out.println((int) (Math.log(num) / Math.log(2)) + 1);

		int x = 0;
		x = (num >> 24) & 0x7ffff;

		int from = 0;
		int to = 3;
		int mask = ((1 << (to - from + 1)) - 1) << from;
		System.out.println((num & mask) >> from);

	}

	public static void splitRanges() {
		int n = 3;
		int start = 5;
		int end = 15;
		int dis = (end - start);
		int chunk = dis / n;
		int remainder = dis % n;

		for (int i = 0; i < n; i++) {
			int next_end = start + chunk + remainder;
			System.out.println("First One :" + start + " -->" + next_end);

			start = next_end;

			if (remainder > 0)
				remainder -= 1;
		}

	}

	public static void bitvector() {
		long num = 0;
		int setbit = 3;

		num |= ((long) 1 << setbit);

		System.out.println(num);
		setbit = 2;
		num |= ((long) 1 << setbit);
		System.out.println(num);
	}

	public static void commonbitFinal(int[] v, BitSet b) {

		if (v.length == 0)
			return;

		String bits = "";
		int[] pos = new int[v.length];
		for (int i = 0; i < v.length; i++) {
			pos[i] = findPositionOfMSB(v[i]);

		}

		for (int i = 0; i < pos.length; i++) {

			if (i != pos.length - 1) {

				bits = bits + "  " + compare(pos[i], pos[i + 1], pos.length);

				if (compare(pos[i], pos[i + 1], pos.length)) {
					b.set(i);
				} else
					b.clear(i);

			} else {
				// int l = pos[i] >= pos[i - 1] ? 1 : 0;
				bits = bits + "  " + compare(pos[i], pos[i - 1], pos.length);
				if (compare(pos[i], pos[i - 1], pos.length)) {
					b.set(i);
				} else
					b.clear(i);

			}

		}

		System.out.println(bits);

	}

	public static boolean compare(int a, int b, int bits) {

		if (a < b)
			return false;
		else if (a > b && a >= bits)
			return true;
		else if (a > b && a < bits)
			return false;

		if (a == b && a >= bits)
			return true;
		else
			return false;

	}

	public static void commonbits(int[] v1) {

		int pos = 0;

		String bits = "";
		for (int i = 0; i < v1.length; i++) {
			int pos2 = 0;
			if (i != 0) {
				pos2 = findPositionOfMSB(v1[i]);

				if (pos > pos2 && pos2 - pos < v1.length) {

					if (i == 1)
						bits = "1" + "1";
					else
						bits = bits + "1";

				} else if (pos > pos2 && pos2 - pos >= v1.length) {

					if (i == 1)
						bits = "1" + "0";
					else
						bits = bits + "0";
				} else if (pos < pos2 && pos - pos2 < v1.length) {
					if (i == 1)
						bits = "0" + "1";
					else
						bits = bits + "1";

				} else if (pos < pos2 && pos - pos2 >= v1.length) {

					if (i == 1)
						bits = "0" + "0";
					else
						bits = bits + "0";

				} else if (pos == pos2 && pos < v1.length) {
					if (i == 1)
						bits = "0" + "0";
					else
						bits = bits + "0";

				} else if (pos == pos2 && pos >= v1.length) {
					if (i == 1)
						bits = "1" + "1";
					else
						bits = bits + "1";
				}

			} else {
				pos = findPositionOfMSB(v1[i]);
				continue;
			}

			pos = pos2;

		}

		System.out.println(bits);

	}

	public static void genericXor(int[] v1, int[] v2) {

		int[] xors = new int[v1.length];
		for (int i = 0; i < v1.length; i++) {

			xors[i] = v1[i] ^ v2[i];
		}

		recursiveoOne(xors, v1, v2, 0, 1);

	}

	public static int common_bits(int a, int b) {
		if (a == 0)
			return 0;
		if (b == 0)
			return 0;
		return ((a ^ ~b) & 1) + common_bits(a / 2, b / 2);
	}

	public static void recursiveoOne(int[] xors, int[] v1, int[] v2, int i, int j) {
		if (j == xors.length) {
			System.out.println("dimension to compare: " + i);

			if (v1[i] < v2[i]) {
				System.out.println("v2 is greater ");
			} else {
				System.out.println("v1 is greater ");
			}
			return;
		}

		if (xors[i] < xors[j] && xors[i] < (xors[i] ^ xors[j])) {
			recursiveoOne(xors, v1, v2, j, j + 1);
		} else {
			recursiveoOne(xors, v1, v2, i, j + 1);
		}

	}

	public static void multidimxor() {
		int a1 = 5;
		int b1 = 3;
		int c1 = 2;

		int a2 = 4;
		int b2 = 2;
		int c2 = 4;

		int a1a2 = a1 ^ a2;

		int b1b2 = b1 ^ b2;
		int c1c2 = c1 ^ c2;

		if (a1a2 < b1b2 && a1a2 < (a1a2 ^ b1b2)) {
			/// second dimension

			/// check second dimesnion with the third one
			if (b1b2 < c1c2 && b1b2 < (b1b2 ^ c1c2)) {
				/// third dimension
				System.out.println("Thirddimen");
			} else {
				System.out.println("second dimen");
			}

		} else {
			/// first dimension

			if (a1a2 < c1c2 && a1a2 < (a1a2 ^ c1c2)) {
				System.out.println("Third dimen");
			} else {
				System.out.println("First dimen");
			}

		}

	}

	public static void xorsoultion(int a, int b, int c, int d) {
		// int a = 1;
		// int b = 2;

		// int c = 2;
		// int d = 3;

		int ac = a ^ c;

		int bd = b ^ d;

		if (ac < bd && ac < (ac ^ bd)) {
			/// only compare the second dimension
			if (b < d)
				System.out.println("the second value (v2) is greater Z");
			else
				System.out.println("the first value (v1) is greater Z");

		} else {
			// only compare the first dimension

			if (a < c)
				System.out.println("the second (v2) value is greater Z");
			else
				System.out.println("the first value (v1) is greater Z");
		}

	}

	public static int findPositionOfMSB(int n) {
		int high = 31, low = 0;

		while (high - low > 1) {
			int mid = (high + low) / 2;
			int maskHigh = (1 << high) - (1 << mid);
			if ((maskHigh & n) > 0) {
				low = mid;
			} else {
				high = mid;
			}
		}
		System.out.println(n + ": MSB at " + low); // ". Between " +
													// (int)Math.pow(2, low) + "
		return low; // and " + (int)Math.pow(2,
		// low+1));

	}

	public static int getBit(int num, int position) {
		return (num >> position) & 1;
	}

	public static void settingbeitsTest() {

		int number = 0;
		int x = 0;
		int n = 0;
		/// chaning nth bit to 1:
		number ^= (-x ^ number) & (1 << n);

		// checking a bit
		int bit = (number >> x) & 1;

		// setting a bit at location x
		number |= 1 << x;

	}

}
