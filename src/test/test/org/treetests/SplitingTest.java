package test.org.treetests;

import java.util.ArrayList;
import java.util.List;

import org.zvalueencoder.ZIndex;

public class SplitingTest {

	public static void main(String[] args) {
		int[] masks = new int[32];
		for (int n = 0; n < 32; n++) {

			masks[n] = 1 << n;
		}
		List<ZIndex> fullLeaf = new ArrayList<>();
		ZIndex z1 = new ZIndex(new int[] { 5, 3, 2 }, 3, masks); // z1
																	// (000100011110)
		ZIndex z2 = new ZIndex(new int[] { 4, 2, 4 }, 3, masks); // z2
																	// (000101010000)
		ZIndex z3 = new ZIndex(new int[] { 5, 6, 2 }, 3, masks); // z3
																	// (000110011100)
		ZIndex z4 = new ZIndex(new int[] { 9, 2, 4 }, 3, masks); // z4
																	// (100001010100)
		ZIndex z5 = new ZIndex(new int[] { 10, 3, 2 }, 3, masks);// z5
																	// (100000111010)
		ZIndex z6 = new ZIndex(new int[] { 4, 2, 1 }, 3, masks); // z6
																	// (000100010001)
		ZIndex z7 = new ZIndex(new int[] { 9, 3, 8 }, 3, masks); // z7
																	// (101000010110)
		ZIndex z8 = new ZIndex(new int[] { 7, 5, 0 }, 3, masks); // z8
																	// (000110100110)

		// System.out.println("first different bit between zindex " +
		// z1.genericCommonBit(z2.getValues()));

		fullLeaf.add(z1);
		fullLeaf.add(z2);
		fullLeaf.add(z3);
		fullLeaf.add(z4);
		fullLeaf.add(z5);
		fullLeaf.add(z6);
		fullLeaf.add(z7);
		fullLeaf.add(z8);

		// not correct !!

		int msb = 0, temp = 0, msdim = 0;
		int result = 0;
		int mpmsb = 0;
		for (int dim = 0; dim < 3; dim++) {
			for (int i = 0; i < fullLeaf.size(); i++) {
				ZIndex xx = fullLeaf.get(i);
				int[] xxx = xx.getValues();
				result = result ^ xxx[dim];

			}
			temp = msb(result);
			if (temp >= msb) {
				msb = temp;
				msdim = dim;
			}

		}
		System.out.println("max position of the msb " + msb + "  " + msdim);
	}

	public static int msb(int vn) {
		int ndx = 0;
		while (1 < vn) {
			vn = (vn >> 1);
			ndx++;
		}
		return ndx;
	}

}
