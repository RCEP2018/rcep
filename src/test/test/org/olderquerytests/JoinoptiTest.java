package test.org.olderquerytests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;

import org.btreeUtils.Value;
import org.event.Event;

import test.org.olderquerytests.ExtendedStockQueryProcessor.TimeComparator;
import test.org.olderquerytests.Jointest.Vtest;

public class JoinoptiTest {
	public static void main(String[] args) throws IOException {

		JoinoptiTest t = new JoinoptiTest();
		Jointest js = new Jointest();

		Jointest.Vtest v1 = js.new Vtest(80, 5, 2);
		Jointest.Vtest v2 = js.new Vtest(200, 10, 3);
		Jointest.Vtest v3 = js.new Vtest(110, 8, 4);
		Jointest.Vtest v4 = js.new Vtest(100, 12, 7);

		Jointest.Vtest v5 = js.new Vtest(250, 11, 8);

		Jointest.Vtest v6 = js.new Vtest(90, 3, 15);

		List<Vtest> a = new ArrayList<>();

		a.add(v1);
		a.add(v2);
		a.add(v3);
		a.add(v4);

		List<Vtest> b = new ArrayList<>();

		b.add(v1);
		b.add(v2);
		b.add(v3);
		b.add(v4);
		b.add(v5);
		b.add(v6);

	}

	public void joinLists(List<Vtest> a, List<Vtest> b) {

		BitSet index = new BitSet(b.size());
		int start = b.size() - 1;
		/// int end = b.size();
		int pos = b.size() - 1;
		int numofsetbits = 0;
		boolean[] setbits = new boolean[b.size()];

		for (int i = a.size() - 1; i >= 0; i--) {

			// setbits = new boolean[b.size()];

			numofsetbits = 0;

			// price is the main sorted attribute
			// pos = b.size() - 1;

			/// compare with the previous bits

			if (i != a.size() - 1) {

				//

				boolean first = a.get(i).v > a.get(i + 1).v;

				boolean second = a.get(i).p < a.get(i + 1).p;
				boolean take = false;
				if (first == second) {

					take = !first;
					// comparePreviousBits(a.get(i), setbits, b, take, start);

				} else {
					start = b.size() - 1;
					setbits = new boolean[b.size()];
					pos = b.size() - 1;

				}

			}

			while (start >= 0) {
				if (a.get(i).t < b.get(start).t && a.get(i).p < b.get(start).p && a.get(i).v > b.get(start).v) {

					setbits[pos] = true;
					pos--;
					// index.set(pos, true);
					start--;

					numofsetbits++;
					/// create the
					/// matches over
					/// from start till the end of the list produce the patterns
				} /// here
				else if (a.get(i).t > b.get(start).t)
					break;
				else {
					boolean val = a.get(i).v > b.get(start).v && a.get(i).p < b.get(start).p;
					setbits[pos] = val;
					// index.set(pos, val);
					start--;

					pos--;
				}

			}

			/// output results
			// if (numofsetbits > 0)
			// outputResult(setbits, a.get(i), start, b);

		}

	}

	public void joinListopti(List<Value> a, List<Value> b, String bop, Event c)

	{

		Collections.sort(a, new TimeComparator());

		Collections.sort(b, new TimeComparator());

		int start = b.size() - 1;
		/// int end = b.size();
		int pos = b.size() - 1;
		int numofsetbits = 0;
		boolean[] setbits = new boolean[b.size()];

		for (int i = a.size() - 1; i >= 0; i--) {

			// setbits = new boolean[b.size()];

			numofsetbits = 0;

			// price is the main sorted attribute
			// pos = b.size() - 1;

			/// compare with the previous bits

			if (i != a.size() - 1) {

				// (b.get(start).compareTo(a.get(i), 0) >= 1 &&
				// b.get(start).compareTo(a.get(i), 1) >= 1
				// && b.get(start).compareTo(a.get(i), 2) < 0

				boolean first = a.get(i).compareTo(a.get(i + 1), 1) >= 1; // v >
																			// a.get(i
																			// +
																			// 1).v;

				boolean second = a.get(i).compareTo(a.get(i + 1), 2) < 0; // a.get(i).p
																			// <
																			// a.get(i
																			// +
																			// 1).p;
				boolean take = false;
				if (first == second) {

					take = !first;
					comparePreviousBits(a.get(i), setbits, b, take, start, numofsetbits);

				} else {
					start = b.size() - 1;
					setbits = new boolean[b.size()];
					pos = b.size() - 1;

				}

			}

			while (start >= 0) {
				if ((b.get(start).compareTo(a.get(i), 0) >= 1 && b.get(start).compareTo(a.get(i), 1) >= 1
						&& b.get(start).compareTo(a.get(i), 2) < 0)) {

					setbits[pos] = true;
					pos--;
					// index.set(pos, true);
					start--;

					numofsetbits++;
					/// create the
					/// matches over
					/// from start till the end of the list produce the patterns
				} /// here
				else if (b.get(start).compareTo(a.get(i), 0) < 0)
					break;
				else {
					boolean val = b.get(start).compareTo(a.get(i), 1) >= 1 && b.get(start).compareTo(a.get(i), 2) < 0;// a.get(i).v
																														// >
																														// b.get(start).v
																														// &&
																														// a.get(i).p
																														// <
																														// b.get(start).p;
					setbits[pos] = val;
					// index.set(pos, val);
					start--;

					pos--;
				}

			}

			/// output results
			// if (numofsetbits > 0)
			// outputResult(setbits, a.get(i), start, b);

		}

	}

	public void comparePreviousBits(Value a, boolean[] setbits, List<Value> b, boolean take, int s, int numofsetbits) {

		for (int i = setbits.length - 1; i >= s; i--) {

			if (setbits[i] == take) {
				/// check the conditions over here
				if (a.compareTo(b.get(i), 1) < 0 && a.compareTo(b.get(i), 2) >= 1) {
					numofsetbits++;
					setbits[i] = true;
				} else {
					setbits[i] = false;
				}
			}

		}

	}

	public void outputResult(boolean[] setbits, Value a, int s, List<Value> b) {

		if (s == -1)
			s = 0;
		for (int i = setbits.length - 1; i >= s; i--) {
			if (setbits[i])
				System.out.println("A-Values " + a + "  B-Values" + b.get(i));
		}
	}
}
