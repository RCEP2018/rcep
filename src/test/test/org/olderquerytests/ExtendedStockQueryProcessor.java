package test.org.olderquerytests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.lucene.util.OpenBitSet;
import org.benchmark.joins.IEjoin;
import org.btreeUtils.Value;
import org.event.Event;
import org.event.EventType;
import org.event.SimpleEvent;
import org.event.StockEvent;
import org.eventstore.EventStore;
import org.queryprocessor.AbstractQueryProcessor;
import org.windowsemantics.AbstractWindow;
import org.zvalueencoder.DimensionException;
import org.zvalueencoder.ZIndex;
import org.zvalueencoder.Zregion;

public class ExtendedStockQueryProcessor extends AbstractQueryProcessor {
	int i = 0;
	int counter = 0;
	int times_dominated = 0;
	int no_dominated = 0;

	public ExtendedStockQueryProcessor(LinkedBlockingQueue<Event> i, CountDownLatch l, AbstractWindow w)
			throws IOException {
		super(i, l, w);
		// TODO Auto-generated constructor stub
	}

	protected void process(Event e) throws IOException, DimensionException {
		window.initliaseWindow();

		if (e.eventType == EventType.STOCKEVENT) {
			i++;

			StockEvent ce = (StockEvent) e;

			stockABCQueryBaseLine(ce);

			/// create the array for event data

			int[] values = { ce.timestamp, ce.price, ce.vol };

			ZIndex key = new ZIndex(values);

			window.updateWindow(ce.timestamp);

			EventStore store = getStore(ce);

			// store.tree().insert(key, new Value(ce, ce.timestamp), false);

			store.tree().insert(key, new Value(ce, values, ce.timestamp), false);

			// store.tree().insertUB(key, new Value(ce, ce.timestamp), false);
			updateMaxMin(ce.timestamp, ce.price, ce.vol);
			// }

		}

	}

	private ZIndex[] createRangesForB(StockEvent ce) {

		ZIndex minZRange = new ZIndex(new int[] { minmax.min[0], ce.price, minmax.min[2] });

		ZIndex maxZRange = new ZIndex(new int[] { minmax.max[0], minmax.max[1], minmax.max[2] });

		return new ZIndex[] { minZRange, maxZRange };
	}

	private ZIndex[] createRangesForA(StockEvent be, int cprice) {
		// /For now hardcoded
		// /For A Sequence;

		ZIndex minZRange = new ZIndex(new int[] { minmax.min[0], minmax.min[1], minmaxInter.min[2] });

		ZIndex maxZRange = new ZIndex(new int[] { minmaxInter.max[0] - 1, minmaxInter.max[1] - 1, minmax.max[2] });

		// Integer minArange = zval.Zorder2d(minmax.min[0], minmax.min[1]);
		// Integer maxARange = zval.Zorder2d(minmax.max[0], ce.vol);

		return new ZIndex[] { minZRange, maxZRange };

	}

	/*
	 * private ZIndex[] createRangesForA2D(Value minB, Value maxB, int cprice) {
	 * // /For now hardcoded // /For A Sequence;
	 * 
	 * ZIndex minZRange = new ZIndex(new int[] { minB.min[0], minB.min[1],
	 * minB.min[2] });
	 * 
	 * ZIndex maxZRange = new ZIndex(new int[] { maxB.max[0] - 1, maxB.max[1] -
	 * 1, maxB.max[2] });
	 * 
	 * // Integer minArange = zval.Zorder2d(minmax.min[0], minmax.min[1]); //
	 * Integer maxARange = zval.Zorder2d(minmax.max[0], ce.vol);
	 * 
	 * return new ZIndex[] { minZRange, maxZRange };
	 * 
	 * }
	 */

	private ZIndex[] createRangesForAPrice() {
		// /For now hardcoded
		// /For A Sequence;

		ZIndex minZRange = new ZIndex(new int[] { minmax.min[0], minmaxInter.min[1], minmaxInter.min[2] });

		ZIndex maxZRange = new ZIndex(new int[] { minmax.max[0], minmaxInter.max[1], minmax.max[2] });
		// Integer minArange = zval.Zorder2d(minmax.min[0], minmax.min[1]);
		// Integer maxARange = zval.Zorder2d(minmax.max[0], ce.vol);

		return new ZIndex[] { minZRange, maxZRange };

	}

	private ZIndex[] createRangesForAVolume() {
		// /For now hardcoded
		// /For A Sequence;

		ZIndex minZRange = new ZIndex(new int[] { minmax.min[0], minmax.min[1], minmaxInter.min[2] });

		ZIndex maxZRange = new ZIndex(new int[] { minmax.max[0], minmaxInter.min[1], minmaxInter.max[2] });

		// Integer minArange = zval.Zorder2d(minmax.min[0], minmax.min[1]);
		// Integer maxARange = zval.Zorder2d(minmax.max[0], ce.vol);

		return new ZIndex[] { minZRange, maxZRange };

	}

	private ZIndex[] createRangesForADominant() {
		// /For now hardcoded
		// /For A Sequence;

		ZIndex minZRange = new ZIndex(new int[] { minmax.min[0], minmax.min[1], minmaxInter.max[2] });

		ZIndex maxZRange = new ZIndex(new int[] { minmax.max[0], minmaxInter.min[1], minmax.max[2] });

		// Integer minArange = zval.Zorder2d(minmax.min[0], minmax.min[1]);
		// Integer maxARange = zval.Zorder2d(minmax.max[0], ce.vol);

		return new ZIndex[] { minZRange, maxZRange };

	}

	// TODO Syed
	private void stockABCQueryBaseLine(StockEvent ce) throws IOException, DimensionException {
		ZIndex[] ranges = createRangesForB(ce);
		List<Value> valB = queryStore(ce, ranges);

		/// from B get the A bindings
		if (valB == null || valB.isEmpty())
			return;

		// Collections.sort(valB, new TimeComparator());

		// ranges= createRangesForA(ce);
		ranges = createRangesForA((StockEvent) valB.get(valB.size() - 1).event, ce.price);

		// ranges=createRangesForAVolume();
		// ranges =createRangesForADominant();
		// ranges= createRangesForAPrice();

		List<Value> valA = queryStore(ce, ranges);

		if (valA == null || valA.isEmpty())
			return;

		// computeMatches(valA, valB, ce);

		// createResultsABC(valA, valB, "<", ce.price);
		// long startTime = System.nanoTime();
		// createResultsIEjoin3(valA, valB, ce);
		// for + for A-> B

		newOutputMethod(valA, valB, ce);
		// computeMatches(valA, valB, ce);
		// createResultsABCComplexJoin(valA, valB, "<", ce);
		// long endTime = System.nanoTime();
		// long totalTime = endTime - startTime;
		// System.out.println("Execution time: " + totalTime + "ns");
	}

	// TODO abderrahmen with simple skyline
	/*
	 * private void stockABCQueryWithChunck(StockEvent ce) throws IOException,
	 * DimensionException { ZIndex[] ranges = createRangesForB(ce);
	 * 
	 * ArrayList<ArrayList<Value>> chunkedValB = queryStorebyChunk(ce, ranges);
	 * /// from B get the A bindings if (chunkedValB == null ||
	 * chunkedValB.isEmpty()) return;
	 * 
	 * ranges = createRangesForA((StockEvent) chunkedValB.get(0).get(0).event,
	 * ce.price); // ForAChunck(chunkedValB.get(0).get(0), //
	 * chunkedValB.get(chunkedValB.size() - 1) // .get(0), ce.price);
	 * 
	 * ArrayList<ArrayList<Value>> chunkedValA = queryStorebyChunk(ce, ranges);
	 * if (chunkedValA == null || chunkedValA.isEmpty()) return;
	 * 
	 * long startTime = System.nanoTime();
	 * 
	 * // for (int i = 0; i < chunkedValA.size(); i++) { // for (int j = 0; j <
	 * chunkedValB.size(); j++) { // if (chunkedValA.get(i).get(0).min[2] > //
	 * chunkedValB.get(j).get(0).max[2] // && chunkedValA.get(i).get(0).max[1] <
	 * // chunkedValB.get(j).get(0).min[1]) { // //
	 * System.out.println("it s possible "); // System.out.println("A " +
	 * chunkedValA.get(i)); // System.out.println("B " + chunkedValB.get(j)); //
	 * // } // } // }
	 * 
	 * createResultsABCComplexJoinWithChunck(chunkedValA, chunkedValB, "<", ce);
	 * long endTime = System.nanoTime(); long totalTime = endTime - startTime;
	 * System.out.println("Execution time: " + totalTime + "ns"); }
	 */

	/*
	 * private void stockABCQuerySkyline(StockEvent ce) throws IOException,
	 * DimensionException { ZIndex[] ranges = createRangesForB(ce); List<Value>
	 * valB = queryStore(ce, ranges); ArrayList<ArrayList<Value>> chunkedValB =
	 * queryStorebyChunk(ce, ranges); /// from B get the A bindings if (valB ==
	 * null || valB.isEmpty()) return;
	 * 
	 * // Collections.sort(valB, new TimeComparator());
	 * 
	 * // ranges= createRangesForA(ce); ranges = createRangesForA((StockEvent)
	 * valB.get(valB.size() - 1).event, ce.price); List<Value> valA =
	 * queryStore(ce, ranges);
	 * 
	 * ranges = createRangesForA((StockEvent) valB.get(valB.size() - 1).event,
	 * ce.price);
	 * 
	 * List<Value> dominatedvalA = queryStore(ce, ranges); if (valA == null ||
	 * valA.isEmpty()) return;
	 * 
	 * // computeMatches(valA, valB, ce);
	 * 
	 * // createResultsABC(valA, valB, "<", ce.price); // long startTime =
	 * System.nanoTime(); // createResultsIEjoin3(valA, valB, ce); // for + for
	 * A-> B
	 * 
	 * createResultsABCComplexJoin(valA, valB, "<", ce); // long endTime =
	 * System.nanoTime(); // long totalTime = endTime - startTime; //
	 * System.out.println("Execution time: " + totalTime + "ns");
	 * 
	 * }
	 */

	private void computeMatches(List<Value> valA, List<Value> valB, StockEvent ce) {

		Collections.sort(valA, new TimeComparator());

		Collections.sort(valB, new TimeComparator());
		for (Value va : valA) {

			// int id = (int) va.event.id;
			StringBuilder sb = new StringBuilder();
			boolean m = false;
			// get_outputQueue().add(Integer.toString(va.event.id));
			StockEvent a = ((StockEvent) va.event);
			// sb.append(" ################# ");
			// sb.append("\n");
			// sb.append(a.toString());
			// sb.append(Integer.toString(va.event.id));
			Value bcheck = null;
			for (Value vb : valB) {

				// System.out.println(va.get_window().get(0) + " " +
				// vb.get_window().get(0));

				// System.out.println(((StockEvent) va.event).price + " " +
				// ((StockEvent) vb.event).price);

				if (((StockEvent) va.event).price < ((StockEvent) vb.event).price
						&& ((StockEvent) va.event).timestamp < ((StockEvent) vb.event).timestamp
						&& ((StockEvent) vb.event).price > ce.price
						&& ((StockEvent) va.event).vol > ((StockEvent) vb.event).vol) {

					// System.out.println("Event A " + ((StockEvent)
					// va.event).price + " Event B "
					// + ((StockEvent) vb.event).price + " Event C " +
					// ce.price);
					// sb.append("\n");

					StockEvent b = ((StockEvent) vb.event);
					// sb.append(Integer.toString(vb.event.id));
					sb.append(b);

					if (!m) {
						bcheck = vb;
					} else {

						// if (((StockEvent) vb.event).price > ((StockEvent)
						// bcheck.event).price) {
						// m = true;
						// } else
						// m = false;
					}

					m = true;
					// get_outputQueue().add("," +
					// Integer.toString(vb.event.id));

				}
			}

			if (m) {

				// sb.append("," + Integer.toString(ce.id));
				// sb.append("\n");
				//
				// sb.append(ce.toString());
				// sb.append("\n");
				counter++;

				System.out.println(counter);
				// sb.append(" ######### Match Number" + counter +
				// "################# ");
				//
				// get_outputQueue().add(sb.toString());
				// get_outputQueue().add("\n");
			}

			// get_outputQueue().add("," + Integer.toString(ce.id));

			// get_outputQueue().add("\n");
		}

	}

	private void createResultsIEjoin3(List<Value> a, List<Value> b, StockEvent c) throws IOException {
		IEjoin.IEJoin3((ArrayList<Value>) a, (ArrayList<Value>) b, get_outputQueue(), c);

	}

	private void createResultsIEjoin2(List<Value> a, List<Value> b, StockEvent c) throws IOException {
		IEjoin.IEJoin2((ArrayList<Value>) a, (ArrayList<Value>) b, get_outputQueue(), c);

	}

	/**
	 * Simple SEQ(A,B+,C) with A.price< B.price and from range query B.price <
	 * C.price
	 * 
	 * @param a
	 * @param b
	 * @param bop
	 * @param c
	 * @throws IOException
	 */
	private void createResultsABC(List<Value> a, List<Value> b, String bop, int c) throws IOException {

		int start = b.size() - 1;
		/// int end = b.size();
		int pos = 0;
		int numofsetbits = 0;
		int[] setbits = new int[b.size()];
		for (int i = a.size() - 1; i >= 0; i--) {
			setbits = new int[b.size()];

			numofsetbits = 0;
			start = b.size() - 1;
			// check.clear();
			pos = b.size() - 1;
			while (start >= 0) {
				if (b.get(start).compareTo(a.get(i), 1) >= 1) {

					setbits[pos] = start;
					pos--;

					// outputCombinations2(a.get(i).p, b, start, b.size(),
					// check);
					start--;
					// end--;
					numofsetbits++;
					/// matches over
					/// from start till the end of the list produce the patterns
				} /// here
				else if (b.get(start).compareTo(a.get(i), 1) < 1)
					break;
				else {
					start--;
				}

				if (numofsetbits > 0)
					outputCombinations(a.get(i).getPredicate(1), b, numofsetbits, b.size() - 1, c, setbits);

			}

		}

	}

	/**
	 * Another Query SEQ(A,B+,C) with A.price < B.price && A.vol > C.vol from
	 * range query B.price < c.Price
	 * 
	 * @param a
	 * @param b
	 * @param bop
	 * @param c
	 * @throws IOException
	 */

	private void createResultsABCvol(List<Value> a, List<Value> b, String bop, Event c) throws IOException {

		Collections.sort(a, new PriceComparator());

		Collections.sort(b, new PriceComparator());

		int start = b.size() - 1;
		/// int end = b.size();
		int pos = 0;
		int numofsetbits = 0;
		int[] setbits = new int[b.size()];
		for (int i = a.size() - 1; i >= 0; i--) {
			setbits = new int[b.size()];

			numofsetbits = 0;
			start = b.size() - 1;
			// check.clear();
			pos = b.size() - 1;
			while (start >= 0) {
				if (b.get(start).compareTo(a.get(i), 1) >= 1 && a.get(i).compareToEvent(c, 2) < 1) {

					setbits[pos] = start;
					pos--;

					// outputCombinations2(a.get(i).p, b, start, b.size(),
					// check);
					start--;
					// end--;
					numofsetbits++;
					/// matches over
					/// from start till the end of the list produce the patterns
				} /// here
				else if (b.get(start).compareTo(a.get(i), 1) < 1)
					break;
				else {
					start--;
				}

				if (numofsetbits > 0)
					outputCombinations(a.get(i).getPredicate(1), b, numofsetbits, b.size() - 1, ((StockEvent) c).price,
							setbits);

			}

		}

	}

	/**
	 * Using the pattern SEQ (A,B+,C) where A.price < B.price && A.vol > B.vol
	 */

	private void createResultsABCComplexJoin(List<Value> a, List<Value> b, String bop, Event c) throws IOException {

		Collections.sort(a, new TimeComparator());

		Collections.sort(b, new TimeComparator());

		int start = b.size() - 1;
		/// int end = b.size();
		int pos = 0;
		int numofsetbits = 0;
		int[] setbits; // = new int[b.size()];
		for (int i = a.size() - 1; i > 0; i--) {
			setbits = new int[b.size()];

			numofsetbits = 0;
			start = b.size() - 1;

			pos = b.size() - 1;

			while (start >= 0) {
				if (b.get(start).compareTo(a.get(i), 0) >= 1 && b.get(start).compareTo(a.get(i), 1) >= 1
						&& b.get(start).compareTo(a.get(i), 2) < 0) {

					setbits[pos] = start;
					pos--;

					// outputCombinations2(a.get(i).p, b, start, b.size(),
					// check);
					start--;
					// end--;
					numofsetbits++;
					/// matches over
					/// from start till the end of the list produce the patterns
				} /// here
				else if (b.get(start).compareTo(a.get(i), 0) < 0)
					break;
				else {
					start--;
				}

			}

			if (numofsetbits > 0) {
				System.out.println("before combinations" + i);
				outputCombinations(a.get(i).event.id, b, numofsetbits, b.size() - 1, ((StockEvent) c).id, setbits);
				System.out.println("after combinations" + i);
			}

		}

	}

	public void newOutputMethod(List<Value> a, List<Value> b, Event c) {

		/// Sort them acording to the timestamps to make it event sequences

		Collections.sort(a, new TimeComparator());

		Collections.sort(b, new TimeComparator());
		int numofsetbits = 0;
		/// create a list of bitsets, with size of A and each elements of size b

		BitSet[] listbitsets = new BitSet[a.size()];

		listbitsets[a.size() - 1] = new BitSet(b.size());

		int start = b.size() - 1;
		/// loop over A and B

		for (int i = a.size() - 1; i > 0; i--) {
			numofsetbits = 0;
			//// if the event dominates the other one
			// System.out.println(a.get(i).event.toString());
			if (i != a.size() - 1) {

				if (a.get(i).compareTo(a.get(i + 1), 1) <= 0 && a.get(i).compareTo(a.get(i + 1), 2) >= 0
						&& listbitsets[i + 1] != null && !listbitsets[i + 1].isEmpty()) {

					listbitsets[i] = listbitsets[i + 1].get(0, listbitsets[i + 1].size());

					// listbitsets[i + 1].isEmpty()
					/// go over all the listbits that are off
					System.out.println(++times_dominated);
					int k = listbitsets[i].previousClearBit(start);
					while (k >= 0) {
						/// do stuff
						if (b.get(k).compareTo(a.get(i), 1) >= 1 && b.get(k).compareTo(a.get(i), 2) < 0) {
							listbitsets[i].set(k);

						}

						k = listbitsets[i].previousClearBit(k - 1);
					}

					// for (int k = listbitsets[i].previousClearBit(start); k >=
					// 0; k = listbitsets[i]
					// .previousClearBit(start - 1)) {
					// if (k == b.size())
					// break;
					//
					// if (b.get(k).compareTo(a.get(i), 1) >= 1 &&
					// b.get(k).compareTo(a.get(i), 2) < 0) {
					// listbitsets[i].set(k);
					//
					// }
					//
					// }

				} else {
					System.out.println("No Dominated " + ++no_dominated);
					start = b.size() - 1;

				}

			}

			while (start >= 0) {
				if (b.get(start).compareTo(a.get(i), 0) >= 1 && b.get(start).compareTo(a.get(i), 1) >= 1
						&& b.get(start).compareTo(a.get(i), 2) < 0) {

					if (listbitsets[i] == null) {
						listbitsets[i] = new BitSet(b.size());
					}

					listbitsets[i].set(start);

					start--;

					numofsetbits++;
				} else if (b.get(start).compareTo(a.get(i), 0) <= 0) {
					// listbitsets[i].set(3);
					break;
				} else {
					start--;
				}
			}

			if (numofsetbits > 0) {

			}

		}

	}

	public void saseOutputCombination(Value a, List<Value> b, int numofsetbits, int end, int c, BitSet setbits) {

		//// for all the setbits output the values in b list

	}

	// TODO abderrahmen
	/*
	 * private void
	 * createResultsABCComplexJoinWithChunck(ArrayList<ArrayList<Value>>
	 * chunkedValA, ArrayList<ArrayList<Value>> chunkedValB, String bop, Event
	 * c) throws IOException {
	 * 
	 * for (int i = 0; i < chunkedValA.size(); i++) {
	 * 
	 * int min = chunkedValA.get(i).get(0).min[2]; int max =
	 * chunkedValA.get(i).get(0).max[1];
	 * 
	 * for (int j = 0; j < chunkedValB.size(); j++) {
	 * 
	 * if (min > chunkedValB.get(j).get(0).max[2] && max <
	 * chunkedValB.get(j).get(0).min[1])
	 * 
	 * {
	 * 
	 * outputCombinationsAllCombinations(chunkedValA.get(i), chunkedValB.get(j),
	 * c.id); // System.out.println("it s possible "); //
	 * System.out.println("A " + chunkedValA.get(i)); // System.out.println("B "
	 * + chunkedValB.get(j));
	 * 
	 * } else { createResultsABCComplexJoin(chunkedValA.get(i),
	 * chunkedValB.get(j), bop, c);
	 * 
	 * } }
	 * 
	 * }
	 * 
	 * }
	 */

	// TODO abderrahmen
	private void outputCombinationsAllCombinations(List<Value> a, List<Value> b, int c) throws IOException {

		for (int i = 0; i < a.size(); i++) {
			get_outputQueue().add(Integer.toString(a.get(i).event.id));
			for (int k = 0; k < b.size(); k++) {
				get_outputQueue().add("," + b.get(k).event.id);
				get_outputQueue().add("," + c);
				get_outputQueue().add("\n");

			}
		}
	}

	private void outputCombinations(int element, List<Value> b, int numofsetbits, int end, int c, int[] setbits)
			throws IOException {

		for (int i = 1; i < Math.pow(2, (numofsetbits)); i++) {

			BitSet bs = BitSet.valueOf(new long[] { i });
			// System.out.print(Integer.toString(element));
			get_outputQueue().add(Integer.toString(element));
			for (int j = bs.previousSetBit(numofsetbits); j >= 0; j = bs.previousSetBit(j - 1)) {
				// operate on index i here
				if (j == Integer.MIN_VALUE) {
					break; // or (i+1) would overflow
				}
				/// get the set bits from the array

				// outputWriter.write("," + b.get(setbits[end - j]));
				// System.out.print("," + b.get(setbits[end - j]).event.id);
				get_outputQueue().add("," + b.get(setbits[end - j]).event.id);
			}

			// System.out.println();
			// System.out.print("," + c + "\n");
			get_outputQueue().add("," + c);
			get_outputQueue().add("\n");

		}
		// System.out.println();
	}

	/*
	 * private ArrayList<ArrayList<Value>> queryStorebyChunk(StockEvent ce,
	 * ZIndex[] ranges) throws IOException {
	 * 
	 * // /search over both the stores if they are not empty, first select the
	 * // secondary store
	 * 
	 * EventStore store = (store_1.getStoreType() == 0) ? store_1 : store_2;
	 * 
	 * ArrayList<ArrayList<Value>> val = null; if (store.tree().hasData()) {
	 * 
	 * val = store.tree().searchRangeWithintValueByChunk(ranges[0], ranges[1],
	 * minmaxInter); }
	 * 
	 * // /select the primary datasource store = (store_1.getStoreType() == 1) ?
	 * store_1 : store_2;
	 * 
	 * if (val != null)
	 * val.addAll(store.tree().searchRangeWithintValueByChunk(ranges[0],
	 * ranges[1], minmaxInter)); else val =
	 * store.tree().searchRangeWithintValueByChunk(ranges[0], ranges[1],
	 * minmaxInter);
	 * 
	 * return val; }
	 */

	private List<Value> queryStore(StockEvent ce, ZIndex[] ranges) throws IOException {

		// /search over both the stores if they are not empty, first select the
		// secondary store

		EventStore store = (store_1.getStoreType() == 0) ? store_1 : store_2;

		List<Value> val = null;
		if (store.tree().hasData()) {

			val = store.tree().searchRangeWithintValue(ranges[0], ranges[1], minmaxInter);
		}

		// /select the primary datasource
		store = (store_1.getStoreType() == 1) ? store_1 : store_2;

		if (val != null)
			val.addAll(store.tree().searchRangeWithintValue(ranges[0], ranges[1], minmaxInter));
		else
			val = store.tree().searchRangeWithintValue(ranges[0], ranges[1], minmaxInter);

		return val;
	}

	private EventStore getStore(StockEvent c) throws IOException {

		EventStore store = (store_1.getStoreType() == 0) ? store_1 : store_2;

		if (window.startTimeofWindow > store.getEndTime() && !store.tree().hasData()) {

			store.tree().clear(); // /change it later, making it null and
		} // reinitialising a new tree

		store = (store_1.getStoreType() == 1) ? store_1 : store_2;

		if (c.timestamp > store.getEndTime()) {
			// /stop putting in this tree and select the other tree
			EventStore primary_store = (store_1.getStoreType() == 0) ? store_1 : store_2;

			store.setStoreType(0); // /make it a secondary store since the new
									// event will be outside its defined window
			primary_store.setStoreType(1); // make it the primary one and put
											// the event in this one
			primary_store.setStartTime(c.timestamp);
			primary_store.setEndTime(c.timestamp + window.windowLength);

			store = primary_store;
		}

		return store;

	}

	// /here 0 is timestamp 1 is price and 2 is volume
	private void updateMaxMin(int t, int p, int v) {

		if (t > minmax.max[0]) {

			minmax.max[0] = t;
		}

		if (t < minmax.min[0]) {

			minmax.min[0] = t;
		}

		if (p > minmax.max[1]) {

			minmax.max[1] = p;
		}

		if (p < minmax.min[1]) {

			minmax.min[1] = p;
		}

		if (v > minmax.max[2]) {

			minmax.max[2] = v;
		}

		if (v < minmax.min[2]) {

			minmax.min[2] = v;
		}

	}

	public static class PriceComparator implements Comparator<Value> {

		@Override
		public int compare(Value o1, Value o2) {
			if (((StockEvent) o1.event).price == ((StockEvent) o2.event).price)
				return 0;
			return ((StockEvent) o1.event).price - ((StockEvent) o2.event).price > 0 ? 1 : -1;
		}
	}

	public static class VolumeComparator implements Comparator<Value> {

		@Override
		public int compare(Value o1, Value o2) {
			if (((StockEvent) o1.event).vol == ((StockEvent) o2.event).vol)
				return 0;
			return ((StockEvent) o1.event).vol - ((StockEvent) o2.event).vol > 0 ? 1 : -1;
		}
	}

	public static class TimeComparator implements Comparator<Value> {

		@Override
		public int compare(Value o1, Value o2) {
			if (((StockEvent) o1.event).timestamp == ((StockEvent) o2.event).timestamp)
				return 0;
			return ((StockEvent) o1.event).timestamp - ((StockEvent) o2.event).timestamp > 0 ? 1 : -1;
		}
	}

	@Override
	protected void process(SimpleEvent e) throws IOException, DimensionException {
		// TODO Auto-generated method stub

	}

	@Override
	protected boolean isInrangeB(int[] min, int[] max, int[] point) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected void joinSingleRegions(OpenBitSet[] listbitsets, Zregion r1, Zregion r2, String[] phis,
			ArrayList<ZIndex> valB) {
		// TODO Auto-generated method stub

	}

	@Override
	protected boolean isInrangeA(int[] min, int[] max, int[] point) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int dominates_generic(int[] max_r1, int[] min_r1, int[] max_r2, int[] min_r2, String[] phis) {
		// TODO Auto-generated method stub
		return 0;
	}

}
